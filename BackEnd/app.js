const express = require("express");
const bodyParser = require("body-parser");


const app = express();


app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PATCH, DELETE, OPTIONS"
    );
    next();
  });

const mongoose = require('mongoose');

const { Hotel } = require('./Models/hotel');
const { Reservation } = require('./Models/ReservationVoyage');



mongoose.connect("mongodb+srv://freedomTravel:wZgHpfnI8g7zqFjE@cluster0-iejc0.mongodb.net/test?retryWrites=true&w=majority",{useNewUrlParser :true});

mongoose.set('useCreateIndex', true)

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));





module.exports = app;