const mongooose = require('mongoose');


const Voyageschema = new mongooose.Schema(
    {
        
        id: {
            type: String,
            require: true

        },
        nom: {
            type: String,
            require: true
        },
        pays: {
            type: String,
            require: true
        },
        titre: {
            type: String,
            require: true
        },
        prix: {
            type: Number,
            require: true
        },
        departvoyages: 
            {
                datedepart:String,
                dateretour:String
            }
        ,
        imageVoyages:[
            {
                images: String,
            }
        ]
        
 
   
        
    }

);

Voyage = mongooose.model('Voyage', Voyageschema);
module.exports = { Voyage };



