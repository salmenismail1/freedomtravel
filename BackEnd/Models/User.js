
const mongooose = require('mongoose');

const userSchema = mongooose.Schema({
    
    nom: {
        type: String,
        require: true,
        trim: true
    },
    email: {
        type: String,
        require: true,
        trim: true
    },
    cin: {
        type: String,
        require: true,
        trim: true
    },
    tel: {
        type: String,
        require: true,
        trim: true
    },

    password: {
        type: String,
        require: true,
        trim: true
    },
    type: {
        type: String,
        require: true,
        trim: true
    },
    
    
});

User = mongooose.model('User', userSchema);
module.exports = { User };
