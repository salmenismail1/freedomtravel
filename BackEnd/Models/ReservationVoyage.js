const mongooose = require('mongoose');
const { Chambre } = require('./Chambre');

const ReservationVoyage = new mongooose.Schema(
    {
        IdClient: {
            type: String,
            require: true,
            trim: true
        },
        Etat: {
            type: String,
            require: true,
            trim: true
        },
        Payement: {
            type: Boolean,
            require: true,
            trim: true
        },
        Titre: {
            type: String,
            require: true,
            trim: true
        },
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Tel: {
            type: String,
            require: true,
            trim: true
        },
        Localisation: {
            type: String,
            require: true,
            trim: true
        },
        DateReservation: {
            type: Date,
            require: true,
            trim: true
        },
        DateDepart: {
            type: Date,
            require: true,
            trim: true
        },
        DatRetour: {
            type: Date,
            require: true,
            trim: true
        },
        Prix: {
            type: Number,
            require: true,
            trim: true
        },
        Chambre :[
            
                {
                    num: {
                        type: String,
                        require: true
            
                    },
                    adulte: [{
                        type: String,
                        require: true
                    }],
                    enfant: [{
                        type: String,
                        require: true
                    }],
                    bb:[ {
                        type: String,
                        require: true
                    }],
                }
            
        ]
    }

);


RVoyage = mongooose.model('ReservationVoyage', ReservationVoyage);
module.exports = { RVoyage };