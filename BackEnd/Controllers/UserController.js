const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {User} = require("../Models/User");
const{RCircuit} = require("../Models/ReservationCircuit");
const {ReservationSoiree}= require("../Models/ReservationSoiree");
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST,PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});
app.post("/in", (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(409).json({
          message: "Mail exists"
        });
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            const user = new User({
              email: req.body.email,
              password: hash,
              cin: req.body.cin,
              tel: req.body.tel,
              nom: req.body.nom,
              type: req.body.type,
            });
            user
              .save()
              .then(result => {
                res.status(201).json({
                  message: "User created"
                });
              })
              .catch(err => {
                res.status(500).json({
                  error: err
                });
              });
          }
        });
      }
    });
});

app.post("/connection", (req, res) => {
  var data = req.body;
  let email = data.email;
  let Password = data.password;
  let nom = data.nom
if(email.includes('@')){
  User.findOne({ email: email }).then(async (resultat) => {
    if (resultat) { 
      if (!bcrypt.compareSync(Password, resultat.password)) {
        res.status(404).send({ message: "mot de passe incorrect" })
    }
    let token = jwt.sign({
        id: resultat._id, email: resultat.email, password: resultat.password,type:resultat.type
    }, "k").toString();
    res.status(200).json({ token:token });
    }
}).catch(() => {
    resultat.status(400).send({
        message: "erreur : " 
    })
});
}else{
  User.findOne({ nom: email }).then(async (resultat) => {
    if (resultat) { 
      if (!bcrypt.compareSync(Password, resultat.password)) {
        res.status(404).send({ message: "mot de passe incorrect" })
    }
    let token = jwt.sign({
        id: resultat._id, nom: resultat.nom, password: resultat.password , type:resultat.type
    }, "k").toString();
    res.status(200).json({ token:token });
    }
}).catch(() => {
    resultat.status(400).send({
        message: "erreur : " 
    })
});
}

    


 
});
app.put('/api/ModifUser/:id', (req, res) => {
  var user = {
              email: req.body.email,
              cin: req.body.cin,
              tel: req.body.tel,
              nom: req.body.nom,
};
  User.findByIdAndUpdate({
      _id: req.params.id
  }, { $set: user }, { new: true }, (err, doc) => {
      if (!err) { res.status(200).send(doc); }
      else {
          res.status(400).send(console.log("erreur de mise a jour" + err));
      }
  })
});
app.put('/api/UpgradeToAdmin/:id', (req, res) => {
  var user = {
    type: "admin",
};
  User.findByIdAndUpdate({
      _id: req.params.id
  }, { $set: user }, { new: true }, (err, doc) => {
      if (!err) { res.status(200).send(doc); }
      else {
          res.status(400).send(console.log("erreur de mise a jour" + err));
      }
  })
});
app.put('/api/DowngradeToClient/:id', (req, res) => {
  var user = {
    type: "client",
};
  User.findByIdAndUpdate({
      _id: req.params.id
  }, { $set: user }, { new: true }, (err, doc) => {
      if (!err) { res.status(200).send(doc); }
      else {
          res.status(400).send(console.log("erreur de mise a jour" + err));
      }
  })
});
app.get('/api/client', (req, res) => {
  User.find({type:"client"},(err,client) => {
    res.send(client);
});
});
app.get('/api/admin', (req, res) => {
  User.find({type:"admin"},(err,client) => {
    res.send(client);
});
});
app.delete("/api/DeleteUser/:id", (req, res) => {
  User.findByIdAndRemove(
      {
          _id: req.params.id,
      },
      (err, doc) => {
          if (!err) {
              res.status(200).send(doc);
          }
          else { console.log('Error in User Delete :' + err) }
      });
});

app.get('/api/statut/:id', (req, res) => {
  User.findOne({_id: req.params.id},(err,user) => {
    res.status(200).send(user);
});
});

app.get('/api/ReservationHotelClientAccepter/:id', (req, res) => {

  
  RHotel.find({Etat:"accepter",Payement:false,IdClient:req.params.id},(err,Historique1) => {
    res.send(Historique1);

  });
  });
  app.get('/api/ReservationVoyageClientAccepter/:id', (req, res) => {

  
    RVoyage.find({Etat:"accepter",Payement:false,IdClient:req.params.id},(err,Historique1) => {
      res.send(Historique1);

  
    });
    });
    app.get('/api/ReservationCircuitClientAccepter/:id', (req, res) => {

  
      RCircuit.find({Etat:"accepter",Payement:false,IdClient:req.params.id},(err,Historique1) => {
        res.send(Historique1);

    
      });
      });
      app.get('/api/ReservationSoireeClientAccepter/:id', (req, res) => {

  
        ReservationSoiree.find({Etat:"accepter",Payement:false,IdClient:req.params.id},(err,Historique1) => {
          res.send(Historique1);

      
        });
        });
app.get('/api/ReservationHotelClientEncour/:id', (req, res) => {

  
  RHotel.find({Etat:"En attente",Payement:false,IdClient:req.params.id},(err,Historique1) => {
    res.send(Historique1);

  });
  });
  app.get('/api/ReservationVoyageClientEncour/:id', (req, res) => {

  
    RVoyage.find({Etat:"En attente",Payement:false,IdClient:req.params.id},(err,Historique1) => {
      res.send(Historique1);

  
    });
    });
    app.get('/api/ReservationCircuitClientEncour/:id', (req, res) => {

  
      RCircuit.find({Etat:"En attente",Payement:false,IdClient:req.params.id},(err,Historique1) => {
        res.send(Historique1);

    
      });
      });
      app.get('/api/ReservationSoireeClientEncour/:id', (req, res) => {

  
        ReservationSoiree.find({Etat:"En attente",Payement:false,IdClient:req.params.id},(err,Historique1) => {
          res.send(Historique1);

      
        });
        });
app.get('/api/HistoriqueClient/:id', (req, res) => {
   RVoyage.find({Payement:true,IdClient:req.params.id},(err,Historique1) => {
            res.send(Historique1);
  });
});
app.get('/api/HistoriqueClientHotel/:id', (req, res) => {
  RHotel.find({Payement:true,IdClient:req.params.id},(err,Historique1) => {
           res.send(Historique1);
 });
});
app.get('/api/HistoriqueClientCircuit/:id', (req, res) => {
  RCircuit.find({Payement:true,IdClient:req.params.id},(err,Historique1) => {
           res.send(Historique1);
 });
});
app.get('/api/HistoriqueClientSoiree/:id', (req, res) => {
  ReservationSoiree.find({Payement:true,IdClient:req.params.id},(err,Historique1) => {
           res.send(Historique1);
 });
});
app.get('/api/Historique', (req, res) => {
  RVoyage.find({Payement:true},(err,Historique1) => {
    RHotel.find({Payement:true},(err,Historique) => {
      res.send(Historique1.concat(Historique));
    });
  });
});

app.put('/api/UpgradePayement/:id', (req, res) => {
  var user = {
    Payement: true,
  };
  RVoyage.findByIdAndUpdate({
  _id: req.params.id
  }, { $set: user }, { new: true }, (err, doc) => {
  if (!err) { 
    RHotel.findByIdAndUpdate({
      _id: req.params.id
      }, { $set: user }, { new: true }, (err, doc) => {
      if (!err) {
        res.status(200).send(doc);
      }
  else {
      res.status(400).send(console.log("erreur de mise a jour" + err));
  }
})
}
});
});
app.get('/api/reservation/:id', (req, res) => {
  RVoyage.find({_id:req.params.id},(err,ReservationVoyage) => {
   if(ReservationVoyage.length>0)
   {
    res.send(ReservationVoyage);
   }
   else{
    RHotel.find({_id:req.params.id},(err,ReservationHotel) => {
      if(ReservationHotel.length>0)
      {
        res.send(ReservationHotel);
      }
      else{
        ROmra.find({_id:req.params.id},(err,ReservationOmra) => {
          if(ReservationOmra.length>0)
          {
            res.send(ReservationOmra);
          }
          else{
            ReservationSoiree.find({_id:req.params.id},(err,Reservationsoiree) => {
              if(Reservationsoiree.length>0)
              {
                res.send(Reservationsoiree);
              }
              else{
                RCircuit.find({_id:req.params.id},(err,ReservationCircuit) => {
                  res.send(ReservationCircuit); 
                });
              }
            });
          }
        });
      }
    });
   }
  });
});
app.delete("/api/DeleteReservation/:id", (req, res) => {
  RVoyage.findByIdAndRemove({_id:req.params.id},(err,ReservationVoyage) => {
    if(ReservationVoyage!=null)
    {
     res.send(ReservationVoyage);
    }
    else{
     RHotel.findByIdAndRemove({_id:req.params.id},(err,ReservationHotel) => {
       if(ReservationHotel!=null)
       {
         res.send(ReservationHotel);
       }
       else{
         ROmra.findByIdAndRemove({_id:req.params.id},(err,ReservationOmra) => {
           if(ReservationOmra!=null)
           {
             res.send(ReservationOmra);
           }
           else{
            ReservationSoiree.findByIdAndRemove({_id:req.params.id},(err,Reservationsoiree) => {
               if(Reservationsoiree!=null)
               {
                 res.send(Reservationsoiree);
               }
               else{
                 RCircuit.findByIdAndRemove({_id:req.params.id},(err,ReservationCircuit) => {
                   res.send(ReservationCircuit); 
                 });
               }
             });
           }
         });
       }
     });
    }
   });
 });
module.exports = app;