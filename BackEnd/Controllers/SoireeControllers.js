const express = require("express");
const bodyParser = require("body-parser");


const app = express();
const { ReservationSoiree } = require('../Models/ReservationSoiree');
const multer = require('multer');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const { Soiree } = require('../Models/Soiree');
const { Soireehomeliset }  = require('../Models/SoireeHomeListe');


const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'src/assets')
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`)
    }
  })
  
  const upload = multer({ storage: storage })
  
  app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST,PUT, PATCH, DELETE, OPTIONS"
    );
    next();
  });
  
  app.get("/api/soiree", (req, res, next) => {
    Soiree.find().then(documents => {
      console.log(documents);
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });
    });
  });


app.post("/api/soiree",(req,res,next) => {
    const h = new Soiree ({

        id :  req.body.id,
        titre : req.body.titre,
        image : req.body.image,
        date : req.body.date,
        venteadultesingle : req.body.venteadultesingle,
        venteadultedble : req.body.venteadultedble,
        vente3rdad : req.body.vente3rdad,
        venteenfant2ad : req.body.venteenfant2ad,
        tarifbebe : req.body.tarifbebe,
        venteenfant1ad : req.body.venteenfant1ad,


    });
    h.save();
    res.status(201).json({
        message:"fdvbfdbdf hdfvbjkd bdfj jdfkb jkdfb kjdf jkdf jk dfjk kdfj kjdf kjfd kjdf kjdf kjdf kjdf jk"
    });
});




app.post('/file', upload.single('file'), (req, res, next) => {
    const file = req.file;
    file.filename="3oss";
    console.log(file.filename);
    if (!file) {
      const error = new Error('No File')
      error.httpStatusCode = 400
      return next(error)
    }
      res.send(file);
  })
  
  app.post('/multipleFiles', upload.array('files'), (req, res, next) => {
    const files = req.files;
    console.log(files);
    if (!files) {
      const error = new Error('No File')
      error.httpStatusCode = 400
      return next(error)
    }
      res.send({sttus:  'ok'});
  })
  app.delete("/api/posts/:id", (req, res, next) => {
    Soiree.deleteOne({ id: req.params.id }).then(result => {
      console.log(result);
      res.status(200).json({ message: "Post deleted!" });
    });
  });
  
  app.put("/api/soireeModif/:id", (req, res) => {
    console.log(req.params.id);

    const h = new Soiree({
  
        id :  req.body.id,
        titre : req.body.titre,
        image : req.body.image,
        date : req.body.date,
        venteadultesingle : req.body.venteadultesingle,
        venteadultedble : req.body.venteadultedble,
        vente3rdad : req.body.vente3rdad,
        venteenfant2ad : req.body.venteenfant2ad,
        tarifbebe : req.body.tarifbebe,
        venteenfant1ad : req.body.venteenfant1ad,

  });
console.log(req.body);
Soiree.findOneAndUpdate({

        _id: req.params.id
    }, { $set: req.body }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).send(doc); }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })

});

app.delete("/api/deleteall/:id", (req, res, next) => {
  Soireehomeliset.remove({}).then(result => {
    console.log(result);
    res.status(200).json({ message: "Post deleted!" });
  });
});

app.post("/api/soireehomelist",(req,res,next) => {
  const h = new Soireehomeliset({

      id :  req.body.id,
      titre : req.body.titre,
      image : req.body.image,
      date : req.body.date,
      venteadultesingle : req.body.venteadultesingle, 
      venteadultedble : req.body.venteadultedble,
      vente3rdad : req.body.vente3rdad,
      venteenfant2ad : req.body.venteenfant2ad,
      tarifbebe : req.body.tarifbebe,
      venteenfant1ad : req.body.venteenfant1ad

  });
  h.save();
  res.status(201).json({
      message:"fdvbfdbdf hdfvbjkd bdfj jdfkb jkdfb kjdf jkdf jk dfjk kdfj kjdf kjfd kjdf kjdf kjdf kjdf jk"
  });
});

app.get("/api/soireehomelist", (req, res, next) => {
  Soireehomeliset.find().then(documents => {
    console.log(documents);
    res.status(200).json({
      message: "Posts fetched successfully!",
      posts: documents
    });
    console.log("-------------------------------------");

  });
});





  //******************** Reservation Hotel ********************* */

  app.post("/api/reservation",(req,res,next) => {
    const s = new ReservationSoiree({
  
      IdClient:req.body.IdClient,
      Etat:"En attente" ,
      Payement:false ,
      Titre: req.body.Titre,
      Nom: req.body.Nom,
      Email: req.body.Email,
      Tel: req.body.Tel,
      Localisation:  req.body.Localisation,
      DateReservation:  req.body.DateReservation,
      DateDepart:  req.body.DateDepart,
      DatRetour:  req.body.DatRetour,
      Prix:  req.body.Prix,
      Chambre :req.body.Chambre
  
    });
    s.save();
    res.status(201).json({
        message:"L'ajout du reservation de voyage avec succée"
    });
  });
  
  
  
  app.delete("/api/reservation/:nom", (req, res) => {
  
    RVoyage.findOneAndRemove(
        {
            Nom: req.params.nom,
        },
  
        (err, doc) => {
            if (!err) {
                res.status(200).send(doc);
                console.log(doc);
            }
            else { console.log('Error in Reservation Voyage  Delete :' + err) }
        });
  });
  
  app.get('/api/ReservationVoyage', (req, res) => {
  
    RVoyage.find().then((RVoyage) => {
        if (RVoyage) {
            res.status(200).send(RVoyage);
        }
        else { console.log("not found" + err.message) }
  
    })
  
  });
  

  app.get('/api/ReservationSoireeAccepter', (req, res) => {
  
    RHotel.find({Etat:"accepter"},(err,ReservationHotel) => {
      res.send(ReservationHotel);
  });
  
  });
  

  app.get('/api/ReservationSoireeEnAttente', (req, res) => {
  
    RHotel.find({Etat:"En attente"},(err,ReservationHotel) => {
      res.send(ReservationHotel);
  });
  
  });
  
  

  app.put('/api/ReservationAccepter/:id', (req, res) => {
    var resHotel = {
      Etat: "accepter",
  };

      RHotel.findByIdAndUpdate({

        _id: req.params.id
    }, { $set: resHotel }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).send(doc); }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })
  
  });




  app.post('/send/:email', (req, res) => {

    const h = new Hotel({
    
      id :  req.body.id,
      nom : req.body.nom,
      adultOnly : req.body.adultOnly,
      ville : req.body.ville,
      categorie : req.body.categorie,
      type : req.body.type,
      lpdvente : req.body.lpdvente,
      dpvente : req.body.dpvente,
      pcvente : req.body.pcvente,
      allinsoftvente : req.body.allinsoftvente,
      allinvente : req.body.allinvente,
      ultraallinvente : req.body.ultraallinvente,
      age_enf_gratuit : req.body.age_enf_gratuit,
      image:req.body.image
  
  });
  
    const output = `
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
    <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width" name="viewport"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <title></title>
    <style type="text/css">
  
      .rating a {
      float: center;
      color: #aaa;
      text-decoration: none;
      font-size: 3em;
      transition: color .4s;
      color: orange;
   }
    
    </style>
    </head>
    <body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;">
      <img style="margin: 1%; text-align: right;width: 5%;height: 5%" src="https://www.freedomtravel.tn/assets/images/logo_1_freedomtravel.png" width="100%" height="200px" />
      <h1 style="color: blue"><center>Promotion  !!!!!</center></h1>
      <h2 style="color: orangered"><center>${h.nom}</center></h2>
      <center><img src="${h.image}" /></center>
  
      <div class="row">
        <h5 ><center>
        <div class="rating">
           <a href="#5" title="Donner 5 étoiles">☆</a>
           <a href="#4" title="Donner 4 étoiles">☆</a>
           <a href="#3" title="Donner 3 étoiles">☆</a>
           <a href="#2" title="Donner 2 étoiles">☆</a>
           <a href="#1" title="Donner 1 étoile">☆</a>
        </div></center>
        </h5>
      </div>
  
      <h2><center> Prix : ${h.lpdvente} DT </center></h2>
   
    </body>
    </html>
    `;
  
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'salmenismail3@gmail.com',
        pass: 'salmen.ismail3'
      }
    });
  
    // setup email data with unicode symbols
    let mailOptions = {
        from: '"FreedomTravel" <salmenismail3@gmail.com>', // sender address
        to: req.params.email, // list of receivers
        subject: 'Promotion du jour', // Subject line
        text: 'Hello world?', // plain text body
        html: output // html body
    };
  
    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);   
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  
        res.render('contact', {msg:'Email has been sent'});
    });
    });

module.exports = app;