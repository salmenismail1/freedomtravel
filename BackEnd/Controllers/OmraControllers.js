const express = require("express");
const bodyParser = require("body-parser");


const app = express();
const { Omra } = require('../Models/Omra');
const { ROmra } = require('../Models/ReservationOmra');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



app.post("/api/AjoutHotel",(req,res,next) => {
  const h = new Hotel({

      id :  req.body.id,
      nom : req.body.nom,
      adultOnly : req.body.adultOnly,
      ville : req.body.ville,
      categorie : req.body.categorie,
      type : req.body.type,
      lpdvente : req.body.lpdvente,
      dpvente : req.body.dpvente,
      pcvente : req.body.pcvente,
      allinsoftvente : req.body.allinsoftvente,
      allinvente : req.body.allinvente,
      ultraallinvente : req.body.ultraallinvente,
      age_enf_gratuit : req.body.age_enf_gratuit,
      image:req.body.image

  });
  h.save();
  res.status(201).json({
      message:"L'ajout de l'hotel avec succée"
  });
});


app.get('/api/ListerHotel', (req, res) => {

  Hotel.find().then((Hotel) => {
      if (Hotel) {
          res.status(200).send(Hotel);
      }
      else { console.log("not found" + err.message) }

  })

});

app.delete("/api/Hotel/:nom", (req, res) => {

  Hotel.findOneAndRemove(
      {
          nom: req.params.nom,
      },

      (err, doc) => {
          if (!err) {
              res.status(200).send(doc);
              console.log(doc);
          }
          else { console.log('Error in Hotel Delete :' + err) }
      });
});



//******************** Reservation Hotel ********************* */

app.post("/api/reservationOmra",(req,res) => {
    const r = new ROmra({

        IdClient:req.body.IdClient,
        Etat:"En attente" ,
        Payement:false ,
        Titre: req.body.Titre,
        Nom: req.body.Nom,
        Email: req.body.Email,
        Tel: req.body.Tel,
        Localisation:  req.body.Localisation,
        DateReservation:  req.body.DateReservation,
        DateDepart:  req.body.DateDepart,
        DatRetour:  req.body.DatRetour,
        Prix:  req.body.Prix,
        Chambre :req.body.Chambre  
  
    });
    r.save();
    res.status(201).json({
        message:"L'ajout du reservation de Omra avec succée"
    });
  });
  
  
  
  app.delete("/api/DeleteReservationOmra/:nom", (req, res) => {
  
    ROmra.findOneAndRemove(
        {
            Nom: req.params.nom,
        },
  
        (err, doc) => {
            if (!err) {
                res.status(200).send(doc);
                console.log(doc);
            }
            else { console.log('Error in Reservation Omra  Delete :' + err) }
        });
  });
  
  app.get('/api/ListerReservationOmra', (req, res) => {
  
    ROmra.find().then((ReservationHotel) => {
        if (ReservationHotel) {
            res.status(200).send(ReservationHotel);
        }
        else { console.log("not found" + err.message) }
  
    })
  
  });


  app.put("/ReservationOmraModif/:id", (req, res) => {
    
    var resOmra = {
        Nom: req.body.Nom,
        Email: req.body.Email,
        Date:  req.body.Date

    };

    ROmra.findByIdAndUpdate({

        _id: req.params.id
    }, { $set: resOmra }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).send(doc); }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })

});

app.post('/send/:email', (req, res) => {

    const c = new Circuit ({
  
      id :  req.body.id,
      nom : req.body.nom,
      image:req.body.image,
      datedepart : req.body.datedepart,
      dateretour : req.body.dateretour,
      programmme : req.body.programmme,
      venteadultesingle : req.body.venteadultesingle,
      venteadultedble : req.body.venteadultedble,
      vente3rdad : req.body.vente3rdad,
      venteenfant2ad : req.body.venteenfant2ad,
      tarifbebe : req.body.tarifbebe,
      venteenfant1ad : req.body.venteenfant1ad,
  
     
  
  
  }); 
  
    const output = `
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
    <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width" name="viewport"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <title></title>
    <style type="text/css">
  
      .rating a {
      float: center;
      color: #aaa;
      text-decoration: none;
      font-size: 3em;
      transition: color .4s;
      color: orange;
   }
    
    </style>
    </head>
    <body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;">
      <img style="margin: 1%; text-align: right;width: 5%;height: 5%" src="https://www.freedomtravel.tn/assets/images/logo_1_freedomtravel.png" width="100%" height="200px" />
      <h1 style="color: blue"><center>Promotion  !!!!!</center></h1>
      <h2 style="color: orangered"><center>${c.nom}</center></h2>
      <center><img src="${c.image}" /></center>
  
      <div class="row">
        <h5 ><center>
        <div class="rating">
           <a href="#5" title="Donner 5 étoiles">☆</a>
           <a href="#4" title="Donner 4 étoiles">☆</a>
           <a href="#3" title="Donner 3 étoiles">☆</a>
           <a href="#2" title="Donner 2 étoiles">☆</a>
           <a href="#1" title="Donner 1 étoile">☆</a>
        </div></center>
        </h5>
      </div>
  
      <h2><center> Prix : ${c.venteadultesingle} DT </center></h2>
   
    </body>
    </html>
    `;
  
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'salmenismail3@gmail.com',
        pass: 'salmen.ismail3'
      }
    });
  
    // setup email data with unicode symbols
    let mailOptions = {
        from: '"FreedomTravel" <salmenismail3@gmail.com>', // sender address
        to: req.params.email, // list of receivers
        subject: 'Promotion du jour', // Subject line
        text: 'Hello world?', // plain text body
        html: output // html body
    };
  
    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);   
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  
        res.render('contact', {msg:'Email has been sent'});
    });
    });





module.exports = app;