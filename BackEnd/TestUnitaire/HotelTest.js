let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../Controllers/HotelControllers");

//Assertion Style
chai.should();

chai.use(chaiHttp);

describe('Hotel Controller API', () => {

    /**
     * Test the GET Reservation Hotels
     */
    describe("GET /api/ReservationHotel", () => {
        it("It should GET all the Hotels", () => {
            chai.request(server)
                .get("/api/ReservationHotel")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
               
                });
        });

        it("Url Ghalet", () => {
            chai.request(server)
                .get("/api/urlghalet")
                .end((err, response) => {
                    response.should.have.status(404);
                
                });
        });

    });


     /**
     * Test the POST  Hotel
     */
    describe("POST /api/hotel", () => {
        it("It should POST a new Reservation Hotel", () => {
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
            };
            chai.request(server)                
                .post("/api/hotel")
                .send(hotel)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                
                });
        });

        

    });

    /**
     * Test the DELETE route
     */
    describe("DELETE /api/posts/:id", () => {
        it("It should DELETE an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            chai.request(server)                
                .delete("/api/posts/" + id)
                .end((err, response) => {
                    response.should.have.status(200);
            
                });
        });

        

    });

     /**
     * Test the PUT route
     */
    describe("PUT /api/hotelModif/:id", () => {
        it("It should PUT an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
                
            };
            chai.request(server)                
                .put("/api/hotelModif/" + id)
                .send(hotel)
                .end((err, response) => {
                    //response.should.have.status(200);
                    response.body.should.be.a('object');
                
                });
        });

              
    });



});

describe('Voyage Controller API', () => {

    /**
     * Test the GET Reservation Voyage
     */
    describe("GET ", () => {
        it("It should GET all the Voyages", () => {
            chai.request(server)
                .get("")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
               
                });
        });

    });


     /**
     * Test the POST  Voyage
     */
    describe("POST /api/hotel", () => {
        it("It should POST a new Reservation Hotel", () => {
            const voyage = {
                
            };
            chai.request(server)                
                .post("/api/hotel")
                .send(voyage)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                
                });
        });

        

    });

    /**
     * Test the DELETE route
     */
    describe("DELETE /api/deleteall/:id", () => {
        it("It should DELETE an existing hotel", () => {
            const id = "5f1ab76d97a20d57e0c07f89";
            chai.request(server)                
                .delete("/api/deleteall/" + id)
                .end((err, response) => {
                    response.should.have.status(200);
            
                });
        });

        

    });

     /**
     * Test the PUT route
     */
    describe("PUT :id", () => {
        it("It should PUT an existing hotel", () => {
            const id = "5f1ab76d97a20d57e0c07f89";
            const hotel = {
                
                
            };
            chai.request(server)                
                .put("/api/hotelModif/" + id)
                .send(hotel)
                .end((err, response) => {
                    //response.should.have.status(200);
                    response.body.should.be.a('object');
                
                });
        });

              
    });



});

describe('Circuit Controller API', () => {

    /**
     * Test the GET Reservation Hotels
     */
    describe("GET /api/ReservationHotel", () => {
        it("It should GET all the Hotels", () => {
            chai.request(server)
                .get("/api/ReservationHotel")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
               
                });
        });

        it("Url Ghalet", () => {
            chai.request(server)
                .get("/api/urlghalet")
                .end((err, response) => {
                    response.should.have.status(404);
                
                });
        });

    });


     /**
     * Test the POST  Hotel
     */
    describe("POST /api/hotel", () => {
        it("It should POST a new Reservation Hotel", () => {
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
            };
            chai.request(server)                
                .post("/api/hotel")
                .send(hotel)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                
                });
        });

        

    });

    /**
     * Test the DELETE route
     */
    describe("DELETE /api/posts/:id", () => {
        it("It should DELETE an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            chai.request(server)                
                .delete("/api/posts/" + id)
                .end((err, response) => {
                    response.should.have.status(200);
            
                });
        });

        

    });

     /**
     * Test the PUT route
     */
    describe("PUT /api/hotelModif/:id", () => {
        it("It should PUT an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
                
            };
            chai.request(server)                
                .put("/api/hotelModif/" + id)
                .send(hotel)
                .end((err, response) => {
                    //response.should.have.status(200);
                    response.body.should.be.a('object');
                
                });
        });

              
    });



});

describe('Omra Controller API', () => {

    /**
     * Test the GET Reservation Hotels
     */
    describe("GET /api/ReservationHotel", () => {
        it("It should GET all the Hotels", () => {
            chai.request(server)
                .get("/api/ReservationHotel")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
               
                });
        });

        it("Url Ghalet", () => {
            chai.request(server)
                .get("/api/urlghalet")
                .end((err, response) => {
                    response.should.have.status(404);
                
                });
        });

    });


     /**
     * Test the POST  Hotel
     */
    describe("POST /api/hotel", () => {
        it("It should POST a new Reservation Hotel", () => {
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
            };
            chai.request(server)                
                .post("/api/hotel")
                .send(hotel)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                
                });
        });

        

    });

    /**
     * Test the DELETE route
     */
    describe("DELETE /api/posts/:id", () => {
        it("It should DELETE an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            chai.request(server)                
                .delete("/api/posts/" + id)
                .end((err, response) => {
                    response.should.have.status(200);
            
                });
        });

        

    });

     /**
     * Test the PUT route
     */
    describe("PUT /api/hotelModif/:id", () => {
        it("It should PUT an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
                
            };
            chai.request(server)                
                .put("/api/hotelModif/" + id)
                .send(hotel)
                .end((err, response) => {
                    //response.should.have.status(200);
                    response.body.should.be.a('object');
                
                });
        });

              
    });



});

describe('Soiree Controller API', () => {

    /**
     * Test the GET Reservation Hotels
     */
    describe("GET /api/ReservationHotel", () => {
        it("It should GET all the Hotels", () => {
            chai.request(server)
                .get("/api/ReservationHotel")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
               
                });
        });

        it("Url Ghalet", () => {
            chai.request(server)
                .get("/api/urlghalet")
                .end((err, response) => {
                    response.should.have.status(404);
                
                });
        });

    });


     /**
     * Test the POST  Hotel
     */
    describe("POST /api/hotel", () => {
        it("It should POST a new Reservation Hotel", () => {
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
            };
            chai.request(server)                
                .post("/api/hotel")
                .send(hotel)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                
                });
        });

        

    });

    /**
     * Test the DELETE route
     */
    describe("DELETE /api/posts/:id", () => {
        it("It should DELETE an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            chai.request(server)                
                .delete("/api/posts/" + id)
                .end((err, response) => {
                    response.should.have.status(200);
            
                });
        });

        

    });

     /**
     * Test the PUT route
     */
    describe("PUT /api/hotelModif/:id", () => {
        it("It should PUT an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
                
            };
            chai.request(server)                
                .put("/api/hotelModif/" + id)
                .send(hotel)
                .end((err, response) => {
                    //response.should.have.status(200);
                    response.body.should.be.a('object');
                
                });
        });

              
    });



});

describe('User Controller API', () => {

    /**
     * Test the GET Reservation Hotels
     */
    describe("GET /api/ReservationHotel", () => {
        it("It should GET all the Hotels", () => {
            chai.request(server)
                .get("/api/ReservationHotel")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
               
                });
        });

        it("Url Ghalet", () => {
            chai.request(server)
                .get("/api/urlghalet")
                .end((err, response) => {
                    response.should.have.status(404);
                
                });
        });

    });


     /**
     * Test the POST  Hotel
     */
    describe("POST /api/hotel", () => {
        it("It should POST a new Reservation Hotel", () => {
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
            };
            chai.request(server)                
                .post("/api/hotel")
                .send(hotel)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                
                });
        });

        

    });

    /**
     * Test the DELETE route
     */
    describe("DELETE /api/posts/:id", () => {
        it("It should DELETE an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            chai.request(server)                
                .delete("/api/posts/" + id)
                .end((err, response) => {
                    response.should.have.status(200);
            
                });
        });

        

    });

     /**
     * Test the PUT route
     */
    describe("PUT /api/hotelModif/:id", () => {
        it("It should PUT an existing hotel", () => {
            const id = "5f2c5f0a77f8f30cb04fe6a9";
            const hotel = {
                id :  "id",
                nom : "nom",
                adultOnly : "adultOnly",
                ville : "ville",
                categorie : "categorie",
                type : "type",
                lpdvente : "lpdvente",
                dpvente : "dpvente",
                pcvente : "pcvente",
                allinsoftvente : "allinsoftvente",
                allinvente : "allinvente",
                ultraallinvente : "ultraallinvente",
                age_enf_gratuit : "age_enf_gratuit",
                image:"image"
                
            };
            chai.request(server)                
                .put("/api/hotelModif/" + id)
                .send(hotel)
                .end((err, response) => {
                    //response.should.have.status(200);
                    response.body.should.be.a('object');
                
                });
        });

              
    });



});


