import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rechercheFilterCircuit'
})
export class RechercheFilterCircuitPipe implements PipeTransform {

  transform(ListeCircuits: any, text:string): any {
    if( text === undefined) return ListeCircuits;
      return ListeCircuits.filter(function (circuit) {return circuit.nom.toLowerCase().includes(text.toLocaleLowerCase());})
    
  }
}
