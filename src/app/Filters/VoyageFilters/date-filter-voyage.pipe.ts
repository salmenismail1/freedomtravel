import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@angular/common';

@Pipe({
  name: 'dateFilterVoyage'
})
export class DateFilterVoyagePipe implements PipeTransform {

  transform(ListeVoyages: any, text:string): any {
    
    if( text === undefined) return ListeVoyages;

        return ListeVoyages.filter(function (v) {
          console.log(v.departvoyages[0].datedepart);
          return (
          
          formatDate( new Date(v.departvoyages[0].datedepart), 'yyyy/MM/dd', 'en'))>= (formatDate(text, 'yyyy/MM/dd', 'en'));
        })

  }

  

}
