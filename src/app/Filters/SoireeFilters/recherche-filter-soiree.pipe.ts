import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rechercheFilterSoiree'
})
export class RechercheFilterSoireePipe implements PipeTransform {

  transform(ListeSoirees: any, text:string): any {
    if( text === undefined) return ListeSoirees;
      return ListeSoirees.filter(function (soiree) {return soiree.titre.toLowerCase().includes(text.toLocaleLowerCase());})
    
  }

}
