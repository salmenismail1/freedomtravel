export class Hotels {

        id :  string;
        nom : string;
        adultOnly : string;
        ville : string;
        categorie : string;
        type : string;
        lpdvente : number;
        dpvente : number;
        pcvente : number;
        allinsoftvente : number;
        allinvente : number;
        ultraallinvente : number;
        age_enf_gratuit : number;
        image:string;

        
}
