export class Circuit {
    
    id:string;
    nom:string;
    image:string;
    datedepart:string;
    dateretour:string;
    programmme:{
        description:string;
        libelle:string;
        programme:string;
    };
    venteadultesingle:number;
    venteadultedble:number;
    vente3rdad:number;
    venteenfant2ad:number;
    tarifbebe:number;
    venteenfant1ad:number    

}
