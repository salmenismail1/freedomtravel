export class Omra {
    id:number;
    titre:string;
    date:string;
    prix:number;
    programme:string;
    agent:string;
    image:string;
}
