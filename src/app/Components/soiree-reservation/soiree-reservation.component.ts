import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { Reservation } from '../../Models/reservation';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ReservationService } from '../../Services/reservation.service';
import { Soiree } from '../../Models/soiree';
import { Observable } from 'rxjs';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import { Chambre } from '../../Models/chambre';
import { ToastrService } from 'ngx-toastr';
import * as html2pdf  from 'html2pdf.js';
import * as jwt_decode from 'jwt-decode';

const AjoutReservationUrl = "http://localhost:3000/Soiree/api/reservation";

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};

@Component({
  selector: 'app-soiree-reservation',
  templateUrl: './soiree-reservation.component.html',
  styleUrls: ['./soiree-reservation.component.scss']
})


export class SoireeReservationComponent implements OnInit {
  reactiveform :FormGroup;
  reservation = new Reservation;
  ListeReservation
  adult;enfant ;bb;
  id;
  uneSoiree;
  soiree;
  first;
  nb;
  e;
  prix : number;
  TChambre;nbChambre = new Array(5);
  venteadultesingle:number;venteadultedble:number;tarifbebe:number;
  datedepart;dateretour;

  Adulte;Enfant;Bebe;nbrAdulte ; nbrEnfant ; nbrBebe; TAdulte ; TEnfant; TBebe;TnbrAdulte;TnbrEnfant;TnbrBebe;
  
  constructor(private router : Router, private toastr: ToastrService,private Activate:ActivatedRoute,private httpClient:HttpClient,private serviceReservation:ReservationService) { }
 

  ngOnInit() {
    this.nb=4;
    this.id =this.Activate.snapshot.paramMap.get("id");
    this.uneSoiree = "https://topresa.ovh/api/departsoirees.json?date[before]="+formatDate(new Date(), 'yyyy-MM-dd', 'en')+'&soiree.id=' +this.id;

    this.reactiveform= new FormGroup({
      'Nom' : new FormControl('',[Validators.required]),
      'Email' : new FormControl('', [Validators.email,Validators.required]),
      'Tel' : new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern('[0-9]*')]),
      
    })
    
  
  
 
    this.getUneSoiree().subscribe(        
      response =>{
        this.first=response[0];
        
        this.soiree=response;
        console.log(response)
      }
    )

  }

  get Nom() { return this.reactiveform.get('Nom'); }
  get Email() { return this.reactiveform.get('Email'); }
  get Tel() { return this.reactiveform.get('Tel'); }


  getUneSoiree():Observable<Soiree>{
    return this.httpClient.get<Soiree>(this.uneSoiree,httpOptions );

  }


  chambre(){
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);

      if(selectElement.hidden==true){
        selectElement.hidden=false;
        console.log(selectElement);
        console.log(this.adult)
        return;
      }
    }
  }
  delchambre(i:number){
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
    selectElement.hidden=true;

    const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
    select1.selectedIndex=0;

    const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
    console.log(select2.selectedIndex)
    select2.selectedIndex=0;

    const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
    select3.selectedIndex=0;
    console.log(selectElement);

    console.log(this.adult)
   

  }

  verif(){
    console.log(this.e)
  }
  ngAfterViewInit(): void {
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+0);
    
    selectElement.hidden=false;
  
    console.log(selectElement);
    console.log(this.adult)
  }

  Prix(venteadultesingle:any,venteadultedble:any,tarifbebe:any,datedepart:any,dateretour:any){
    this.venteadultesingle=venteadultesingle;
    this.venteadultedble = venteadultedble;
    this.tarifbebe = tarifbebe;
    this.datedepart=datedepart;
    this.dateretour=dateretour;
  }

  calculPrix(){
    
   this.prix=0;
    for(let j=0;j<this.nb;j++){
      
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+j);

      if(selectElement.hidden==false){
        
        const select1 = <HTMLSelectElement>document.getElementById('homme'+j);
        const select2 = <HTMLSelectElement>document.getElementById('enfant'+j);
        const select3 = <HTMLSelectElement>document.getElementById('bebe'+j);
        

        this.prix+= (select1.selectedIndex*this.venteadultesingle)+( select2.selectedIndex*this.venteadultedble)+(select3.selectedIndex*this.tarifbebe)

        console.log(this.prix);
      }
    }
    console.log(this.prix);
  }

  clear() {

    this.reactiveform.reset();
    
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
      selectElement.hidden=true;
      const firstElement = <HTMLSelectElement>document.getElementById('rowid'+0);
      firstElement.hidden=false;
        const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
        select1.selectedIndex=0;
        const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
        select2.selectedIndex=0;
        const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
        select3.selectedIndex=0;

    }
    this.prix=0;
    
  }

  pdf(){
    const options = {
      margin:       15,
      filename:     'myfile.pdf',
      image:        { type: 'jpeg' },
      jsPDF:        {orientation: 'landscape' }
    };
    var content = document.getElementById('content');

    html2pdf()
      .from(content)
      .set(options)
      .save();
  }

  affiche(){

    this.TnbrAdulte= new Array();  this.TnbrEnfant= new Array();  this.TnbrBebe= new Array();
    this.nbrAdulte=0; this.nbrEnfant=0; this.nbrBebe=0;
    this.TChambre=[] ;
    let cmp = 0;
    
    for(let j=0;j<this.nb;j++){
      
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+j);

      
      if(selectElement.hidden==false){
        
         this.Adulte = <HTMLSelectElement>document.getElementById('homme'+j);
         this.Enfant = <HTMLSelectElement>document.getElementById('enfant'+j);
         this.Bebe = <HTMLSelectElement>document.getElementById('bebe'+j);

         this.nbrAdulte +=this.Adulte.selectedIndex ;
         this.nbrEnfant+= this.Enfant.selectedIndex;
         this.nbrBebe += this.Bebe.selectedIndex;

         this.TnbrAdulte[cmp]=this.Adulte.selectedIndex;
         this.TnbrEnfant[cmp]=this.Enfant.selectedIndex;
         this.TnbrBebe[cmp]=this.Bebe.selectedIndex;

         cmp+=1;

        let chambre= new Chambre();
      chambre.num=j+1+"";
      
      console.log(chambre)
      this.TChambre.push(chambre);
      }

      this.TAdulte=new Array(this.nbrAdulte)
      this.TEnfant=new Array(this.nbrEnfant)
      this.TBebe=new Array(this.nbrBebe)

    }
    let decoded : any = jwt_decode(localStorage.getItem('token'));
    this.reservation.IdClient=decoded.id+"";
    this.reservation.Prix=this.prix;
    this.reservation.Titre=this.first.titre;
    this.reservation.DateReservation= new Date().toString();
    
    this.reservation.DateDepart=formatDate(this.datedepart, 'yyyy-MM-dd', 'en');
    this.reservation.DatRetour=formatDate(this.dateretour, 'yyyy-MM-dd', 'en');
    this.reservation.Nom=this.reactiveform.value.Nom;
    this.reservation.Email=this.reactiveform.value.Email;
    this.reservation.Tel=this.reactiveform.value.Tel;
    console.log(this.reservation.Nom);
  }

  test(){

    this.TAdulte=new Array(this.nbrAdulte-1)
    this.TEnfant=new Array(this.nbrEnfant-1)
    this.TBebe=new Array(this.nbrBebe-1)
    let j=1;
    let k = 0;
    let t = [];

    for(let i=0;i<this.nbrAdulte;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('Adulte'+i);
      
      if(j<this.TnbrAdulte[k] ){

        t.push(selectElement.value+"")
        j+=1;

      }
        else{
         
        t.push(selectElement.value+"")
        this.TChambre[k].adulte=t;
        t=[];
        j=1;
        k+=1;

        }
        
      
    }
       
    j=1;
    k = 0;
    t=[];

    for(let i=0;i<this.nbrEnfant;i++){
      console.log(i)
      const selectElement2 = <HTMLSelectElement>document.getElementById('Enfant'+i);
      
        if(j<this.TnbrEnfant[k] ){

          t.push(selectElement2.value+"")
          j+=1;
  
        }
          else{
           
          t.push(selectElement2.value+"")
          this.TChambre[k].enfant=t;
          t=[];
          j=1;
          k+=1;
          
      }
    }

    j=1;
    k = 0;
    t=[];

    for(let i=0;i<this.nbrBebe;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('Bebe'+i);
      
      if(j<this.TnbrBebe[k] ){

        t.push(selectElement.value+"")
        j+=1;

      }
      else{
         
        t.push(selectElement.value+"")
        this.TChambre[k].bb=t;
        t=[];
        j=1;
        k+=1;
        
    }
        
      
    }
    this.reservation.Chambre=this.TChambre;
  }

  AjoutCambre(){
    this.serviceReservation.AjoutReservation(AjoutReservationUrl,this.reservation).subscribe(        
      response =>{
        console.log(response)
       this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Soiree avec succée');
       
       
      },
      
      
      err =>{this.toastr.error('', 'Verifiez vos Donnees')}
      
    )
  }

}
