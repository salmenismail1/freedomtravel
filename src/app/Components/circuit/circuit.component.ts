import { Component, OnInit } from '@angular/core';
import { CircuitService } from '../../Services/circuit.service';
import { Circuit } from '../../Models/circuit';


@Component({
  selector: 'app-circuit',
  templateUrl: './circuit.component.html',
  styleUrls: ['./circuit.component.css']
})
export class CircuitComponent implements OnInit {
  circuit = new Circuit;
  ListeCircuits;

  constructor(private circuitServices : CircuitService) { }

  ngOnInit() {
    this.circuitServices.getCircuit().subscribe(        
      response =>{
        this.ListeCircuits=response;
        console.log(response)
      },
      err =>{"error"}
    )
  }

}
