import { Component, OnInit } from '@angular/core';
import { ContactService } from '../../Services/contact.service';
import { Contact } from '../../Models/contact';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contact=new Contact;

  constructor(private contactserv:ContactService) { }


  ngOnInit() {
  }

  afficher(){
    console.log(this.contact);
    this.contactserv.postContact(this.contact).subscribe(
      response =>{
        console.log(response)
      },
      err =>{"error"}
    )
  }
  

}
