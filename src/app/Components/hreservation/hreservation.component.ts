import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelsService } from '../../Services/hotels.service';
import {formatDate } from '@angular/common';
import { NgbModule, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { ReservationHotel } from '../../Models/reservation-hotel';
import { ViewChild } from '@angular/core';
import { Hotels } from '../../Models/hotels';
import { Observable } from 'rxjs';
import { Caroussel } from '../../Models/caroussel';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Reservation } from '../../Models/reservation';
import { ReservationService } from '../../Services/reservation.service';
import { Chambre } from '../../Models/chambre';
import { DatePipe } from '@angular/common';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import * as jwt_decode from 'jwt-decode';
import * as html2pdf  from 'html2pdf.js';
import { first } from 'rxjs/operators';

const AjoutReservationUrl = "http://localhost:3000/hotel/api/reservation";
const  url ="https://freedomtravel.tn/json/carouselHotel.php?id=";
const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};

@Component({
  selector: 'app-hreservation',
  templateUrl: './hreservation.component.html',
  styleUrls: ['./hreservation.component.css']
})

export class HreservationComponent implements OnInit {
  @ViewChild('ngcarousel', { static: true }) ngCarousel: NgbCarousel;

  hotelreserver ;
  reservation = new Reservation;
  reactiveform :FormGroup;
  d;

  id;
  obj;
  ListeHotels;  Listimg;  ListeReservation

  
  p;
  nb;
  prix : number;
  venteadultesingle:number;venteadultedble:number;tarifbebe:number;
  datedepart;dateretour;
  
  TChambre;nbChambre = new Array(5);

  Adulte;Enfant;Bebe;nbrAdulte ; nbrEnfant ; nbrBebe; TAdulte ; TEnfant; TBebe;TnbrAdulte;TnbrEnfant;TnbrBebe;
 
  
constructor(private router:Router,private toastr: ToastrService,private Activate:ActivatedRoute,private service:HotelsService,private httpClient:HttpClient,private serviceReservation:ReservationService) { }

  ngOnInit() {
    this.nb=4;

    this.id=this.Activate.snapshot.paramMap.get("id");

    this.obj=formatDate(new Date(), 'yyyy/MM/dd', 'en');

    this.reactiveform= new FormGroup({
      'Nom' : new FormControl('',[Validators.required]),
      'Email' : new FormControl('', [Validators.email,Validators.required]),
      'Tel' : new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern('[0-9]*')]),
      
    })
    
  
 

this.service.getHotel().subscribe(        
  response =>{
    
    this.ListeHotels=response;
    console.log(response)
  },
  err =>{"error"}
)


  this.getImg().subscribe(        
  response =>{
    this.Listimg=response;
    console.log(response)
  },
  err =>{"error"})

  
  }
  get Nom() { return this.reactiveform.get('Nom'); }
  get Email() { return this.reactiveform.get('Email'); }
  get Tel() { return this.reactiveform.get('Tel'); }

  getImg():Observable<Caroussel>{
    
    return this.httpClient.get<Caroussel>(url+this.id,httpOptions);
  }
 

  
 
  chambre(){
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);

      if(selectElement.hidden==true){
        selectElement.hidden=false;
        return;
      }
    }
  }
  delchambre(i:number){
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
    selectElement.hidden=true;

    const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
    select1.selectedIndex=0;

    const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
    select2.selectedIndex=0;

    const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
    select3.selectedIndex=0;


  }

  

  
  
  calculPrix(){
    this.prix=0;
    let prix;
    let prixtElement = <HTMLSelectElement>document.getElementById('prix');
    let typePrix = prixtElement.selectedIndex;


    for(let i=0;i<this.ListeHotels.length;i++){
      
      if(this.ListeHotels[i].id==this.id){
          this.hotelreserver =this.ListeHotels[i];
          if(typePrix ==1){ prix=this.hotelreserver.allinsoftvente ;}
          if(typePrix ==2){prix=this.hotelreserver.allinvente;}
          if(typePrix ==3){prix=this.hotelreserver.dpvente;}
          if(typePrix ==4){prix=this.hotelreserver.lpdvente;}
          if(typePrix ==5){prix=this.hotelreserver.pcvente;}
          if(typePrix ==6){prix=this.hotelreserver.ultraallinvente;}
      }

      console.log(prix)
      

    }
    
    console.log(this.hotelreserver);
   
    for(let j=0;j<this.nb;j++){
      
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+j);

      if(selectElement.hidden==false){
        
        const select1 = <HTMLSelectElement>document.getElementById('homme'+j);
        const select2 = <HTMLSelectElement>document.getElementById('enfant'+j);
        const select3 = <HTMLSelectElement>document.getElementById('bebe'+j);
        

        this.prix+= (select1.selectedIndex*prix)+( select2.selectedIndex*prix)+(select3.selectedIndex*prix)

        console.log(this.prix);
      }
    }
    console.log(this.prix);
  }

  clear() {

    this.reactiveform.reset();
    
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
      selectElement.hidden=true;
      
        const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
        select1.selectedIndex=0;
        const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
        select2.selectedIndex=0;
        const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
        select3.selectedIndex=0;

    }
    const firstElement = <HTMLSelectElement>document.getElementById('rowid'+0);
      firstElement.hidden=false;
    
  }
  affiche(){

    this.TnbrAdulte= new Array();  this.TnbrEnfant= new Array();  this.TnbrBebe= new Array();
    this.nbrAdulte=0; this.nbrEnfant=0; this.nbrBebe=0;
    this.TChambre=[] ;
    let cmp = 0;
    
    for(let j=0;j<this.nb;j++){
      
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+j);

      
      if(selectElement.hidden==false){
        
         this.Adulte = <HTMLSelectElement>document.getElementById('homme'+j);
         this.Enfant = <HTMLSelectElement>document.getElementById('enfant'+j);
         this.Bebe = <HTMLSelectElement>document.getElementById('bebe'+j);

         this.nbrAdulte +=this.Adulte.selectedIndex ;
         this.nbrEnfant+= this.Enfant.selectedIndex;
         this.nbrBebe += this.Bebe.selectedIndex;

         this.TnbrAdulte[cmp]=this.Adulte.selectedIndex;
         this.TnbrEnfant[cmp]=this.Enfant.selectedIndex;
         this.TnbrBebe[cmp]=this.Bebe.selectedIndex;

         cmp+=1;

        let chambre= new Chambre();
      chambre.num=j+1+"";
      
      console.log(chambre)
      this.TChambre.push(chambre);
      }

      this.TAdulte=new Array(this.nbrAdulte)
      this.TEnfant=new Array(this.nbrEnfant)
      this.TBebe=new Array(this.nbrBebe)

    }
    let decoded : any = jwt_decode(localStorage.getItem('token'));
    this.reservation.IdClient=decoded.id+"";
    this.reservation.Prix=this.prix;
    this.reservation.Titre=this.hotelreserver.nom;
    this.reservation.DateReservation= new Date().toString();
    
    this.reservation.DateDepart=this.d[0];
    this.reservation.DatRetour=this.d[1];
    this.reservation.Nom=this.reactiveform.value.Nom;
    this.reservation.Email=this.reactiveform.value.Email;
    this.reservation.Tel=this.reactiveform.value.Tel;
    console.log(this.reservation.Nom);
  }

  test(){

    this.TAdulte=new Array(this.nbrAdulte-1)
    this.TEnfant=new Array(this.nbrEnfant-1)
    this.TBebe=new Array(this.nbrBebe-1)
    let j=1;
    let k = 0;
    let t = [];

    for(let i=0;i<this.nbrAdulte;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('Adulte'+i);
      
      if(j<this.TnbrAdulte[k] ){

        t.push(selectElement.value+"")
        j+=1;

      }
        else{
         
        t.push(selectElement.value+"")
        this.TChambre[k].adulte=t;
        t=[];
        j=1;
        k+=1;

        }
        
      
    }
       
    j=1;
    k = 0;
    t=[];

    for(let i=0;i<this.nbrEnfant;i++){
      console.log(i)
      const selectElement2 = <HTMLSelectElement>document.getElementById('Enfant'+i);
      
        if(j<this.TnbrEnfant[k] ){

          t.push(selectElement2.value+"")
          j+=1;
  
        }
          else{
           
          t.push(selectElement2.value+"")
          this.TChambre[k].enfant=t;
          t=[];
          j=1;
          k+=1;
          
      }
    }

    j=1;
    k = 0;
    t=[];

    for(let i=0;i<this.nbrBebe;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('Bebe'+i);
      
      if(j<this.TnbrBebe[k] ){

        t.push(selectElement.value+"")
        j+=1;

      }
      else{
         
        t.push(selectElement.value+"")
        this.TChambre[k].bb=t;
        t=[];
        j=1;
        k+=1;
        
    }
        
      
    }
    this.reservation.Chambre=this.TChambre;
  }

  AjoutCambre(){
    this.serviceReservation.AjoutReservation(AjoutReservationUrl,this.reservation).subscribe(        
      response =>{
        console.log(response)
       this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Voyage avec succée');
       
       
      },
      
      
      err =>{this.toastr.error('', 'Verifiez vos Donnees')}
      
    )
  }

  pdf(){
    const options = {
      margin:       15,
      filename:     'myfile.pdf',
      image:        { type: 'jpeg' },
      jsPDF:        {orientation: 'landscape' }
    };
    var content = document.getElementById('content');

    html2pdf()
      .from(content)
      .set(options)
      .save();
  }
  
}