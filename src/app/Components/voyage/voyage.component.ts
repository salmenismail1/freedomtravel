import { Component, OnInit } from '@angular/core';
import { VoyagesService } from '../../Services/voyages.service';
import { Voyages } from '../../Models/voyages';

@Component({
  selector: 'app-voyage',
  templateUrl: './voyage.component.html',
  styleUrls: ['./voyage.component.css']
})
export class VoyageComponent implements OnInit {
  voyage = new Voyages;
  ListeVoyages;
  min;max;test="";text;d;

  constructor(private voyageService:VoyagesService) { }

  ngOnInit() {
    this.voyageService.getVoyage(this.voyage).subscribe(        
      response =>{
        this.ListeVoyages=response;
        console.log(response)
      },
      err =>{"error"}
    )
  }
  testing1(){
    console.log(this.test);
    this.test= ""; 
    this.reset();
  }
  
    testing4(){
      this.test="prix";
      console.log("min : "+this.min+"max : "+this.max);
      this.test= "f" ;
      this.text= ""; 
    }
    reset(){
      this.min=0;
      this.max=3000;

    }
}
