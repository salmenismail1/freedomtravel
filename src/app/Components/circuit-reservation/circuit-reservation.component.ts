import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Reservation } from '../../Models/reservation';
import { ActivatedRoute } from '@angular/router';
import { ReservationService } from '../../Services/reservation.service';
import { formatDate } from '@angular/common';
import { Chambre } from '../../Models/chambre';
import { Observable } from 'rxjs';
import { Circuit } from '../../Models/circuit';
import { ToastrService } from 'ngx-toastr';
import * as html2pdf  from 'html2pdf.js';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";



const AjoutReservationUrl = "http://localhost:3000/Soiree/api/reservation";

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};


@Component({
  selector: 'app-circuit-reservation',
  templateUrl: './circuit-reservation.component.html',
  styleUrls: ['./circuit-reservation.component.scss']
})
export class CircuitReservationComponent implements OnInit {
 
  reactiveform;RoomReactiveform :FormGroup;
  reservation = new Reservation;
  ListeReservation
  adult;enfant ;bb;
  id;
  unCircuit;
  circuit;
  first;
  nb;
  e;
  TChambre;nbChambre = new Array(5);
  prix : number;
  venteadultesingle:number;venteadultedble:number;tarifbebe:number;
  datedepart;dateretour;

  Adulte;Enfant;Bebe;nbrAdulte ; nbrEnfant ; nbrBebe; TAdulte ; TEnfant; TBebe;TnbrAdulte;TnbrEnfant;TnbrBebe;


  constructor(private toastr: ToastrService,private Activate:ActivatedRoute,private httpClient:HttpClient,private serviceReservation:ReservationService) { }
 


  ngOnInit() {

    this.reactiveform= new FormGroup({
      'Nom' : new FormControl('',[Validators.required]),
      'Email' : new FormControl('', [Validators.email,Validators.required]),
      'Tel' : new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern('[0-9]*')]),
      
    })

    this.nb=4;
    this.id =this.Activate.snapshot.paramMap.get("id");
    this.unCircuit= "https://topresa.ovh/api/circuits.json?departcircuits.date[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en')+'&soiree.id=' +this.id;
    
    this.getUnCircuit().subscribe(        
      response =>{
        this.first=response[0];
        
        this.circuit=response;
        console.log(response)
      }
    )
  }

  
  get Nom() { return this.reactiveform.get('Nom'); }
  get Email() { return this.reactiveform.get('Email'); }
  get Tel() { return this.reactiveform.get('Tel'); }
  get input() { return this.RoomReactiveform.get('required'); }


  getUnCircuit():Observable<Circuit>{
    return this.httpClient.get<Circuit>(this.unCircuit,httpOptions );

  }


  chambre(){
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);

      if(selectElement.hidden==true){
        selectElement.hidden=false;
        console.log(selectElement);
        console.log(this.adult)
        return;
      }
    }
  }
  delchambre(i:number){
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
    selectElement.hidden=true;

    const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
    select1.selectedIndex=0;

    const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
    console.log(select2.selectedIndex)
    select2.selectedIndex=0;

    const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
    select3.selectedIndex=0;
    console.log(selectElement);

    console.log(this.adult)
   

  }

 
  ngAfterViewInit(): void {
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+0);
    
    selectElement.hidden=false;
  
    console.log(selectElement);
    console.log(this.adult)
  }


  pdf(){
    const options = {
      margin:       15,
      filename:     'myfile.pdf',
      image:        { type: 'jpeg' },
      jsPDF:        {orientation: 'landscape' }
    };
    var content = document.getElementById('content');

    html2pdf()
      .from(content)
      .set(options)
      .save();
  }


  affiche(){

    this.TnbrAdulte= new Array();  this.TnbrEnfant= new Array();  this.TnbrBebe= new Array();
    this.nbrAdulte=0; this.nbrEnfant=0; this.nbrBebe=0;
    this.TChambre=[] ;
    let cmp = 0;
    
    for(let j=0;j<this.nb;j++){
      
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+j);

      
      if(selectElement.hidden==false){
        
         this.Adulte = <HTMLSelectElement>document.getElementById('homme'+j);
         this.Enfant = <HTMLSelectElement>document.getElementById('enfant'+j);
         this.Bebe = <HTMLSelectElement>document.getElementById('bebe'+j);

         this.nbrAdulte +=this.Adulte.selectedIndex ;
         this.nbrEnfant+= this.Enfant.selectedIndex;
         this.nbrBebe += this.Bebe.selectedIndex;

         this.TnbrAdulte[cmp]=this.Adulte.selectedIndex;
         this.TnbrEnfant[cmp]=this.Enfant.selectedIndex;
         this.TnbrBebe[cmp]=this.Bebe.selectedIndex;

         cmp+=1;

        let chambre= new Chambre();
      chambre.num=j+1+"";
      
      console.log(chambre)
      this.TChambre.push(chambre);
      }

      this.TAdulte=new Array(this.nbrAdulte)
      this.TEnfant=new Array(this.nbrEnfant)
      this.TBebe=new Array(this.nbrBebe)

    }
    this.reservation.Prix=this.prix;
    //this.reservation.Titre=this.x.titre;
    this.reservation.DateReservation= new Date().toString();
    
    this.reservation.DateDepart=formatDate(this.datedepart, 'yyyy-MM-dd', 'en');
    this.reservation.DatRetour=formatDate(this.dateretour, 'yyyy-MM-dd', 'en');
    this.reservation.Nom=this.reactiveform.value.Nom;
    this.reservation.Email=this.reactiveform.value.Email;
    this.reservation.Tel=this.reactiveform.value.Tel;
    console.log(this.reservation.Nom);
  }

  test(){

    this.TAdulte=new Array(this.nbrAdulte-1)
    this.TEnfant=new Array(this.nbrEnfant-1)
    this.TBebe=new Array(this.nbrBebe-1)
    let j=1;
    let k = 0;
    let t = [];

    for(let i=0;i<this.nbrAdulte;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('Adulte'+i);
      
      if(j<this.TnbrAdulte[k] ){

        t.push(selectElement.value+"")
        j+=1;

      }
        else{
         
        t.push(selectElement.value+"")
        this.TChambre[k].adulte=t;
        t=[];
        j=1;
        k+=1;

        }
        
      
    }
       
    j=1;
    k = 0;
    t=[];

    for(let i=0;i<this.nbrEnfant;i++){
      console.log(i)
      const selectElement2 = <HTMLSelectElement>document.getElementById('Enfant'+i);
      
        if(j<this.TnbrEnfant[k] ){

          t.push(selectElement2.value+"")
          j+=1;
  
        }
          else{
           
          t.push(selectElement2.value+"")
          this.TChambre[k].enfant=t;
          t=[];
          j=1;
          k+=1;
          
      }
    }

    j=1;
    k = 0;
    t=[];

    for(let i=0;i<this.nbrBebe;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('Bebe'+i);
      
      if(j<this.TnbrBebe[k] ){

        t.push(selectElement.value+"")
        j+=1;

      }
      else{
         
        t.push(selectElement.value+"")
        this.TChambre[k].bb=t;
        t=[];
        j=1;
        k+=1;
        
    }
        
      
    }
    this.reservation.Chambre=this.TChambre;
  }

  /*AjoutCambre(){
    this.serviceReservation.AjoutReservation(AjoutReservationUrl,this.reservation).subscribe(        
      response =>{
        console.log(response)
       this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Voyage avec succée');
       
       
      },
      
      
      err =>{this.toastr.error('', 'Verifiez vos Donnees')}
      
    )
  }*/


}
