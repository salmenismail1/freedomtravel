import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs'
import { User } from '../../../../../Models/user';
import { Router } from '@angular/router';
import { LoginService } from '../../../../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
import { AppVarsGlobal } from '../../../../../app.vars.global';

@Component({
  selector: 'app-userliste',
  templateUrl: './userliste.component.html',
  styleUrls: ['./userliste.component.scss']
})
export class UserlisteComponent implements OnInit {

  
  constructor(private userServices: LoginService,public matDialog: MatDialog,private router:Router,private vars: AppVarsGlobal) { } 

  listeAdmin: MatTableDataSource<any>;
  listeclient : MatTableDataSource<any>;
  listePayement : MatTableDataSource<any>;

  
  AdminColumns: string[] = ['nom', 'cin','tel','type','actions'];
  ClientColumns: string[] = ['nom', 'cin','tel','type','actions'];
  HistoriqueColumns: string[] = ['Titre', 'Etat','Nom','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  
  searchKey;searchKey2;searchKey3;

  LAdmin ;LClient ;LPayement;

  userType
  ngOnInit() {

    
    this.userType=this.vars.type;
    console.log(this.userType)

    

    this.GetListeAdmin();
    this.GetListeClient();
    this.GetListePayement();
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onSearchClear2() {
    this.searchKey2 = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey3 = "";
    this.applyFilter3();
  }
  

  applyFilter() {
    this.listeAdmin.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter2() {
    this.listeclient.filter = this.searchKey2.trim().toLowerCase();
  }
  applyFilter3() {
    this.listePayement.filter = this.searchKey3.trim().toLowerCase();
  }
 


  DeleteUser(id:string){
    console.log(id);
    this.userServices.DeleteUser(id).subscribe();
  
}

  

  GetListeClient(){
    this.userServices.getClient().subscribe(        
      response =>{
        this.LClient=response;
        this.listeclient = new MatTableDataSource(this.LClient);
        this.listeclient.sort = this.sort;
        this.listeclient.paginator = this.paginator;
        this.listeclient.filterPredicate = (data, filter) => {
          return this.ClientColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
        console.log(response);
      },
      err =>{"error"}
    )
    
      
  }

  GetListePayement(){

    var decoded = jwt_decode(this.userServices.getToken());
    this.userServices.getHistoriquee().subscribe(        
      response =>{
        this.LPayement=response;
        this.listePayement = new MatTableDataSource(this.LPayement);
        this.listePayement.sort = this.sort;
        this.listePayement.paginator = this.paginator;
        this.listePayement.filterPredicate = (data, filter) => {
          return this.HistoriqueColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
        console.log(response);
      },
      err =>{"error"}
    )
    
      
  

  }

  GetListeAdmin(){
    this.userServices.getAdmin().subscribe(        
      response =>{
        this.LAdmin=response;
        this.listeAdmin = new MatTableDataSource(this.LAdmin);
        this.listeAdmin.sort = this.sort;
        this.listeAdmin.paginator = this.paginator;
        this.listeAdmin.filterPredicate = (data, filter) => {
          return this.AdminColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
  
      }
        console.log(response);
      },
      err =>{"error"}
    )
    
      
  }

  UpgradeToAdmin(user:User){

    this.userServices.UpgradeToAdmin(user).subscribe();
    this.GetListeAdmin();
    this.GetListeClient();

  }
  DowngradeToClient(user:User){
    this.userServices.DowngradeToClient(user).subscribe();
    this.GetListeAdmin();
    this.GetListeClient();
  }

  
  

}

