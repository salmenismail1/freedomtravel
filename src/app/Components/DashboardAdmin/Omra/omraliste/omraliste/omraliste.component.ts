import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../../../../../../app/Models/user';
import { LoginService } from '../../../../../Services/login.service';

@Component({
  selector: 'app-omraliste',
  templateUrl: './omraliste.component.html',
  styleUrls: ['./omraliste.component.scss']
})
export class OmralisteComponent implements OnInit {

  constructor(public matDialog: MatDialog,private router:Router,private userServices: LoginService) { }

  ngOnInit() {
  }

  UpgradePayement(user:User){

    this.userServices.UpgradePayement(user).subscribe();
    //this.GetListeReservationAccepter();
  }
  

}
