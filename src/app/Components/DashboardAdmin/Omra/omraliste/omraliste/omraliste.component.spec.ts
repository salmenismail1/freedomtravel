import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OmralisteComponent } from './omraliste.component';

describe('OmralisteComponent', () => {
  let component: OmralisteComponent;
  let fixture: ComponentFixture<OmralisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OmralisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OmralisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
