import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { AddCircuitComponent as ModalComponent } from '../add-circuit/add-circuit.component';
import { CircuitmodifComponent as ModalComponent2 } from '../circuitmodif/circuitmodif.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Circuit } from '../../../../Models/circuit';
import { CircuitService } from '../../../../Services/circuit.service';

@Component({
  selector: 'app-circuitlist',
  templateUrl: './circuitlist.component.html',
  styleUrls: ['./circuitlist.component.scss']
})
export class CircuitlistComponent implements OnInit {

  constructor(private circuitServices: CircuitService,public matDialog: MatDialog,private router:Router) { } 

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['id', 'nom','actions'];
  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey: string;
  l: Circuit[];
  private postsSub: Subscription;
  ngOnInit() {
    
    this.getData();
     
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component-voyage";
    const g=this.matDialog.open(ModalComponent, {
      height: '500px',
      width: '500px',
    });
    g.afterClosed().subscribe(() => {
      // Do stuff after the dialog has closed
      this.getData();
  });

  }
  openModifModal(item:any){
    console.log(item);
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modifmodal-component-voyage";
    const g=this.matDialog.open(ModalComponent2, {
      height: '500px',
      width: '500px',
      data: { comp: item },
    });
    
    g.afterClosed().subscribe(() => {

      // Do stuff after the dialog has closed
      this.getData();
     

  });
  
  console.log(item);

  }

delete(id:string){


this.circuitServices.deletePost(id);
this.postsSub = this.circuitServices.getPostUpdateListener()
.subscribe((posts: Circuit[]) => {
  this.l = posts;
});
setTimeout(() => 
{
  console.log(this.l);
  this.listData = new MatTableDataSource(this.l);
  this.listData.sort = this.sort;
  this.listData.paginator = this.paginator;
  this.listData.filterPredicate = (data, filter) => {
    return this.displayedColumns.some(ele => {
      return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
    });
  };

},
5000);
}

  getData(){
    this.circuitServices.getCircuitBase();
    this.postsSub = this.circuitServices.getPostUpdateListener()
      .subscribe((posts: Circuit[]) => {
        this.l = posts;
      });
      setTimeout(() => 
      {
        console.log(this.l);
        this.listData = new MatTableDataSource(this.l);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        };
  
      },
      4000);
  }
}
