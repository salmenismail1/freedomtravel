import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircuitlistComponent } from './circuitlist.component';

describe('CircuitlistComponent', () => {
  let component: CircuitlistComponent;
  let fixture: ComponentFixture<CircuitlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircuitlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircuitlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
