import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircuitmodifComponent } from './circuitmodif.component';

describe('CircuitmodifComponent', () => {
  let component: CircuitmodifComponent;
  let fixture: ComponentFixture<CircuitmodifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircuitmodifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircuitmodifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
