import { Component, OnInit, Inject, ɵɵqueryRefresh } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HotelsService } from '../../../../Services/hotels.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SoireesService } from '../../../../Services/soirees.service';

@Component({
  selector: 'app-soireemodif',
  templateUrl: './soireemodif.component.html',
  styleUrls: ['./soireemodif.component.scss']
})
export class SoireemodifComponent implements OnInit {


  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  constructor(public dialogRef: MatDialogRef<SoireemodifComponent>,private soireeServices:SoireesService,private http: HttpClient,@Inject(MAT_DIALOG_DATA) public data:any){}

  ngOnInit(){
    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      titre: new FormControl(null, { validators: [Validators.required] }),
      date: new FormControl(null, { validators: [Validators.required] }),
      venteadultesingle: new FormControl(null, { validators: [Validators.required] }),
      venteadultedble: new FormControl(null, { validators: [Validators.required] }),
      vente3rdad: new FormControl(null, { validators: [Validators.required] }),
      venteenfant2ad: new FormControl(null, { validators: [Validators.required] }),
      tarifbebe: new FormControl(null, { validators: [Validators.required] }),
      venteenfant1ad: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectImage(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
      if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);
      
      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);

      }


    }

  }
  

/*
  selectMultipleImage(event){
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      this.form.patchValue({ image: multipleImages });
        this.form.get("image").updateValueAndValidity();
        this.images = multipleImages;
     
      for(let item of this.multipleImages){
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview = reader.result as string;
        };
        reader.readAsDataURL(item);
      if(item.type=="image/png" || item.type=="image/jpeg" || item.type=="image/jpg" || item.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);
      
      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);
        return;

      }
    }
    }

  }
  */

  onSubmit(idg:string){
    console.log(idg);
    if(this.form.value.id!="" || this.form.value.titre!="" || this.form.value.date!="" || this.form.value.venteadultesingle!="" ||
    this.form.value.venteadultedble!="" || this.form.value.vente3rdad!="" || this.form.value.venteenfant2ad!="" || this.form.value.name!="" || this.imageverif==true
    || this.form.value.tarifbebe!="" || this.form.value.venteenfant1ad!=""
    ){

      this.soireeServices.update(this.form.value.id,this.form.value.titre,
        this.form.value.venteadultesingle,this.form.value.venteadultedble,this.form.value.date,this.images.name,
        this.form.value.vente3rdad , this.form.value.venteenfant2ad,this.form.value.tarifbebe,this.form.value.venteenfant1ad,idg);
      const formData = new FormData();
      formData.append('file', this.images);
      console.log(formData);
      this.http.post<any>("http://localhost:3000/soiree/file", formData).subscribe(
        (res) => console.log(res),
        (err) => console.log(err)
      );
      /*ng serve --live-reload false
      location.reload();
            window.stop();

      */
     


    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  onMultipleSubmit(){
    const formData = new FormData();
    for(let img of this.multipleImages){
      formData.append('files', img);
    }

    this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }


  close(){
        this.dialogRef.close();


  }

}
