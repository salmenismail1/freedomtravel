import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSoireeComponent } from './add-soiree.component';

describe('AddSoireeComponent', () => {
  let component: AddSoireeComponent;
  let fixture: ComponentFixture<AddSoireeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSoireeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSoireeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
