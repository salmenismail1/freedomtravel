import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SoireesService } from '../../../../Services/soirees.service';

@Component({
  selector: 'app-add-soiree',
  templateUrl: './add-soiree.component.html',
  styleUrls: ['./add-soiree.component.scss']
})
export class AddSoireeComponent implements OnInit {

  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  constructor(public dialogRef: MatDialogRef<AddSoireeComponent>,private soireeServices:SoireesService,private http: HttpClient){}
  
      
  ngOnInit(){
    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      titre: new FormControl(null, { validators: [Validators.required] }),
      date: new FormControl(null, { validators: [Validators.required] }),
      venteadultesingle: new FormControl(null, { validators: [Validators.required] }),
      venteadultedble: new FormControl(null, { validators: [Validators.required] }),
      vente3rdad: new FormControl(null, { validators: [Validators.required] }),
      venteenfant2ad: new FormControl(null, { validators: [Validators.required] }),
      tarifbebe: new FormControl(null, { validators: [Validators.required] }),
      venteenfant1ad: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectImage(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
      if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);
      
      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);

      }


    }

  }
  


  selectMultipleImage(event){
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
    }

  }
  titre:string;
  image :string;
  date:string;
  venteadultesingle:number;
  venteadultedble:number;
  vente3rdad:number;
  venteenfant2ad:number;
  tarifbebe:number;
  venteenfant1ad:number    
  onSubmit(){
    if(this.form.value.id!="" && this.form.value.titre!="" && this.form.value.date!="" && this.form.value.venteadultesingle!="" &&
    this.form.value.venteadultedble!="" && this.form.value.vente3rdad!="" && this.form.value.venteenfant2ad!="" && this.form.value.name!="" && this.imageverif==true
    && this.form.value.tarifbebe!="" && this.form.value.venteenfant1ad!=""
    ){

      this.soireeServices.addPost(this.form.value.id,this.form.value.titre,
        this.form.value.venteadultesingle,this.form.value.venteadultedble,this.form.value.date,this.images.name,this.form.value.vente3rdad , this.form.value.venteenfant2ad,this.form.value.tarifbebe,this.form.value.venteenfant1ad);
      const formData = new FormData();
      formData.append('file', this.images);
      console.log(formData);
      this.http.post<any>("http://localhost:3000/soiree/file", formData).subscribe(
        (res) => console.log(res),
        (err) => console.log(err)
      );


    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  onMultipleSubmit(){
    const formData = new FormData();
    for(let img of this.multipleImages){
      formData.append('files', img);
    }

    this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
  close(){
        this.dialogRef.close();


  }
}
