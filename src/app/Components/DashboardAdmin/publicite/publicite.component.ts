import { Component, OnInit, ViewChild } from '@angular/core';

import { HotelsService } from '../../../Services/hotels.service';
import { Hotels } from '../../../Models/hotels';
import { Router } from '@angular/router';
import { Voyages } from '../../../Models/voyages';
import { VoyagesService } from '../../../Services/voyages.service';
import { Soiree } from '../../../Models/soiree';
import { SoireesService } from '../../../Services/soirees.service';
import { OmraService } from '../../../Services/omra.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { LoginService } from '../../../Services/login.service';
import { CircuitService } from '../../../Services/circuit.service';
import { Circuit } from '../../../Models/circuit';

@Component({
  selector: 'app-publicite',
  templateUrl: './publicite.component.html',
  styleUrls: ['./publicite.component.scss']
})
export class PubliciteComponent implements OnInit {

  constructor(private hotelServices: HotelsService,private userServices: LoginService,private voyageServices: VoyagesService,private soireeServices: SoireesService,private circuitServices: CircuitService,) { }

  private postsSub: Subscription;

  ngOnInit() {
    this.getHotels();
    this.getVoyages();
    this.getCircuit();
    this.getSoiree();
  }


  listeReservationAccepter: MatTableDataSource<any>;
  HistoriqueColumns: string[] = ['Nom','Etat','actions'];
  displayedColumns: string[] = ['id', 'nom','actions'];
  displayedColumns2: string[] = ['id', 'titre','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey;searchKey2;searchKey3;searchKey4;searchKey5;
  LReservationAccepter;
  choice;

  listeVoyage: MatTableDataSource<any>;
  listeSoiree: MatTableDataSource<any>;
  listeOmra: MatTableDataSource<any>;
  listeHotel: MatTableDataSource<any>;
  listeCircuit: MatTableDataSource<any>;
  

  LHotels;LVoyage;LOmra;Lsoiree;Lcircuit;

  

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onSearchClear2() {
    this.searchKey = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey = "";
    this.applyFilter3();
  }
  onSearchClear4() {
    this.searchKey = "";
    this.applyFilter4();
  }
  onSearchClear5() {
    this.searchKey = "";
    this.applyFilter5();
  }

  applyFilter() {
    this.listeHotel.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter2() {
    this.listeVoyage.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter3() {
    this.listeSoiree.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter4() {
    this.listeCircuit.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter5() {
    this.listeReservationAccepter.filter = this.searchKey.trim().toLowerCase();
  }

  Email(ch:string,hotel:any){    

    this.userServices.getClient().subscribe(        
      response =>{
        for(let i=0;i<response.length;i++){

          this.hotelServices.send(response[i].email,hotel).subscribe(        
            response =>{
              console.log(response)
          });
        }
      },
      err =>{"error"}
    )
    const selectElement = <HTMLSelectElement>document.getElementById("b"+ch);
    selectElement.disabled=true;
  }
  Email2(ch:string,hotel:any){    

    this.userServices.getClient().subscribe(        
      response =>{
        for(let i=0;i<response.length;i++){

          this.voyageServices.send(response[i].email,hotel).subscribe(        
            response =>{
              console.log(response)
          });
        }
      },
      err =>{"error"}
    )
    const selectElement = <HTMLSelectElement>document.getElementById("b"+ch);
    selectElement.disabled=true;
  }

  Email3(ch:string,hotel:any){    

    this.userServices.getClient().subscribe(        
      response =>{
        for(let i=0;i<response.length;i++){

          this.soireeServices.send(response[i].email,hotel).subscribe(        
            response =>{
              console.log(response)
          });
        }
      },
      err =>{"error"}
    )
    const selectElement = <HTMLSelectElement>document.getElementById("b"+ch);
    selectElement.disabled=true;
  }

  Email4(ch:string,hotel:any){    

    this.userServices.getClient().subscribe(        
      response =>{
        for(let i=0;i<response.length;i++){

          this.circuitServices.send(response[i].email,hotel).subscribe(        
            response =>{
              console.log(response)
          });
        }
      },
      err =>{"error"}
    )
    const selectElement = <HTMLSelectElement>document.getElementById("b"+ch);
    selectElement.disabled=true;
  }

  getHotels(){
    this.hotelServices.getHotelBase();
    this.postsSub = this.hotelServices.getPostUpdateListener()
      .subscribe((posts: Hotels[]) => {
        this.LHotels = posts;
        
        console.log(this.LHotels);
        this.listeHotel = new MatTableDataSource(this.LHotels);
        this.listeHotel.sort = this.sort;
        this.listeHotel.paginator = this.paginator;
        this.listeHotel.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        };
      });  
  }

  getVoyages(){
    this.voyageServices.getVoyageBase();
    this.postsSub = this.voyageServices.getPostUpdateListener()
      .subscribe((posts: Voyages[]) => {
        this.LVoyage = posts;

        console.log(this.LVoyage);
        this.listeVoyage = new MatTableDataSource(this.LVoyage);
        this.listeVoyage.sort = this.sort;
        this.listeVoyage.paginator = this.paginator;
        this.listeVoyage.filterPredicate = (data, filter) => {
          return this.displayedColumns2.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
      
          };
        });
  
  }
  
  getSoiree(){
    this.soireeServices.getSoireesBase();
    this.postsSub = this.soireeServices.getPostUpdateListener()
      .subscribe((posts: Soiree[]) => {
        this.Lsoiree = posts;

        console.log(this.Lsoiree);
        this.listeSoiree = new MatTableDataSource(this.Lsoiree);
        this.listeSoiree.sort = this.sort;
        this.listeSoiree.paginator = this.paginator;
        this.listeSoiree.filterPredicate = (data, filter) => {
          return this.displayedColumns2.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        
          
        };
      });

  }

  getCircuit(){
    this.circuitServices.getCircuitBase();
    this.postsSub = this.circuitServices.getPostUpdateListener()
      .subscribe((posts: Circuit[]) => {
        this.Lcircuit = posts;

        console.log(this.Lcircuit);
        this.listeCircuit = new MatTableDataSource(this.Lcircuit);
        this.listeCircuit.sort = this.sort;
        this.listeCircuit.paginator = this.paginator;
        this.listeCircuit.filterPredicate = (data, filter) => {
          return this.displayedColumns2.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        
          
        };
      });



  }

  getOmras(){


  }

  

}
