import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoyagemodifComponent } from './voyagemodif.component';

describe('VoyagemodifComponent', () => {
  let component: VoyagemodifComponent;
  let fixture: ComponentFixture<VoyagemodifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoyagemodifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoyagemodifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
