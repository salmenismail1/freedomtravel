import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { AddVoyageComponent as ModalComponent } from '../add-voyage/add-voyage.component';
import { VoyagemodifComponent as ModalComponent2 } from '../voyagemodif/voyagemodif.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { VoyagesService } from '../../../../Services/voyages.service';
import { Voyages } from '../../../../Models/voyages';
import { User } from '../../../../Models/user';

@Component({
  selector: 'app-voyagelist',
  templateUrl: './voyagelist.component.html',
  styleUrls: ['./voyagelist.component.scss']
})
export class VoyagelistComponent implements OnInit {

  constructor(private voyageServices: VoyagesService,public matDialog: MatDialog,private router:Router) { } 

  listData: MatTableDataSource<any>;
  listeReservationAccepter : MatTableDataSource<any>;
  listeReservationEnAttente : MatTableDataSource<any>;

  displayedColumns: string[] = ['id', 'nom','actions'];
  ReservationHotelColumns: string[] = ['Titre', 'Email','Prix','actions'];
  ReservationHotelColumns2: string[] = ['Titre', 'Email','Prix','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey: string;searchKey2;searchKey3;
  l: Voyages[];LReservationAccepter ;LReservationEnAttente ;
  private postsSub: Subscription;

  

  ngOnInit() {
    
    //this.getData();
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
     
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  onSearchClear2() {
    this.searchKey2 = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey3 = "";
    this.applyFilter3();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  applyFilter2() {
    this.listeReservationAccepter.filter = this.searchKey2.trim().toLowerCase();
  }
  applyFilter3() {
    this.listeReservationEnAttente.filter = this.searchKey3.trim().toLowerCase();
  }


  

  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component-voyage";
    const g=this.matDialog.open(ModalComponent, {
      height: '500px',
      width: '500px',
    });
    g.afterClosed().subscribe(() => {
      // Do stuff after the dialog has closed
      this.getData();
  });

  }
  openModifModal(item:any){
    console.log(item);
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modifmodal-component-voyage";
    const g=this.matDialog.open(ModalComponent2, {
      height: '500px',
      width: '500px',
      data: { comp: item },
    });
    
    g.afterClosed().subscribe(() => {

      // Do stuff after the dialog has closed
      this.getData();
     

  });
  
  console.log(item);

  }

delete(id:string){


this.voyageServices.deletePost(id);
this.postsSub = this.voyageServices.getPostUpdateListener()
.subscribe((posts: Voyages[]) => {
  this.l = posts;
});
setTimeout(() => 
{
  console.log(this.l);
  this.listData = new MatTableDataSource(this.l);
  this.listData.sort = this.sort;
  this.listData.paginator = this.paginator;
  this.listData.filterPredicate = (data, filter) => {
    return this.displayedColumns.some(ele => {
      return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
    });
  };

},
5000);
}

deleteReservationHotel(Titre:string){
  console.log(Titre);
  this.voyageServices.DeleteReservationHotel(Titre).subscribe();
  
}

  getData(){
    this.voyageServices.getVoyageBase();
    this.postsSub = this.voyageServices.getPostUpdateListener()
      .subscribe((posts: Voyages[]) => {
        this.l = posts;
      });
      setTimeout(() => 
      {
        console.log(this.l);
        this.listData = new MatTableDataSource(this.l);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        };
  
      },
      4000);
  }

  GetListeReservationAccepter(){
    this.voyageServices.getReservationAccepter().subscribe(        
      response =>{
        this.LReservationAccepter=response;

        console.log(response);
      },
      err =>{"error"}
    )
    
      setTimeout(() => 
      {
        console.log(this.LReservationAccepter);
        this.listeReservationAccepter = new MatTableDataSource(this.LReservationAccepter);
        this.listeReservationAccepter.sort = this.sort;
        this.listeReservationAccepter.paginator = this.paginator;
        this.listeReservationAccepter.filterPredicate = (data, filter) => {
          console.log("-------"+filter)
          return this.ReservationHotelColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
  
      },
      4000);
  }

  GetListeReservationEnAttente(){
    this.voyageServices.getReservationEnAttent().subscribe(        
      response =>{
        this.LReservationEnAttente=response;

        console.log(response);
      },
      err =>{"error"}
    )
    
      setTimeout(() => 
      {
        console.log(this.LReservationEnAttente);
        this.listeReservationEnAttente = new MatTableDataSource(this.LReservationEnAttente);
        this.listeReservationEnAttente.sort = this.sort;
        this.listeReservationEnAttente.paginator = this.paginator;
        this.listeReservationEnAttente.filterPredicate = (data, filter) => {
          return this.ReservationHotelColumns2.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
  
      },
      40000);
  }

  ReservationAccepter(Titre : string){
    this.voyageServices.ReservationAccepter(Titre).subscribe()
  }

  UpgradePayement(user:User){

    this.voyageServices.UpgradePayement(user).subscribe();
    this.GetListeReservationAccepter();
  }

}
