import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { VoyagesService } from '../../../../Services/voyages.service';


@Component({
  selector: 'app-add-voyage',
  templateUrl: './add-voyage.component.html',
  styleUrls: ['./add-voyage.component.scss']
})
export class AddVoyageComponent implements OnInit {

 /* title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  constructor(public dialogRef: MatDialogRef<AddVoyageComponent>,private voyageServices:VoyagesService,private http: HttpClient){}*/

  ngOnInit(){
   /* this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      nom: new FormControl(null, { validators: [Validators.required] }),
      pays: new FormControl(null, { validators: [Validators.required] }),
      titre: new FormControl(null, { validators: [Validators.required] }),
      prix: new FormControl(null, { validators: [Validators.required] }),
      datedepart: new FormControl(null, { validators: [Validators.required] }),
      dateretour: new FormControl(null, { validators: [Validators.required] }),

      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectImage(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
      if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);
      
      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);

      }


    }

  }
  


  selectMultipleImage(event){
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
    }

  }

  onSubmit(){
    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.pays!="" && this.form.value.titre!="" &&
    this.form.value.prix!="" && this.form.value.datedepart!="" && this.form.value.dateretour!="" && this.form.value.name!="" && this.imageverif==true
    ){

      this.voyageServices.addPost(this.form.value.id,this.form.value.nom,this.form.value.pays,
        this.form.value.titre,this.form.value.prix,this.form.value.datedepart , this.form.value.dateretour,this.images.name);
      const formData = new FormData();
      formData.append('file', this.images);
      console.log(formData);
      this.http.post<any>("http://localhost:3000/voyage/file", formData).subscribe(
        (res) => console.log(res),
        (err) => console.log(err)
      );


    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  onMultipleSubmit(){
    const formData = new FormData();
    for(let img of this.multipleImages){
      formData.append('files', img);
    }

    this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
  close(){
        this.dialogRef.close();

*/
  }

}
