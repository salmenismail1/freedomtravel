import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { AddHotelComponent as ModalComponent } from '../add-hotel/add-hotel.component';
import { HotelmodifComponent as ModalComponent2 } from '../hotelmodif/hotelmodif.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { HotelsService } from '../../../../Services/hotels.service';
import { Subscription } from 'rxjs';
import { Hotels } from '../../../../Models/hotels';
import { Router } from '@angular/router';
import { User } from '../../../../../app/Models/user';
import { LoginService } from '../../../../Services/login.service';

@Component({
  selector: 'app-hotellist',
  templateUrl: './hotellist.component.html',
  styleUrls: ['./hotellist.component.scss']
})
export class HotellistComponent implements OnInit {

  constructor(private hotelServices: HotelsService,public matDialog: MatDialog,private router:Router,private userServices: LoginService) { } 

  listData: MatTableDataSource<any>;
  listeReservationAccepter : MatTableDataSource<any>;
  listeReservationEnAttente : MatTableDataSource<any>;

  displayedColumns: string[] = ['id', 'nom','actions'];
  ReservationHotelColumns: string[] = ['Titre', 'Email','Prix','actions'];
  ReservationHotelColumns2: string[] = ['Titre', 'Email','Prix','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  
  searchKey;searchKey2;searchKey3;

  l: Hotels[];
  private postsSub: Subscription;

  LReservationAccepter ;LReservationEnAttente ;
  
  ngOnInit() {
 
    //this.getData();
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onSearchClear2() {
    this.searchKey2 = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey3 = "";
    this.applyFilter3();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter2() {
    this.listeReservationAccepter.filter = this.searchKey2.trim().toLowerCase();
  }
  applyFilter3() {
    this.listeReservationEnAttente.filter = this.searchKey3.trim().toLowerCase();
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    const g=this.matDialog.open(ModalComponent, {
      height: '500px',
      width: '500px',
    });
    g.afterClosed().subscribe(() => {
      // Do stuff after the dialog has closed
      this.getData();

      

  });

  }
  openModifModal(item:any,event){
    console.log(item);
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modifmodal-component";
    const g=this.matDialog.open(ModalComponent2, {
      height: '500px',
      width: '500px',
      data: { comp: item },
    });
    event.pre
    g.afterClosed().subscribe(() => {

      // Do stuff after the dialog has closed
      this.getData();
     

  });
  
  console.log(item);

  }

delete(id:string){


    this.hotelServices.deletePost(id);
    this.postsSub = this.hotelServices.getPostUpdateListener()
    .subscribe((posts: Hotels[]) => {
      this.l = posts;
    });
    setTimeout(() => 
    {
      console.log(this.l);
      this.listData = new MatTableDataSource(this.l);
      this.listData.sort = this.sort;
      this.listData.paginator = this.paginator;
     

    },
    5000);
}


deleteReservationHotel(Titre:string){
  console.log(Titre);
  this.hotelServices.DeleteReservationHotel(Titre).subscribe();
  
}

  getData(){
    this.hotelServices.getHotelBase();
    this.postsSub = this.hotelServices.getPostUpdateListener()
      .subscribe((posts: Hotels[]) => {
        this.l = posts;
      });
      setTimeout(() => 
      {
        console.log(this.l);
        this.listData = new MatTableDataSource(this.l);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        };
  
      },
      4000);
  }
  

  GetListeReservationAccepter(){
    this.hotelServices.getReservationAccepter().subscribe(        
      response =>{
        this.LReservationAccepter=response;

        console.log(response);
      },
      err =>{"error"}
    )
    
      setTimeout(() => 
      {
        console.log(this.LReservationAccepter);
        this.listeReservationAccepter = new MatTableDataSource(this.LReservationAccepter);
        this.listeReservationAccepter.sort = this.sort;
        this.listeReservationAccepter.paginator = this.paginator;
        this.listeReservationAccepter.filterPredicate = (data, filter) => {
          console.log("-------"+filter)
          return this.ReservationHotelColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
  
      },
      4000);
  }

  GetListeReservationEnAttente(){
    this.hotelServices.getReservationEnAttent().subscribe(        
      response =>{
        this.LReservationEnAttente=response;

        console.log(response);
      },
      err =>{"error"}
    )
    
      setTimeout(() => 
      {
        console.log(this.LReservationEnAttente);
        this.listeReservationEnAttente = new MatTableDataSource(this.LReservationEnAttente);
        this.listeReservationEnAttente.sort = this.sort;
        this.listeReservationEnAttente.paginator = this.paginator;
        this.listeReservationEnAttente.filterPredicate = (data, filter) => {
          return this.ReservationHotelColumns2.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
  
      },
      4000);
  }

  ReservationAccepter(Titre : string){
    this.hotelServices.ReservationAccepter(Titre).subscribe()
  }

  UpgradePayement(user:User){

    this.userServices.UpgradePayement(user).subscribe();
    this.GetListeReservationAccepter();
  }


 
}
