import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HotelsService } from '../../../../Services/hotels.service';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-hotel',
  templateUrl: './add-hotel.component.html',
  styleUrls: ['./add-hotel.component.scss']
})
export class AddHotelComponent implements OnInit {

  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  constructor(private _snackBar: MatSnackBar,public dialogRef: MatDialogRef<AddHotelComponent>,private hotelServices:HotelsService,private http: HttpClient){}

  ngOnInit(){
    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      nom: new FormControl(null, { validators: [Validators.required] }),
      content: new FormControl(null, { validators: [Validators.required] }),
      adultOnly: new FormControl(null, { validators: [Validators.required] }),
      ville: new FormControl(null, { validators: [Validators.required] }),
      categorie: new FormControl(null, { validators: [Validators.required] }),
      type: new FormControl(null, { validators: [Validators.required] }),
      lpdvente: new FormControl(null, { validators: [Validators.required] }),
      dpvente: new FormControl(null, { validators: [Validators.required] }),
      pcvente: new FormControl(null, { validators: [Validators.required] }),
      allinsoftvente: new FormControl(null, { validators: [Validators.required] }),
      allinvente: new FormControl(null, { validators: [Validators.required] }),
      ultraallinvente: new FormControl(null, { validators: [Validators.required] }),
      age_enf_gratuit: new FormControl(null, { validators: [Validators.required] }),

      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectImage(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
      if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);
      
      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);

      }


    }
    this._snackBar.open('Cannonball!!', '', {
      duration: 5000,
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });

  }
  


  selectMultipleImage(event){
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
    }

  }

  onSubmit(){
    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.adultOnly!="" && this.form.value.ville!="" &&
    this.form.value.categorie!="" && this.form.value.type!="" && this.form.value.lpdvente!="" && this.form.value.dpvente!="" &&
    this.form.value.pcvente!="" && this.form.value.allinsoftvente!="" && this.form.value.allinvente!="" && this.form.value.ultraallinvente!="" &&
    this.form.value.age_enf_gratuit!="" && this.form.value.name!="" && this.imageverif==true
    ){

      this.hotelServices.addPost(this.form.value.id,this.form.value.nom,this.form.value.adultOnly,
        this.form.value.ville,this.form.value.categorie,this.form.value.type , this.form.value.lpdvente,
         this.form.value.dpvente, this.form.value.pcvente,this.form.value.allinsoftvente,this.form.value.allinvente,
         this.form.value.ultraallinvente,this.form.value.age_enf_gratuit,this.images.name);
      const formData = new FormData();
      formData.append('file', this.images);
      console.log(formData);
      this.http.post<any>("http://localhost:3000/hotel/file", formData).subscribe(
        (res) => console.log(res),
        (err) => console.log(err)
      );


    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  onMultipleSubmit(){
    const formData = new FormData();
    for(let img of this.multipleImages){
      formData.append('files', img);
    }

    this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
  close(){
        this.dialogRef.close();


  }

}
