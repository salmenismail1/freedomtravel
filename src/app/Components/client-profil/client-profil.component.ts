import { Component, OnInit } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { User } from '../../Models/user';
import { LoginService } from '../../Services/login.service';


@Component({
  selector: 'app-client-profil',
  templateUrl: './client-profil.component.html',
  styleUrls: ['./client-profil.component.scss']
})
export class ClientProfilComponent implements OnInit {

  user;
  constructor(private userServices: LoginService) { }

  ngOnInit() {

   /**  var decoded = jwt_decode(this.userServices.getToken());
    this.userServices.getOnUser(decoded.id).subscribe(
      response => {
        
        this.user= response ; 
        console.log(this.user)
    })*/
  }

  ModifierUser(id:string){
    this.userServices.ModifUser(id,this.user).subscribe();
    console.log(this.user)
  }

}
