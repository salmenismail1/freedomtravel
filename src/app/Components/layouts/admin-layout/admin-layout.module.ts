import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../DashboardAdmin/dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { HotellistComponent } from '../../DashboardAdmin/Hotel/hotellist/hotellist.component';
import { VoyagelistComponent } from '../../DashboardAdmin/Voyage/voyagelist/voyagelist.component';
import { SoireelistComponent } from '../../DashboardAdmin/Soiree/soireelist/soireelist.component';
import { CircuitlistComponent } from '../../DashboardAdmin/Circuit/circuitlist/circuitlist.component';
import { HomecontrolComponent } from '../../DashboardAdmin/homecontrol/homecontrol.component';
import { MatTableModule, MatIconModule, MatFormFieldModule, MatPaginatorModule, MatCardModule, MatSidenavModule, MatInputModule, MatToolbarModule, MatMenuModule, MatCheckboxModule, MatTabsModule, MatProgressSpinnerModule, MatGridListModule, MatListModule, MatDividerModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { UserlisteComponent } from '../../DashboardAdmin/User/userliste/userliste/userliste.component';
import { ClientProfilComponent } from '../../client-profil/client-profil.component';
import { HistoriqueComponent } from '../../DashboardClient/historique/historique.component';
import { ReservationEnCourComponent } from '../../DashboardClient/reservation-en-cour/reservation-en-cour.component';
import { ReservationAccepterComponent } from '../../DashboardClient/reservation-accepter/reservation-accepter.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { PubliciteComponent } from '../../DashboardAdmin/publicite/publicite.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ChartsModule,
    NgbModule,
    MDBBootstrapModule.forRoot(),
    ToastrModule.forRoot(),
    MatTableModule,
    MatIconModule,
    MatFormFieldModule,
    FormsModule,
    MatPaginatorModule,
    LayoutModule,
    MatCardModule,MatSidenavModule,MatInputModule,MatDividerModule,MatListModule,
    MatTableModule,MatToolbarModule, MatMenuModule,MatIconModule,MatCheckboxModule,MatTabsModule, MatProgressSpinnerModule, MatGridListModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    HotellistComponent,
    VoyagelistComponent,
    SoireelistComponent,
    CircuitlistComponent,
    HomecontrolComponent,
    UserlisteComponent,
    ClientProfilComponent,
    HistoriqueComponent,
    ReservationEnCourComponent,
    ReservationAccepterComponent,
    PubliciteComponent,
  ]
})

export class AdminLayoutModule {}
