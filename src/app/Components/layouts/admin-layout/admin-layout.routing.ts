import { Routes } from '@angular/router';

import { DashboardComponent } from '../../DashboardAdmin/dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { CircuitlistComponent } from '../../DashboardAdmin/Circuit/circuitlist/circuitlist.component';
import { VoyagelistComponent } from '../../DashboardAdmin/Voyage/voyagelist/voyagelist.component';
import { HotellistComponent } from '../../DashboardAdmin/Hotel/hotellist/hotellist.component';
import { SoireelistComponent } from '../../DashboardAdmin/Soiree/soireelist/soireelist.component';
import { HomecontrolComponent } from '../../DashboardAdmin/homecontrol/homecontrol.component';
import { UserlisteComponent } from '../../DashboardAdmin/User/userliste/userliste/userliste.component';
import { ClientProfilComponent } from '../../client-profil/client-profil.component';
import { HistoriqueComponent } from '../../DashboardClient/historique/historique.component';
import { ReservationEnCourComponent } from '../../DashboardClient/reservation-en-cour/reservation-en-cour.component';
import { ReservationAccepterComponent } from '../../DashboardClient/reservation-accepter/reservation-accepter.component';
import { PubliciteComponent } from '../../DashboardAdmin/publicite/publicite.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'hotel',        component: HotellistComponent },
    { path: 'client',        component: ClientProfilComponent },
    { path: 'user',        component: UserlisteComponent },
    { path: 'voyage',        component: VoyagelistComponent },
    { path: 'soiree',        component: SoireelistComponent },
    { path: 'circuit'            ,        component: CircuitlistComponent },
    { path: 'home'               ,        component: HomecontrolComponent },
    { path :'Historique'         ,component:HistoriqueComponent},
    { path :'ReservationEnCour'  ,component:ReservationEnCourComponent},
    { path :'ReservationAccepter', component:ReservationAccepterComponent},
    { path :'Publiciter', component:PubliciteComponent},
];
