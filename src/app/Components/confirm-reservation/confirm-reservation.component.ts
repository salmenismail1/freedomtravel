import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as jsPDF from 'jspdf'
import { ActivatedRoute } from '@angular/router';
import { VoyagesService } from '../../Services/voyages.service';

@Component({
  selector: 'app-confirm-reservation',
  templateUrl: './confirm-reservation.component.html',
  styleUrls: ['./confirm-reservation.component.scss']
})
export class ConfirmReservationComponent implements OnInit {

  //@ViewChild('content', { static: true }) content:ElementRef;

  id;
  uneReservation;
  
  constructor(private Activate:ActivatedRoute,private voyageService:VoyagesService) { }

  ngOnInit() {
    this.id =this.Activate.snapshot.paramMap.get("id");

    this.voyageService.getUneReservation(this.id).subscribe(        
      response =>{
        this.uneReservation=response ;
        console.log(this.uneReservation)
      }
    )

  }

  pdf(){
    let doc = new jsPDF();
    
    let s = {

      "#editor":function(element,renderer){
        return true ;
      }

    };

    /*let content = this.content.nativeElement;

    doc.fromHTML(content.innerHTML,15,15 ,{

      'width' : 190,
      'elementHandlers' : s ,


    })
doc.save('test.pdf');*/
  }
}
