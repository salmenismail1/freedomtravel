import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
import { User } from '../../Models/user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  
  user;
  loading=true;
  constructor(private userServices: LoginService) { }

  ngOnInit() {

    var decoded = jwt_decode(this.userServices.getToken());
    this.userServices.getOnUser(decoded.id).subscribe(
      response => {
        
        this.user= response ; 
    })
    
    this.loading=false ;
  }

  ModifierUser(id:string){
    this.userServices.ModifUser(id,this.user).subscribe();
    console.log(this.user)
  }

}
