import { Component, OnInit, Input, Sanitizer, ViewChild } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { Caroussel } from '../../Models/caroussel';
import { Observable, Subject, Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HotelsService } from '../../Services/hotels.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { VoyagesService } from '../../Services/voyages.service';
import { Voyages } from '../../Models/voyages';
import { User } from '../../Models/user';
import { Hotels } from '../../Models/hotels';
import { Circuit } from '../../Models/circuit';
import { CircuitService } from '../../Services/circuit.service';
import { SoireesService } from '../../Services/soirees.service';
import { Soiree } from '../../Models/soiree';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('ngcarousel', { static: true }) ngCarousel: NgbCarousel;



  constructor(private circuitServices : CircuitService,private soireeServices : SoireesService,private sanitization:DomSanitizer,private httpClient:HttpClient,private hotelServices:HotelsService,private voyageService:VoyagesService) {}
  ngOnInit() {
 
  }

}