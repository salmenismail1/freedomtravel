import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent as ModalComponent, LoginComponent } from './Components/login/login.component';
import { AdminLayoutComponent } from './Components/layouts/admin-layout/admin-layout.component';
import { OmraComponent } from './Components/omra/omra.component';
import { HreservationComponent } from './Components/hreservation/hreservation.component';
import { VreservationComponent } from './Components/vreservation/vreservation.component';
import { SoireeComponent } from './Components/soiree/soiree.component';
import { CircuitComponent } from './Components/circuit/circuit.component';
import { HotelsComponent } from './Components/hotels/hotels.component';
import { ContactComponent } from './Components/contact/contact.component';
import { VoyageComponent } from './Components/voyage/voyage.component';
import { HomeComponent } from './Components/home/home.component';
import { SoireeReservationComponent } from './Components/soiree-reservation/soiree-reservation.component';
import { CircuitReservationComponent } from './Components/circuit-reservation/circuit-reservation.component';
import { AuthGuard } from './Guards/auth.guard';
import { VoyagemodifComponent } from './Components/DashboardAdmin/Voyage/voyagemodif/voyagemodif.component';
import { AddSoireeComponent } from './Components/DashboardAdmin/Soiree/add-soiree/add-soiree.component';
import { SoireemodifComponent } from './Components/DashboardAdmin/Soiree/soireemodif/soireemodif.component';
import { CircuitmodifComponent } from './Components/DashboardAdmin/Circuit/circuitmodif/circuitmodif.component';
import { AddCircuitComponent } from './Components/DashboardAdmin/Circuit/add-circuit/add-circuit.component';
import { AddVoyageComponent } from './Components/DashboardAdmin/Voyage/add-voyage/add-voyage.component';
import { AddHotelComponent } from './Components/DashboardAdmin/Hotel/add-hotel/add-hotel.component';
import { HotelmodifComponent } from './Components/DashboardAdmin/Hotel/hotelmodif/hotelmodif.component';
import { ConfirmReservationComponent } from './Components/confirm-reservation/confirm-reservation.component';
import { HistoriqueComponent } from './Components/DashboardClient/historique/historique.component';
import { ReservationEnCourComponent } from './Components/DashboardClient/reservation-en-cour/reservation-en-cour.component';
import { ReservationAccepterComponent } from './Components/DashboardClient/reservation-accepter/reservation-accepter.component';

const routes: Routes =[
  {
    path: 'admin',
    component: AdminLayoutComponent,canActivate: [AuthGuard],
    children: [
      {
      path: '',
      loadChildren: './Components/layouts/admin-layout/admin-layout.module#AdminLayoutModule'
      }
    ]
  },
  




  {path :'hotels',component: HotelsComponent},
  {path :'contact',component: ContactComponent},
  {path :'home',component: HomeComponent},
  {path :'',component: HomeComponent},
  {path :'circuit',component: CircuitComponent},
  {path :'login',component:LoginComponent},
  {path :'voyage',component:VoyageComponent},
  {path :'omra',component:OmraComponent},
  {path :'soiree',component:SoireeComponent},
  {path :'voyage',component:VoyageComponent},
  {path :'ReservationHotel/:id',component:HreservationComponent,canActivate: [AuthGuard]},
  {path :'ReservationVoyage/:id',component:VreservationComponent,canActivate: [AuthGuard]},
  {path :'ReservationSoiree/:id',component:SoireeReservationComponent,canActivate: [AuthGuard]},
  {path :'ReservationCircuit/:id',component:CircuitReservationComponent,canActivate: [AuthGuard]},

  {path :'addv',component:AddVoyageComponent},
  {path :'vmod',component:VoyagemodifComponent},
  {path :'adds',component:AddSoireeComponent},
  {path :'smod',component:SoireemodifComponent},
  {path :'addc',component:AddCircuitComponent},
  {path :'cmod',component:CircuitmodifComponent},
  {path :'addh',component:AddHotelComponent},
  {path :'hmod',component:HotelmodifComponent},
  {path :'confirm/:id',component:ConfirmReservationComponent},

];

@NgModule({
  
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
