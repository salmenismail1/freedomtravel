import { TestBed } from '@angular/core/testing';

import { OmraService } from './omra.service';

describe('OmraService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OmraService = TestBed.get(OmraService);
    expect(service).toBeTruthy();
  });
});
