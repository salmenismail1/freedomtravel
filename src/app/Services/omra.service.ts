import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Omra } from '../Models/omra';
import { formatDate } from '@angular/common';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
//const url = "https://topresa.ovh/api/omras.json?date[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en');
const url = "https://topresa.ovh/api/omras.json?date[after]=11/11/1000";
const sendUrl ="http://localhost:3000/omra/send/"


@Injectable({
  providedIn: 'root'
})
export class OmraService {
  
  constructor(private httpClient:HttpClient) {}


  getOmra():Observable<Omra>{
    
    return this.httpClient.get<Omra>(url,httpOptions);
  }
 
  send(email:string,omra:any){
    return this.httpClient.post<any>(sendUrl+email,omra)
  }
}
