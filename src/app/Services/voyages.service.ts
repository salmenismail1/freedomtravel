import { Injectable } from '@angular/core';
import { HttpHeaders ,HttpClient} from '@angular/common/http';
import { Voyages } from '../Models/voyages';
import { Observable, Subject } from 'rxjs';
import { formatDate } from '@angular/common';
import { map } from 'rxjs/operators';
import { Reservation } from '../Models/reservation';
import { User } from '../Models/user';

const url = "https://topresa.ovh/api/voyages.json?departvoyages.datedepart[after]=2020-02-14 ";

const deleteReservationVoyageUrl ="http://localhost:3000/voyage/api/DeleteReservation/" ;
const ReservationAccepter ="http://localhost:3000/voyage/api/ReservationAccepter/" ;
const ReservationAccepterUrl = "http://localhost:3000/voyage/api/ReservationVoyageAccepter"
const ReservationEnAttenteUrl = "http://localhost:3000/voyage/api/ReservationVoyageEnAttente";
const UneReservationUrl ="http://localhost:3000/voyage/api/UneReservation/";
const UpgradePayementUrl = "http://localhost:3000/user/api/UpgradePayement/";
const sendUrl ="http://localhost:3000/soiree/send/"



const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};



@Injectable({
  providedIn: 'root'
})
export class VoyagesService {

//*******njeimi */

  private posts: Voyages[] = [];
  private postsUpdated = new Subject<Voyages[]>();
  private voyagehomelist: Voyages[] = [];
  private voyagehomelistUpdated = new Subject<Voyages[]>();

  //*******njeimi */


  constructor(private httpClient:HttpClient) {}

  getVoyage(voyage:Voyages):Observable<Voyages>{
    
    return this.httpClient.get<Voyages>(url,httpOptions);
  }




  DeleteReservationHotel(Titre : string){
    return this.httpClient.delete<Reservation>(deleteReservationVoyageUrl+Titre,httpOptions);
  }

  ReservationAccepter(Titre : string){
    return this.httpClient.put<Reservation>( ReservationAccepter+Titre,httpOptions);
  }

  getReservationAccepter(){
    return this.httpClient.get<Reservation>( ReservationAccepterUrl,httpOptions);
  }

  getReservationEnAttent(){
    return this.httpClient.get<Reservation>(ReservationEnAttenteUrl,httpOptions);
    
  }
  getUneReservation(id : string){
    
    return this.httpClient.get<Reservation>(UneReservationUrl+id,httpOptions);
  }

  UpgradePayement(user:User){
    return this.httpClient.put<User>(UpgradePayementUrl+user,user);

  }
  
  send(email:string,voyage:any){
    return this.httpClient.post<any>(sendUrl+email,voyage)
  }

  //*******njeimi */

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }


  gethomevoyageUpdateListener() {
    return this.voyagehomelistUpdated.asObservable();
  }

  getVoyageBase() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/voyage/api/voyage"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,
            id :  post.id,nom : post.nom,pays : post.pays,titre : post.titre,prix : post.prix,departvoyages : post.departvoyages,
            imageVoyages : post.imageVoyages,image:post.image
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  addPost(    id : string,nom : string,pays : string,titre : string,prix : number,datedepart:string,dateretour:string,images: String ) {
    const post: Voyages = {  id :  id,nom : nom,pays:pays,titre:titre,prix:prix,departvoyages:{datedepart,dateretour},imageVoyages:[{images}]
  };
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/voyage/api/voyage", post)
      .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
      });
  }

  update(  id : string,nom : string,pays : string,titre : string,prix : number,datedepart:string,dateretour:string,images: String,idg:any) {
    const post: Voyages = {  id :  id,nom : nom,pays:pays,titre:titre,prix:prix,departvoyages:{datedepart,dateretour},imageVoyages:[{images}]
    };
    this.httpClient
      .put("http://localhost:3000/voyage/api/voyageModif/"+idg, post)
      .subscribe(responseData => {
        
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
      });
  }  

  deletePost(postId: string) {
    this.httpClient.delete("http://localhost:3000/voyage/api/posts/" + postId)
      .subscribe(() => {
        const updatedPosts = this.posts.filter(post => post.id !== postId);
        this.posts = updatedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  deleteall(){
  
    this.httpClient.delete("http://localhost:3000/voyage/api/deleteall/" + null)
    .subscribe(() => {
    });
  
  }
  getVoyagehomelist() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/voyage/api/voyagehomelist"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,
                      id :  post.id,nom : post.nom,pays : post.pays,titre : post.titre,prix : post.prix,
                      departvoyages: {datedepart : post.datedepart,dateretour : post.dateretour},imageVoyages:post.imageVoyages
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
        this.voyagehomelist = transformedPosts;
        this.voyagehomelistUpdated.next([...this.posts]);
      });
  }
  voyagehomelistpost(item:any) {

let datedepart=item.datedepart;  
let dateretour=item.dateretour;    

    const post: Voyages = {  id :  item.id,nom : item.nom,pays : item.pays,titre : item.titre,prix : item.prix,
      departvoyages: {datedepart,dateretour},imageVoyages:item.imageVoyages
};
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/voyage/api/voyagehomelist",post) .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.voyagehomelist.push(post);
        this.voyagehomelistUpdated.next([...this.posts]);
      });
  }

  //*******njeimi */
}
