import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Reservation } from '../Models/reservation';


const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private httpClient:HttpClient) { }

  AjoutReservation(url:string,reservation:Reservation){
    return this.httpClient.post<Reservation>(url,reservation);
  }

}


