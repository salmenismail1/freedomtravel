import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Hotels } from '../Models/hotels';
import { Observable, Subject } from 'rxjs';
import { User } from '../Models/user';
import { ReservationHotel } from '../Models/reservation-hotel';
import { map } from 'rxjs/operators';
import { Reservation } from '../Models/reservation';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
const HotelUrl = "https://freedomtravel.tn/json/hotels2.php";
const AjoutReservationUrl ="http://localhost:3000/hotel/api/reservation"
const  villeUrl = "https://www.freedomtravel.tn/ng/villes.php";

const Hotel2Url = "http://localhost:3000/hotel/api/ListerHotel" ;

const ReservationHotelUrl ="http://localhost:3000/hotel/api/ReservationHotel";
const deleteReservationHotelUrl ="http://localhost:3000/hotel/api/DeleteReservation/" ;

const ReservationAccepter ="http://localhost:3000/hotel/api/ReservationAccepter/" ;
const ReservationAccepterUrl = "http://localhost:3000/hotel/api/ReservationHotelAccepter"
const ReservationEnAttenteUrl = "http://localhost:3000/hotel/api/ReservationHotelEnAttente"

const sendUrl ="http://localhost:3000/hotel/send/"

const DeleteHotelUrl = "http://localhost:3000/hotel/api/Hotel/";



@Injectable({
  providedIn: 'root'
})
export class HotelsService {

  //*****njeimi***********

  private posts: Hotels[] = [];
  private postsUpdated = new Subject<Hotels[]>();
  private hotelhomelist: Hotels[] = [];
  private hotelhomelistUpdated = new Subject<Hotels[]>();
  

  //********njeimi */

  constructor(private httpClient:HttpClient) {}



  getHotel():Observable<any>{
    return this.httpClient.get<any>(HotelUrl,httpOptions);
  }

  getHotel2():Observable<Hotels>{
    return this.httpClient.get<Hotels>(Hotel2Url,httpOptions);
  }


  getVille(){
    return this.httpClient.get(villeUrl);
  }

  getPrix(hotel:Hotels){
    if (hotel.lpdvente !=0)
      return hotel.lpdvente;
    else if(hotel.dpvente)
      return hotel.dpvente;
    else if (hotel.allinvente)
      return hotel.allinvente;
    else (hotel.allinsoftvente)
      return hotel.allinsoftvente ;

  }

  getReservationHotel(){
    return this.httpClient.get<Reservation>(ReservationHotelUrl,httpOptions);
  }

  DeleteReservationHotel(Titre : string){
    return this.httpClient.delete<Reservation>(deleteReservationHotelUrl+Titre,httpOptions);
  }

  ReservationAccepter(Titre : string){
    return this.httpClient.put<Reservation>( ReservationAccepter+Titre,httpOptions);
  }

  getReservationAccepter(){
    return this.httpClient.get<Reservation>( ReservationAccepterUrl,httpOptions);
  }

  getReservationEnAttent(){
    return this.httpClient.get<Reservation>(ReservationEnAttenteUrl,httpOptions);
    
  }
  
  send(email:string,hotel:any){
    return this.httpClient.post<any>(sendUrl+email,hotel)
  }
  
//*******************Njeimi**************************
  
getPostUpdateListener() {
  return this.postsUpdated.asObservable();
}
gethomehotelUpdateListener() {
  return this.hotelhomelistUpdated.asObservable();
}

postContact(ReservationHotel:ReservationHotel){
  return this.httpClient.post<ReservationHotel>(AjoutReservationUrl,ReservationHotel);

}

getHotelBase() {
  this.httpClient
    .get<{ message: string; posts: any }>(
      "http://localhost:3000/hotel/api/hotel"
      
    )
    .pipe(map((postData) => {
      return postData.posts.map(post => {
                  return {
                    _id : post._id,
          id :  post.id,nom : post.nom,adultOnly : post.adultOnly,ville : post.ville,categorie : post.categorie,type : post.type,
    lpdvente : post.lpdvente,dpvente : post.dpvente,pcvente : post.pcvente,allinsoftvente : post.allinsoftvente,allinvente : post.allinvente,
    ultraallinvente : post.ultraallinvente,age_enf_gratuit : post.age_enf_gratuit,image:post.image
          
        };
      });
    }))
    .subscribe(transformedPosts => {
      this.posts = transformedPosts;
      this.postsUpdated.next([...this.posts]);
    });
}
addPost( id :  string,nom : string,adultOnly : string,ville : string,categorie : string,type : string,lpdvente : number,dpvente : number,pcvente : number,allinsoftvente : number,allinvente : number,ultraallinvente : number,age_enf_gratuit : number,image:string) {
  const post: Hotels = {  id :  id,nom : nom,adultOnly : adultOnly,ville : ville,categorie : categorie,type : type,
    lpdvente : lpdvente,dpvente : dpvente,pcvente : pcvente,allinsoftvente : allinsoftvente,allinvente : allinvente,
    ultraallinvente : ultraallinvente,age_enf_gratuit : age_enf_gratuit,image:image
};
  this.httpClient
    .post<{ message: string, postId: string }>("http://localhost:3000/hotel/api/hotel", post)
    .subscribe(responseData => {
      const id = responseData.postId;
      post.id = id;
      this.posts.push(post);
      this.postsUpdated.next([...this.posts]);
    });
}

update( id :  string,nom : string,adultOnly : string,ville : string,categorie : string,type : string,lpdvente : number,dpvente : number,pcvente : number,allinsoftvente : number,allinvente : number,ultraallinvente : number,age_enf_gratuit : number,image:string,idg:any) {
  const post: Hotels = {  id :  id,nom : nom,adultOnly : adultOnly,ville : ville,categorie : categorie,type : type,
    lpdvente : lpdvente,dpvente : dpvente,pcvente : pcvente,allinsoftvente : allinsoftvente,allinvente : allinvente,
    ultraallinvente : ultraallinvente,age_enf_gratuit : age_enf_gratuit,image:image
};
  this.httpClient
    .put("http://localhost:3000/hotel/api/hotelModif/"+idg, post)
    .subscribe(responseData => {
      
      this.posts.push(post);
      this.postsUpdated.next([...this.posts]);
    });
}  

deletePost(postId: string) {
  this.httpClient.delete("http://localhost:3000/hotel/api/posts/" + postId)
    .subscribe(() => {
      const updatedPosts = this.posts.filter(post => post.id !== postId);
      this.posts = updatedPosts;
      this.postsUpdated.next([...this.posts]);
    });
}

pubpost(item:any) {
 
  this.httpClient
    .post<{ message: string, postId: string }>("http://localhost:3000/hotel/api/pub",item)
    .subscribe(responseData => {
      console.log(responseData.message);
    });
}



deleteall(){

this.httpClient.delete("http://localhost:3000/hotel/api/deleteall/" + null)
.subscribe(() => {
console.log("dd");
});

}

getHotelhomelist() {
this.httpClient
  .get<{ message: string; posts: any }>(
    "http://localhost:3000/hotel/api/hotelhomelist"
    
  )
  .pipe(map((postData) => {
    return postData.posts.map(post => {
                return {
                  _id : post._id,
        id :  post.id,nom : post.nom,adultOnly : post.adultOnly,ville : post.ville,categorie : post.categorie,type : post.type,
  lpdvente : post.lpdvente,dpvente : post.dpvente,pcvente : post.pcvente,allinsoftvente : post.allinsoftvente,allinvente : post.allinvente,
  ultraallinvente : post.ultraallinvente,age_enf_gratuit : post.age_enf_gratuit,image:post.image
        
      };
    });
  }))
  .subscribe(transformedPosts => {
    this.posts = transformedPosts;
    this.postsUpdated.next([...this.posts]);
    this.hotelhomelist = transformedPosts;
    this.hotelhomelistUpdated.next([...this.posts]);
  });
}

hotelhomelistpost(item:any) {


  
  const post: Hotels = {  id :  item.id,nom : item.nom,adultOnly : item.adultOnly,ville : item.ville,categorie : item.categorie,type : item.type,
    lpdvente : item.lpdvente,dpvente : item.dpvente,pcvente : item.pcvente,allinsoftvente : item.allinsoftvente,allinvente : item.allinvente,
    ultraallinvente : item.ultraallinvente,age_enf_gratuit : item.age_enf_gratuit,image:item.image
};
  this.httpClient
    .post<{ message: string, postId: string }>("http://localhost:3000/hotel/api/hotelhomelist",post) .subscribe(responseData => {
      const id = responseData.postId;
      post.id = id;
      this.hotelhomelist.push(post);
      this.hotelhomelistUpdated.next([...this.posts]);
    });
}

}


