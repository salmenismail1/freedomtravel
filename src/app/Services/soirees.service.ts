import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Soiree } from '../Models/soiree';
import { formatDate } from '@angular/common';
import { map } from 'rxjs/operators';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
//const url = "https://topresa.ovh/api/soirees.json?departsoirees.date[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en');
const url = "https://topresa.ovh/api/soirees.json?departsoirees.date[after]=17/06/2020";
const sendUrl ="http://localhost:3000/soiree/send/"

@Injectable({
  providedIn: 'root'
})
export class SoireesService {

   //*********Njeimi */

  private posts: Soiree[] = [];
  private postsUpdated = new Subject<Soiree[]>();
  private soireehomelist: Soiree[] = [];
  private soireehomelistUpdated = new Subject<Soiree[]>();

  //*********Njeimi */

  
  constructor(private httpClient:HttpClient) {}

  getSoiree():Observable<Soiree>{
    
    return this.httpClient.get<Soiree>(url,httpOptions);
  }

  send(email:string,soiree:any){
    return this.httpClient.post<any>(sendUrl+email,soiree)
  }

  //*********Njeimi */

  getSoireesBase() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/soiree/api/soiree"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,id :  post.id,titre : post.titre,image : post.image, date : post.date,
                      venteadultesingle : post.venteadultesingle,venteadultedble : post.venteadultedble, 
                      vente3rdad : post.vente3rdad,venteenfant2ad : post.venteenfant2ad,tarifbebe : post.tarifbebe,
                      venteenfant1ad : post.venteenfant1ad
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  addPost(    id : string,titre : string,venteadultesingle : number,venteadultedble:number,date:string,images: string,vente3rdad:number,venteenfant2ad:number,tarifbebe:number,venteenfant1ad:number ) {
    const post: Soiree = {  id :  id,titre:titre,image : images, date : date,venteadultesingle : venteadultesingle,
      venteadultedble :venteadultedble, vente3rdad : vente3rdad,venteenfant2ad : venteenfant2ad,tarifbebe : tarifbebe,
      venteenfant1ad : venteenfant1ad
  };
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/soiree/api/soiree", post)
      .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
      });
  }
  id:String;
  titre:string;
  image :string;
  date:string;
  venteadultesingle:number;
  venteadultedble:number;
  vente3rdad:number;
  venteenfant2ad:number;
  tarifbebe:number;
  venteenfant1ad:number    
  update(  id : string,titre : string,venteadultesingle : number,venteadultedble:number,date:string,images: string,vente3rdad:number,venteenfant2ad:number,tarifbebe:number,venteenfant1ad:number,idg:any) {
    const post: Soiree = {  id :  id,titre:titre,image : images, date : date,venteadultesingle : venteadultesingle,
      venteadultedble :venteadultedble, vente3rdad : vente3rdad,venteenfant2ad : venteenfant2ad,tarifbebe : tarifbebe,
      venteenfant1ad : venteenfant1ad
  };
    this.httpClient
      .put("http://localhost:3000/soiree/api/soireeModif/"+idg, post)
      .subscribe(responseData => {
        
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
      });
  }  

  deletePost(postId: string) {
    this.httpClient.delete("http://localhost:3000/soiree/api/posts/" + postId)
      .subscribe(() => {
        const updatedPosts = this.posts.filter(post => post.id !== postId);
        this.posts = updatedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }

  deleteall(){
  
    this.httpClient.delete("http://localhost:3000/soiree/api/deleteall/" + null)
    .subscribe(() => {
  console.log("dd");
    });
  
  }
  getSoireehomelist() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/soiree/api/soireehomelist"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,
                      id :  post.id,titre:post.titre,image : post.image, date : post.date,venteadultesingle : post.venteadultesingle,
                      venteadultedble :post.venteadultedble, vente3rdad : post.vente3rdad,venteenfant2ad : post.venteenfant2ad,tarifbebe : post.tarifbebe,
                      venteenfant1ad : post.venteenfant1ad
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
        this.soireehomelist = transformedPosts;
        this.soireehomelistUpdated.next([...this.posts]);
      });
  }
  soireehomelistpost(item:any) {


    const post: Soiree = {   id :  item.id,titre:item.titre,image : item.image, date : item.date,venteadultesingle : item.venteadultesingle,
      venteadultedble :item.venteadultedble, vente3rdad : item.vente3rdad,venteenfant2ad : item.venteenfant2ad,tarifbebe : item.tarifbebe,
      venteenfant1ad : item.venteenfant1ad
};
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/soiree/api/soireehomelist",post) .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.soireehomelist.push(post);
        this.soireehomelistUpdated.next([...this.posts]);
      });
      console.log(post);
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  gethomesoireeUpdateListener() {
    return this.soireehomelistUpdated.asObservable();
  }
  //*********Njeimi */

}
