import { Injectable } from '@angular/core';
import { HttpHeaders ,HttpClient} from '@angular/common/http';
import { Voyages } from '../Models/voyages';
import { Observable } from 'rxjs';
import { formatDate } from '@angular/common';
import { Circuit } from '../Models/circuit';
import {  Subject } from 'rxjs';
import { User } from '../Models/user';
import { map } from 'rxjs/operators';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
const url = "https://topresa.ovh/api/departcircuits.json?date[before]=2020-06-18";
const sendUrl ="http://localhost:3000/circuit/send/"


@Injectable({
  providedIn: 'root'
})
export class CircuitService {
  
  private posts: Circuit[] = [];
  private postsUpdated = new Subject<Circuit[]>();
 
  constructor(private httpClient:HttpClient) {}


  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  getCircuit():Observable<Circuit>{
    
    return this.httpClient.get<Circuit>(url,httpOptions);
  }
  send(email:string,circuit:any){
    return this.httpClient.post<any>(sendUrl+email,circuit)
  }



 
  //**************njeimi */

  getCircuitBase() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/circuit/api/circuit"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,id :  post.id,nom : post.nom,image : post.image,datedepart:post.datedepart,
                      dateretour:post.dateretour,description:post.programmme.description,libelle:post.programmme.libelle,programme:post.programmme.programme,venteadultesingle:post.venteadultesingle,venteadultedble:post.venteadultedble
                      ,vente3rdad:post.vente3rdad,venteenfant2ad:post.venteenfant2ad,tarifbebe:post.tarifbebe,venteenfant1ad:post.venteenfant1ad
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  

  addPost(item:any,images:string) {
 

    const post: Circuit = {  id :  item.id,nom:item.nom,image : images,datedepart:item.datedepart,
      dateretour:item.dateretour,programmme:{description:item.description,libelle:item.libelle,programme:item.programme},venteadultesingle:item.venteadultesingle,venteadultedble:item.venteadultedble
      ,vente3rdad:item.vente3rdad,venteenfant2ad:item.venteenfant2ad,tarifbebe:item.tarifbebe,venteenfant1ad:item.venteenfant1ad
  };
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/circuit/api/circuit", post)
      .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
      });
  }
  
  update(  item:any,idg:any) {
    const post: Circuit = {  id :  item.id,nom:item.nom,image : item.images,datedepart:item.datedepart,
      dateretour:item.dateretour,programmme:item.programmme,venteadultesingle:item.venteadultesingle,venteadultedble:item.venteadultedble
      ,vente3rdad:item.vente3rdad,venteenfant2ad:item.venteenfant2ad,tarifbebe:item.tarifbebe,venteenfant1ad:item.venteenfant1ad
  };
    this.httpClient
      .put("http://localhost:3000/circuit/api/circuitModif/"+idg, post)
      .subscribe(responseData => {
        
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
      });
  }  

  deletePost(postId: string) {
    this.httpClient.delete("http://localhost:3000/circuit/api/posts/" + postId)
      .subscribe(() => {
        const updatedPosts = this.posts.filter(post => post.id !== postId);
        this.posts = updatedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }

  //**************njeimi */  
 
}
