import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Contact } from '../Models/contact';
import { Observable } from 'rxjs';

const contactUrl = "https://topresa.ovh/api/contactes";
const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};

@Injectable({
  providedIn: 'root'
})

  
export class ContactService {
  

  constructor(private httpClient:HttpClient) {}

  postContact(contact:Contact){
    return this.httpClient.post<Contact>(contactUrl,contact);
  
  }
}
