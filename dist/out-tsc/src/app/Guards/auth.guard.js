import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../Services/login.service';
import { LoginComponent as ModalComponent } from '../Components/login/login.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
var AuthGuard = /** @class */ (function () {
    function AuthGuard(matDialog, auth, router) {
        this.matDialog = matDialog;
        this.auth = auth;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (!this.auth.loggedIn()) {
            return true;
        }
        this.openModal();
        return false;
    };
    AuthGuard.prototype.openModal = function () {
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modal-component";
        this.matDialog.open(ModalComponent, {
            height: '600px',
            width: '500px',
        });
    };
    AuthGuard = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [MatDialog, LoginService,
            Router])
    ], AuthGuard);
    return AuthGuard;
}());
export { AuthGuard };
//# sourceMappingURL=auth.guard.js.map