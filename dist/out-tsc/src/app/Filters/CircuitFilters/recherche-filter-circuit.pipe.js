import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var RechercheFilterCircuitPipe = /** @class */ (function () {
    function RechercheFilterCircuitPipe() {
    }
    RechercheFilterCircuitPipe.prototype.transform = function (ListeCircuits, text) {
        if (text === undefined)
            return ListeCircuits;
        return ListeCircuits.filter(function (circuit) { return circuit.nom.toLowerCase().includes(text.toLocaleLowerCase()); });
    };
    RechercheFilterCircuitPipe = tslib_1.__decorate([
        Pipe({
            name: 'rechercheFilterCircuit'
        })
    ], RechercheFilterCircuitPipe);
    return RechercheFilterCircuitPipe;
}());
export { RechercheFilterCircuitPipe };
//# sourceMappingURL=recherche-filter-circuit.pipe.js.map