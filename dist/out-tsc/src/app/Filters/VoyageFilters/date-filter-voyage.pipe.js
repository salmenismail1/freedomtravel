import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { formatDate } from '@angular/common';
var DateFilterVoyagePipe = /** @class */ (function () {
    function DateFilterVoyagePipe() {
    }
    DateFilterVoyagePipe.prototype.transform = function (ListeVoyages, text) {
        if (text === undefined)
            return ListeVoyages;
        return ListeVoyages.filter(function (v) {
            console.log(v.departvoyages[0].datedepart);
            return (formatDate(new Date(v.departvoyages[0].datedepart), 'yyyy/MM/dd', 'en')) >= (formatDate(text, 'yyyy/MM/dd', 'en'));
        });
    };
    DateFilterVoyagePipe = tslib_1.__decorate([
        Pipe({
            name: 'dateFilterVoyage'
        })
    ], DateFilterVoyagePipe);
    return DateFilterVoyagePipe;
}());
export { DateFilterVoyagePipe };
//# sourceMappingURL=date-filter-voyage.pipe.js.map