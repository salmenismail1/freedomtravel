import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { Voyages } from '../../Models/voyages';
var PrixFilterVoyagePipe = /** @class */ (function () {
    function PrixFilterVoyagePipe() {
    }
    PrixFilterVoyagePipe.prototype.transform = function (ListeHotels, text1, text2) {
        if (text1 === undefined && text2 === undefined)
            return ListeHotels;
        return ListeHotels.filter(function (hotel) {
            var prix;
            prix = hotel.departvoyages[0].venteadultesingle;
            console.log(prix);
            if (text1 == undefined)
                return (text2 >= prix);
            if (text2 == undefined)
                return (text1 <= prix);
            return (prix >= text1 && prix <= text2);
        });
    };
    PrixFilterVoyagePipe.prototype.getPrix = function (Voyage) {
        for (var i = 0; i > Voyage.length; i++) {
            Voyages[i].departvoyages[0].venteadultesingle;
        }
    };
    PrixFilterVoyagePipe = tslib_1.__decorate([
        Pipe({
            name: 'prixFilterVoyage'
        })
    ], PrixFilterVoyagePipe);
    return PrixFilterVoyagePipe;
}());
export { PrixFilterVoyagePipe };
//# sourceMappingURL=prix-filter-voyage.pipe.js.map