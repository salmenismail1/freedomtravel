import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var RechercheFilterVoyagePipe = /** @class */ (function () {
    function RechercheFilterVoyagePipe() {
    }
    RechercheFilterVoyagePipe.prototype.transform = function (ListeVoyages, text) {
        console.log("frfrefrefrefre");
        if (text === undefined)
            return ListeVoyages;
        return ListeVoyages.filter(function (voyage) { return voyage.titre.toLowerCase().includes(text.toLocaleLowerCase()); });
    };
    RechercheFilterVoyagePipe = tslib_1.__decorate([
        Pipe({
            name: 'rechercheFilterVoyage'
        })
    ], RechercheFilterVoyagePipe);
    return RechercheFilterVoyagePipe;
}());
export { RechercheFilterVoyagePipe };
//# sourceMappingURL=recherche-filter-voyage.pipe.js.map