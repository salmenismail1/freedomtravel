import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var FilteretoilePipe = /** @class */ (function () {
    function FilteretoilePipe() {
    }
    FilteretoilePipe.prototype.transform = function (ListeHotels, text) {
        if (text === undefined)
            return ListeHotels;
        return ListeHotels.filter(function (hotel) { return hotel.categorie.toLowerCase().includes(text.toLocaleLowerCase()); });
    };
    FilteretoilePipe = tslib_1.__decorate([
        Pipe({
            name: 'filteretoile'
        })
    ], FilteretoilePipe);
    return FilteretoilePipe;
}());
export { FilteretoilePipe };
//# sourceMappingURL=filteretoile.pipe.js.map