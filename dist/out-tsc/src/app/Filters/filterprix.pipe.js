import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var FilterprixPipe = /** @class */ (function () {
    function FilterprixPipe() {
    }
    FilterprixPipe.prototype.transform = function (ListeHotels, text1, text2, h) {
        if (text1 === undefined && text2 === undefined)
            return ListeHotels;
        return ListeHotels.filter(function (hotel) {
            var prix;
            prix = h.getPrix(hotel);
            if (text1 == undefined)
                return (text2 >= prix);
            if (text2 == undefined)
                return (text1 <= prix);
            return (prix >= text1 && prix <= text2);
        });
    };
    FilterprixPipe = tslib_1.__decorate([
        Pipe({
            name: 'filterprix'
        })
    ], FilterprixPipe);
    return FilterprixPipe;
}());
export { FilterprixPipe };
//# sourceMappingURL=filterprix.pipe.js.map