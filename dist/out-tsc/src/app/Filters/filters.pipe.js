import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var FiltersPipe = /** @class */ (function () {
    function FiltersPipe() {
    }
    FiltersPipe.prototype.transform = function (ListeHotels, text) {
        if (text === undefined || text === "Liste des Villes")
            return ListeHotels;
        return ListeHotels.filter(function (hotel) { return hotel.ville.toLowerCase().includes(text.toLocaleLowerCase()); });
    };
    FiltersPipe = tslib_1.__decorate([
        Pipe({
            name: 'filters'
        })
    ], FiltersPipe);
    return FiltersPipe;
}());
export { FiltersPipe };
//# sourceMappingURL=filters.pipe.js.map