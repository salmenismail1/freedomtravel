import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { formatDate } from '@angular/common';
var SoireeDatePipePipe = /** @class */ (function () {
    function SoireeDatePipePipe() {
    }
    SoireeDatePipePipe.prototype.transform = function (soiree, text) {
        if (text === undefined)
            return null;
        return soiree.filter(function (s) { return (formatDate(s.date, 'yyyy/MM/dd', 'en')).toLowerCase().includes(text.toLocaleLowerCase()); });
    };
    SoireeDatePipePipe = tslib_1.__decorate([
        Pipe({
            name: 'soireeDatePipe'
        })
    ], SoireeDatePipePipe);
    return SoireeDatePipePipe;
}());
export { SoireeDatePipePipe };
//# sourceMappingURL=soiree-date-pipe.pipe.js.map