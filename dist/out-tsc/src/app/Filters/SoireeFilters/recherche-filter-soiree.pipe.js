import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var RechercheFilterSoireePipe = /** @class */ (function () {
    function RechercheFilterSoireePipe() {
    }
    RechercheFilterSoireePipe.prototype.transform = function (ListeSoirees, text) {
        if (text === undefined)
            return ListeSoirees;
        return ListeSoirees.filter(function (soiree) { return soiree.titre.toLowerCase().includes(text.toLocaleLowerCase()); });
    };
    RechercheFilterSoireePipe = tslib_1.__decorate([
        Pipe({
            name: 'rechercheFilterSoiree'
        })
    ], RechercheFilterSoireePipe);
    return RechercheFilterSoireePipe;
}());
export { RechercheFilterSoireePipe };
//# sourceMappingURL=recherche-filter-soiree.pipe.js.map