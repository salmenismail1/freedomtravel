import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var PrixFilterSoireePipe = /** @class */ (function () {
    function PrixFilterSoireePipe() {
    }
    PrixFilterSoireePipe.prototype.transform = function (ListeSoirees, text1, text2) {
        if (text1 === undefined && text2 === undefined)
            return ListeSoirees;
        return ListeSoirees.filter(function (Soiree) {
            var prix;
            prix = Soiree.departsoirees[0].venteadultesingle;
            console.log(prix);
            if (text1 == undefined)
                return (text2 >= prix);
            if (text2 == undefined)
                return (text1 <= prix);
            return (prix >= text1 && prix <= text2);
        });
    };
    PrixFilterSoireePipe = tslib_1.__decorate([
        Pipe({
            name: 'prixFilterSoiree'
        })
    ], PrixFilterSoireePipe);
    return PrixFilterSoireePipe;
}());
export { PrixFilterSoireePipe };
//# sourceMappingURL=prix-filter-soiree.pipe.js.map