import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { formatDate } from '@angular/common';
var DateFilterSoireePipe = /** @class */ (function () {
    function DateFilterSoireePipe() {
    }
    DateFilterSoireePipe.prototype.transform = function (ListeVoyages, text) {
        if (text === undefined)
            return ListeVoyages;
        return ListeVoyages.filter(function (v) {
            console.log(v.departsoirees[0].date);
            return (formatDate(new Date(v.departsoirees[0].date), 'yyyy/MM/dd', 'en')) >= (formatDate(text, 'yyyy/MM/dd', 'en'));
        });
    };
    DateFilterSoireePipe = tslib_1.__decorate([
        Pipe({
            name: 'dateFilterSoiree'
        })
    ], DateFilterSoireePipe);
    return DateFilterSoireePipe;
}());
export { DateFilterSoireePipe };
//# sourceMappingURL=date-filter-soiree.pipe.js.map