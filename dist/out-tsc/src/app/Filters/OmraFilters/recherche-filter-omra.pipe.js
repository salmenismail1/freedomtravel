import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var RechercheFilterOmraPipe = /** @class */ (function () {
    function RechercheFilterOmraPipe() {
    }
    RechercheFilterOmraPipe.prototype.transform = function (ListeOmras, text) {
        console.log("frfrefrefrefre");
        if (text === undefined)
            return ListeOmras;
        return ListeOmras.filter(function (omra) { return omra.titre.toLowerCase().includes(text.toLocaleLowerCase()); });
    };
    RechercheFilterOmraPipe = tslib_1.__decorate([
        Pipe({
            name: 'rechercheFilterOmra'
        })
    ], RechercheFilterOmraPipe);
    return RechercheFilterOmraPipe;
}());
export { RechercheFilterOmraPipe };
//# sourceMappingURL=recherche-filter-omra.pipe.js.map