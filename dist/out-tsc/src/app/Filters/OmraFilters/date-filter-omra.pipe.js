import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { formatDate } from '@angular/common';
var DateFilterOmraPipe = /** @class */ (function () {
    function DateFilterOmraPipe() {
    }
    DateFilterOmraPipe.prototype.transform = function (OmraVoyages, text) {
        console.log("frfrefrefrefre");
        if (text === undefined)
            return OmraVoyages;
        return OmraVoyages.filter(function (v) {
            console.log(v.date);
            return (formatDate(new Date(v.date), 'yyyy/MM/dd', 'en')) >= (formatDate(text, 'yyyy/MM/dd', 'en'));
        });
    };
    DateFilterOmraPipe = tslib_1.__decorate([
        Pipe({
            name: 'dateFilterOmra'
        })
    ], DateFilterOmraPipe);
    return DateFilterOmraPipe;
}());
export { DateFilterOmraPipe };
//# sourceMappingURL=date-filter-omra.pipe.js.map