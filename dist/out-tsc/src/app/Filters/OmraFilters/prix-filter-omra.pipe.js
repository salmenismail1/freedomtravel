import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var PrixFilterOmraPipe = /** @class */ (function () {
    function PrixFilterOmraPipe() {
    }
    PrixFilterOmraPipe.prototype.transform = function (ListeOmra, text1, text2) {
        console.log("frfrefrefrefre");
        if (text1 === undefined && text2 === undefined)
            return ListeOmra;
        return ListeOmra.filter(function (omra) {
            var prix;
            prix = omra.prix;
            console.log(prix);
            if (text1 == undefined)
                return (text2 >= prix);
            if (text2 == undefined)
                return (text1 <= prix);
            return (prix >= text1 && prix <= text2);
        });
    };
    PrixFilterOmraPipe = tslib_1.__decorate([
        Pipe({
            name: 'prixFilterOmra'
        })
    ], PrixFilterOmraPipe);
    return PrixFilterOmraPipe;
}());
export { PrixFilterOmraPipe };
//# sourceMappingURL=prix-filter-omra.pipe.js.map