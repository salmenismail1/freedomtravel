import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { formatDate } from '@angular/common';
var DureeVoyagePipe = /** @class */ (function () {
    function DureeVoyagePipe() {
    }
    DureeVoyagePipe.prototype.transform = function (voyage, text) {
        if (text === undefined)
            return null;
        return voyage.filter(function (v) { return (formatDate(v.datedepart, 'yyyy/MM/dd', 'en') + " vers " + formatDate(v.dateretour, 'yyyy/MM/dd', 'en')).toLowerCase().includes(text.toLocaleLowerCase()); });
    };
    DureeVoyagePipe = tslib_1.__decorate([
        Pipe({
            name: 'dureeVoyage'
        })
    ], DureeVoyagePipe);
    return DureeVoyagePipe;
}());
export { DureeVoyagePipe };
//# sourceMappingURL=duree-voyage.pipe.js.map