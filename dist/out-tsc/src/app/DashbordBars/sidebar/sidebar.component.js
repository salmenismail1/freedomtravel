import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AppVarsGlobal } from '../../app.vars.global';
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(vars) {
        this.vars = vars;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        this.vars.type = this.vars.login();
        var ROUTES = [
            { path: '/admin/dashboard', title: 'Dashboard', icon: 'design_app', class: '', visibility: (this.vars.type != "client") },
            { path: '/admin/user', title: 'user', icon: 'users_single-02', class: '', visibility: (this.vars.type != "client") },
            { path: '/admin/user-profile', title: 'User Profile', icon: 'users_single-02', class: '', visibility: true },
            { path: '/admin/hotel', title: 'hotel', icon: 'users_single-02', class: '', visibility: (this.vars.type != "client") },
            { path: '/admin/voyage', title: 'voyage', icon: 'users_single-02', class: '', visibility: (this.vars.type != "client") },
            { path: '/admin/soiree', title: 'soiree', icon: 'users_single-02', class: '', visibility: (this.vars.type != "client") },
            { path: '/admin/circuit', title: 'circuit', icon: 'users_single-02', class: '', visibility: (this.vars.type != "client") },
            { path: '/admin/home', title: 'home', icon: 'users_single-02', class: '', visibility: (this.vars.type != "client") },
            { path: '/admin/Historique', title: 'Historique', icon: 'users_single-02', class: '', visibility: (this.vars.type == "client") },
            { path: '/admin/ReservationEnCour', title: 'Reservation en cour', icon: 'users_single-02', class: '', visibility: (this.vars.type == "client") },
            { path: '/admin/ReservationAccepter', title: 'Reservation accepter ', icon: 'users_single-02', class: '', visibility: (this.vars.type == "client") },
        ];
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
    };
    SidebarComponent.prototype.isMobileMenu = function () {
        if (window.innerWidth > 991) {
            return false;
        }
        return true;
    };
    ;
    SidebarComponent = tslib_1.__decorate([
        Component({
            selector: 'app-sidebar',
            templateUrl: './sidebar.component.html',
            styleUrls: ['./sidebar.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [AppVarsGlobal])
    ], SidebarComponent);
    return SidebarComponent;
}());
export { SidebarComponent };
//# sourceMappingURL=sidebar.component.js.map