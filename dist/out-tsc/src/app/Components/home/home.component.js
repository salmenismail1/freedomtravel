import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { HotelsService } from '../../Services/hotels.service';
import { VoyagesService } from '../../Services/voyages.service';
import { CircuitService } from '../../Services/circuit.service';
import { SoireesService } from '../../Services/soirees.service';
var HomeComponent = /** @class */ (function () {
    function HomeComponent(circuitServices, soireeServices, sanitization, httpClient, hotelServices, voyageService) {
        this.circuitServices = circuitServices;
        this.soireeServices = soireeServices;
        this.sanitization = sanitization;
        this.httpClient = httpClient;
        this.hotelServices = hotelServices;
        this.voyageService = voyageService;
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    tslib_1.__decorate([
        ViewChild('ngcarousel', { static: true }),
        tslib_1.__metadata("design:type", NgbCarousel)
    ], HomeComponent.prototype, "ngCarousel", void 0);
    HomeComponent = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: './home.component.html',
            styleUrls: ['./home.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [CircuitService, SoireesService, DomSanitizer, HttpClient, HotelsService, VoyagesService])
    ], HomeComponent);
    return HomeComponent;
}());
export { HomeComponent };
//# sourceMappingURL=home.component.js.map