import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { SoireesService } from '../../Services/soirees.service';
var SoireeComponent = /** @class */ (function () {
    function SoireeComponent(soireeService) {
        this.soireeService = soireeService;
        this.test = "";
    }
    SoireeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.soireeService.getSoiree().subscribe(function (response) {
            _this.ListeSoirees = response;
        }, function (err) {
            "error";
        });
    };
    SoireeComponent.prototype.testing1 = function () {
        this.test = "";
        this.reset();
    };
    SoireeComponent.prototype.testing4 = function () {
        this.test = "prix";
        this.test = "f";
        this.text = "";
    };
    SoireeComponent.prototype.reset = function () {
        this.min = 0;
        this.max = 300;
    };
    SoireeComponent = tslib_1.__decorate([
        Component({
            selector: 'app-soiree',
            templateUrl: './soiree.component.html',
            styleUrls: ['./soiree.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [SoireesService])
    ], SoireeComponent);
    return SoireeComponent;
}());
export { SoireeComponent };
//# sourceMappingURL=soiree.component.js.map