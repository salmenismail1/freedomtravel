import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Chambre } from '../../Models/chambre';
import * as html2pdf from 'html2pdf.js';
import { ReservationService } from '../../Services/reservation.service';
import { Reservation } from '../../Models/reservation';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import * as jwt_decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';
var AjoutReservationUrl = "http://localhost:3000/voyage/api/reservation";
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
var VreservationComponent = /** @class */ (function () {
    function VreservationComponent(router, toastr, Activate, httpClient, serviceReservation) {
        this.router = router;
        this.toastr = toastr;
        this.Activate = Activate;
        this.httpClient = httpClient;
        this.serviceReservation = serviceReservation;
        this.reservation = new Reservation();
        this.nbChambre = new Array(5);
    }
    VreservationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.nb = 4;
        this.id = this.Activate.snapshot.paramMap.get("id");
        this.unvoyage = "https://topresa.ovh/api/departvoyages.json?departvoyages.datedepart[after]=" + formatDate(new Date(), 'yyyy-MM-dd', 'en') + "&voyage.id=" + this.id;
        console.log(this.unvoyage);
        this.getUnVoyage().subscribe(function (response) {
            _this.x = response[0];
            _this.voyage = response;
            console.log(_this.x);
            console.log(response);
        });
        this.reactiveform = new FormGroup({
            'Nom': new FormControl('', [Validators.required]),
            'Email': new FormControl('', [Validators.email, Validators.required]),
            'Tel': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('[0-9]*')]),
        });
    };
    Object.defineProperty(VreservationComponent.prototype, "Nom", {
        get: function () { return this.reactiveform.get('Nom'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VreservationComponent.prototype, "Email", {
        get: function () { return this.reactiveform.get('Email'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VreservationComponent.prototype, "Tel", {
        get: function () { return this.reactiveform.get('Tel'); },
        enumerable: true,
        configurable: true
    });
    VreservationComponent.prototype.getUnVoyage = function () {
        return this.httpClient.get(this.unvoyage, httpOptions);
    };
    VreservationComponent.prototype.chambre = function () {
        for (var i = 0; i < this.nb; i++) {
            var selectElement = document.getElementById('rowid' + i);
            if (selectElement.hidden == true) {
                selectElement.hidden = false;
                console.log(selectElement);
                return;
            }
        }
    };
    VreservationComponent.prototype.delchambre = function (i) {
        var selectElement = document.getElementById('rowid' + i);
        selectElement.hidden = true;
        var select1 = document.getElementById('homme' + i);
        select1.selectedIndex = 0;
        var select2 = document.getElementById('enfant' + i);
        console.log(select2.selectedIndex);
        select2.selectedIndex = 0;
        var select3 = document.getElementById('bebe' + i);
        select3.selectedIndex = 0;
        console.log(selectElement);
    };
    VreservationComponent.prototype.ngAfterViewInit = function () {
        var selectElement = document.getElementById('rowid' + 0);
        selectElement.hidden = false;
        console.log(selectElement);
    };
    VreservationComponent.prototype.Prix = function (venteadultesingle, venteadultedble, tarifbebe, datedepart, dateretour) {
        this.venteadultesingle = venteadultesingle;
        this.venteadultedble = venteadultedble;
        this.tarifbebe = tarifbebe;
        this.datedepart = datedepart;
        this.dateretour = dateretour;
    };
    VreservationComponent.prototype.calculPrix = function () {
        this.prix = 0;
        for (var j = 0; j < this.nb; j++) {
            var selectElement = document.getElementById('rowid' + j);
            if (selectElement.hidden == false) {
                var select1 = document.getElementById('homme' + j);
                var select2 = document.getElementById('enfant' + j);
                var select3 = document.getElementById('bebe' + j);
                this.prix += (select1.selectedIndex * this.venteadultesingle) + (select2.selectedIndex * this.venteadultedble) + (select3.selectedIndex * this.tarifbebe);
                console.log(this.prix);
            }
        }
        console.log(this.prix);
    };
    VreservationComponent.prototype.clear = function () {
        this.reactiveform.reset();
        for (var i = 0; i < this.nb; i++) {
            var selectElement = document.getElementById('rowid' + i);
            selectElement.hidden = true;
            var firstElement = document.getElementById('rowid' + 0);
            firstElement.hidden = false;
            var select1 = document.getElementById('homme' + i);
            select1.selectedIndex = 0;
            var select2 = document.getElementById('enfant' + i);
            select2.selectedIndex = 0;
            var select3 = document.getElementById('bebe' + i);
            select3.selectedIndex = 0;
        }
    };
    VreservationComponent.prototype.pdf = function () {
        var options = {
            margin: 15,
            filename: 'myfile.pdf',
            image: { type: 'jpeg' },
            jsPDF: { orientation: 'landscape' }
        };
        var content = document.getElementById('content');
        html2pdf()
            .from(content)
            .set(options)
            .save();
    };
    VreservationComponent.prototype.affiche = function () {
        this.TnbrAdulte = new Array();
        this.TnbrEnfant = new Array();
        this.TnbrBebe = new Array();
        this.nbrAdulte = 0;
        this.nbrEnfant = 0;
        this.nbrBebe = 0;
        this.TChambre = [];
        var cmp = 0;
        for (var j = 0; j < this.nb; j++) {
            var selectElement = document.getElementById('rowid' + j);
            if (selectElement.hidden == false) {
                this.Adulte = document.getElementById('homme' + j);
                this.Enfant = document.getElementById('enfant' + j);
                this.Bebe = document.getElementById('bebe' + j);
                this.nbrAdulte += this.Adulte.selectedIndex;
                this.nbrEnfant += this.Enfant.selectedIndex;
                this.nbrBebe += this.Bebe.selectedIndex;
                this.TnbrAdulte[cmp] = this.Adulte.selectedIndex;
                this.TnbrEnfant[cmp] = this.Enfant.selectedIndex;
                this.TnbrBebe[cmp] = this.Bebe.selectedIndex;
                cmp += 1;
                var chambre = new Chambre();
                chambre.num = j + 1 + "";
                console.log(chambre);
                this.TChambre.push(chambre);
            }
            this.TAdulte = new Array(this.nbrAdulte);
            this.TEnfant = new Array(this.nbrEnfant);
            this.TBebe = new Array(this.nbrBebe);
        }
        var decoded = jwt_decode(localStorage.getItem('token'));
        this.reservation.IdClient = decoded.id + "";
        console.log(this.reservation.IdClient);
        this.reservation.Prix = this.prix;
        this.reservation.Titre = this.x.titre;
        this.reservation.DateReservation = new Date().toString();
        this.reservation.DateDepart = formatDate(this.datedepart, 'yyyy-MM-dd', 'en');
        this.reservation.DatRetour = formatDate(this.dateretour, 'yyyy-MM-dd', 'en');
        this.reservation.Nom = this.reactiveform.value.Nom;
        this.reservation.Email = this.reactiveform.value.Email;
        this.reservation.Tel = this.reactiveform.value.Tel;
        console.log(this.reservation.Nom);
    };
    VreservationComponent.prototype.test = function () {
        this.TAdulte = new Array(this.nbrAdulte - 1);
        this.TEnfant = new Array(this.nbrEnfant - 1);
        this.TBebe = new Array(this.nbrBebe - 1);
        var j = 1;
        var k = 0;
        var t = [];
        for (var i = 0; i < this.nbrAdulte; i++) {
            var selectElement = document.getElementById('Adulte' + i);
            if (j < this.TnbrAdulte[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].adulte = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrEnfant; i++) {
            console.log(i);
            var selectElement2 = document.getElementById('Enfant' + i);
            if (j < this.TnbrEnfant[k]) {
                t.push(selectElement2.value + "");
                j += 1;
            }
            else {
                t.push(selectElement2.value + "");
                this.TChambre[k].enfant = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrBebe; i++) {
            var selectElement = document.getElementById('Bebe' + i);
            if (j < this.TnbrBebe[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].bb = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        this.reservation.Chambre = this.TChambre;
    };
    VreservationComponent.prototype.AjoutCambre = function () {
        var _this = this;
        this.serviceReservation.AjoutReservation(AjoutReservationUrl, this.reservation).subscribe(function (response) {
            console.log(response);
            _this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Voyage avec succée');
        }, function (err) { _this.toastr.error('', 'Verifiez vos Donnees'); });
    };
    tslib_1.__decorate([
        ViewChild('content', { static: true }),
        tslib_1.__metadata("design:type", ElementRef)
    ], VreservationComponent.prototype, "content", void 0);
    VreservationComponent = tslib_1.__decorate([
        Component({
            selector: 'app-vreservation',
            templateUrl: './vreservation.component.html',
            styleUrls: ['./vreservation.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [Router, ToastrService, ActivatedRoute, HttpClient, ReservationService])
    ], VreservationComponent);
    return VreservationComponent;
}());
export { VreservationComponent };
//# sourceMappingURL=vreservation.component.js.map