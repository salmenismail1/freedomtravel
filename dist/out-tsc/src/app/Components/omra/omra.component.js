import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { OmraService } from '../../Services/omra.service';
import { Reservation } from '../../Models/reservation';
import { ReservationService } from '../../Services/reservation.service';
import { Chambre } from '../../Models/chambre';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as html2pdf from 'html2pdf.js';
var AjoutReservationUrl = "http://localhost:3000/omra/api/reservationOmra";
var OmraComponent = /** @class */ (function () {
    function OmraComponent(router, toastr, omraService, serviceReservation) {
        this.router = router;
        this.toastr = toastr;
        this.omraService = omraService;
        this.serviceReservation = serviceReservation;
        this.reservation = new Reservation;
        this.nbChambre = new Array(5);
        this.test = "";
    }
    OmraComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.nb = 4;
        console.log(this.text);
        this.omraService.getOmra().subscribe(function (response) {
            _this.ListeOmra = response;
            console.log(_this.ListeOmra);
        }, function (err) {
            "error";
        });
        this.reactiveform = new FormGroup({
            'Nom': new FormControl('', [Validators.required]),
            'Email': new FormControl('', [Validators.email, Validators.required]),
            'Tel': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('[0-9]*')]),
        });
    };
    Object.defineProperty(OmraComponent.prototype, "Nom", {
        get: function () { return this.reactiveform.get('Nom'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OmraComponent.prototype, "Email", {
        get: function () { return this.reactiveform.get('Email'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OmraComponent.prototype, "Tel", {
        get: function () { return this.reactiveform.get('Tel'); },
        enumerable: true,
        configurable: true
    });
    OmraComponent.prototype.chambre = function () {
        for (var i = 0; i < this.nb; i++) {
            var selectElement = document.getElementById('rowid' + i);
            if (selectElement.hidden == true) {
                selectElement.hidden = false;
                console.log(selectElement);
                console.log(this.adult);
                return;
            }
        }
    };
    OmraComponent.prototype.delchambre = function (i) {
        var selectElement = document.getElementById('rowid' + i);
        selectElement.hidden = true;
        var select1 = document.getElementById('homme' + i);
        select1.selectedIndex = 0;
        var select2 = document.getElementById('enfant' + i);
        console.log(select2.selectedIndex);
        select2.selectedIndex = 0;
        var select3 = document.getElementById('bebe' + i);
        select3.selectedIndex = 0;
        console.log(selectElement);
        console.log(this.adult);
    };
    OmraComponent.prototype.ngAfterViewInit = function () {
    };
    OmraComponent.prototype.calculPrix = function () {
        this.Tprix = 0;
        for (var j = 0; j < this.nb; j++) {
            var selectElement = document.getElementById('rowid' + j);
            if (selectElement.hidden == false) {
                var select1 = document.getElementById('homme' + j);
                var select2 = document.getElementById('enfant' + j);
                this.Tprix += (this.prix * select1.selectedIndex) + (this.prix * select2.selectedIndex);
                console.log(this.prix);
            }
        }
        console.log(this.Tprix);
    };
    OmraComponent.prototype.clear = function () {
        this.reactiveform.reset();
        for (var i = 0; i < this.nb; i++) {
            var selectElement = document.getElementById('rowid' + i);
            selectElement.hidden = true;
            var firstElement = document.getElementById('rowid' + 0);
            firstElement.hidden = false;
            var select1 = document.getElementById('homme' + i);
            select1.selectedIndex = 0;
            var select2 = document.getElementById('enfant' + i);
            select2.selectedIndex = 0;
            var select3 = document.getElementById('bebe' + i);
            select3.selectedIndex = 0;
        }
    };
    OmraComponent.prototype.DateTitre = function (titre, date, prix, program) {
        this.titre = titre;
        this.date = date;
        this.prix = prix;
        this.program = program;
        console.log(this.prix);
    };
    OmraComponent.prototype.testing1 = function () {
        console.log(this.test);
        this.test = "";
        console.log(this.test);
        this.reset();
    };
    OmraComponent.prototype.testing4 = function () {
        //console.log("min : "+this.min+"max : "+this.max);
        this.test = "f";
        console.log(this.test);
    };
    OmraComponent.prototype.reset = function () {
        this.min = 0;
        this.max = 3000;
    };
    OmraComponent.prototype.pdf = function () {
        var options = {
            margin: 15,
            filename: 'myfile.pdf',
            image: { type: 'jpeg' },
            jsPDF: { orientation: 'landscape' }
        };
        var content = document.getElementById('content');
        html2pdf()
            .from(content)
            .set(options)
            .save();
    };
    OmraComponent.prototype.affiche = function () {
        this.TnbrAdulte = new Array();
        this.TnbrEnfant = new Array();
        this.TnbrBebe = new Array();
        this.nbrAdulte = 0;
        this.nbrEnfant = 0;
        this.nbrBebe = 0;
        this.TChambre = [];
        var cmp = 0;
        for (var j = 0; j < this.nb; j++) {
            var selectElement = document.getElementById('rowid' + j);
            if (selectElement.hidden == false) {
                this.Adulte = document.getElementById('homme' + j);
                this.Enfant = document.getElementById('enfant' + j);
                this.Bebe = document.getElementById('bebe' + j);
                this.nbrAdulte += this.Adulte.selectedIndex;
                this.nbrEnfant += this.Enfant.selectedIndex;
                this.nbrBebe += this.Bebe.selectedIndex;
                this.TnbrAdulte[cmp] = this.Adulte.selectedIndex;
                this.TnbrEnfant[cmp] = this.Enfant.selectedIndex;
                this.TnbrBebe[cmp] = this.Bebe.selectedIndex;
                cmp += 1;
                var chambre = new Chambre();
                chambre.num = j + 1 + "";
                console.log(chambre);
                this.TChambre.push(chambre);
            }
            this.TAdulte = new Array(this.nbrAdulte);
            this.TEnfant = new Array(this.nbrEnfant);
            this.TBebe = new Array(this.nbrBebe);
        }
        this.reservation.DateReservation = new Date().toString();
        this.reservation.Prix = this.Tprix;
        this.reservation.DateDepart = this.date;
        this.reservation.Titre = this.titre;
        this.reservation.Nom = this.reactiveform.value.Nom;
        this.reservation.Email = this.reactiveform.value.Email;
        this.reservation.Tel = this.reactiveform.value.Tel;
        console.log(this.reservation.Nom);
    };
    OmraComponent.prototype.teste = function () {
        this.TAdulte = new Array(this.nbrAdulte - 1);
        this.TEnfant = new Array(this.nbrEnfant - 1);
        this.TBebe = new Array(this.nbrBebe - 1);
        var j = 1;
        var k = 0;
        var t = [];
        for (var i = 0; i < this.nbrAdulte; i++) {
            var selectElement = document.getElementById('Adulte' + i);
            if (j < this.TnbrAdulte[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].adulte = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrEnfant; i++) {
            console.log(i);
            var selectElement2 = document.getElementById('Enfant' + i);
            if (j < this.TnbrEnfant[k]) {
                t.push(selectElement2.value + "");
                j += 1;
            }
            else {
                t.push(selectElement2.value + "");
                this.TChambre[k].enfant = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrBebe; i++) {
            var selectElement = document.getElementById('Bebe' + i);
            if (j < this.TnbrBebe[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].bb = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        this.reservation.Chambre = this.TChambre;
    };
    OmraComponent.prototype.AjoutCambre = function () {
        var _this = this;
        this.serviceReservation.AjoutReservation(AjoutReservationUrl, this.reservation).subscribe(function (response) {
            console.log(response);
            _this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Voyage avec succée');
        }, function (err) { _this.toastr.error('', 'Verifiez vos Donnees'); });
    };
    OmraComponent = tslib_1.__decorate([
        Component({
            selector: 'app-omra',
            templateUrl: './omra.component.html',
            styleUrls: ['./omra.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [Router, ToastrService, OmraService, ReservationService])
    ], OmraComponent);
    return OmraComponent;
}());
export { OmraComponent };
//# sourceMappingURL=omra.component.js.map