import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { VoyagesService } from '../../Services/voyages.service';
import { Voyages } from '../../Models/voyages';
var VoyageComponent = /** @class */ (function () {
    function VoyageComponent(voyageService) {
        this.voyageService = voyageService;
        this.voyage = new Voyages;
        this.test = "";
    }
    VoyageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.voyageService.getVoyage(this.voyage).subscribe(function (response) {
            _this.ListeVoyages = response;
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    VoyageComponent.prototype.testing1 = function () {
        console.log(this.test);
        this.test = "";
        this.reset();
    };
    VoyageComponent.prototype.testing4 = function () {
        this.test = "prix";
        console.log("min : " + this.min + "max : " + this.max);
        this.test = "f";
        this.text = "";
    };
    VoyageComponent.prototype.reset = function () {
        this.min = 0;
        this.max = 3000;
    };
    VoyageComponent = tslib_1.__decorate([
        Component({
            selector: 'app-voyage',
            templateUrl: './voyage.component.html',
            styleUrls: ['./voyage.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [VoyagesService])
    ], VoyageComponent);
    return VoyageComponent;
}());
export { VoyageComponent };
//# sourceMappingURL=voyage.component.js.map