import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelsService } from '../../Services/hotels.service';
import { formatDate } from '@angular/common';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Reservation } from '../../Models/reservation';
import { ReservationService } from '../../Services/reservation.service';
import { Chambre } from '../../Models/chambre';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import * as jwt_decode from 'jwt-decode';
import * as html2pdf from 'html2pdf.js';
var AjoutReservationUrl = "http://localhost:3000/hotel/api/reservation";
var url = "https://freedomtravel.tn/json/carouselHotel.php?id=";
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
var HreservationComponent = /** @class */ (function () {
    function HreservationComponent(router, toastr, Activate, service, httpClient, serviceReservation) {
        this.router = router;
        this.toastr = toastr;
        this.Activate = Activate;
        this.service = service;
        this.httpClient = httpClient;
        this.serviceReservation = serviceReservation;
        this.reservation = new Reservation;
        this.nbChambre = new Array(5);
    }
    HreservationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.nb = 4;
        this.id = this.Activate.snapshot.paramMap.get("id");
        this.obj = formatDate(new Date(), 'yyyy/MM/dd', 'en');
        this.reactiveform = new FormGroup({
            'Nom': new FormControl('', [Validators.required]),
            'Email': new FormControl('', [Validators.email, Validators.required]),
            'Tel': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('[0-9]*')]),
        });
        this.service.getHotel().subscribe(function (response) {
            _this.ListeHotels = response;
            console.log(response);
        }, function (err) {
            "error";
        });
        this.getImg().subscribe(function (response) {
            _this.Listimg = response;
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    Object.defineProperty(HreservationComponent.prototype, "Nom", {
        get: function () { return this.reactiveform.get('Nom'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HreservationComponent.prototype, "Email", {
        get: function () { return this.reactiveform.get('Email'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HreservationComponent.prototype, "Tel", {
        get: function () { return this.reactiveform.get('Tel'); },
        enumerable: true,
        configurable: true
    });
    HreservationComponent.prototype.getImg = function () {
        return this.httpClient.get(url + this.id, httpOptions);
    };
    HreservationComponent.prototype.chambre = function () {
        for (var i = 0; i < this.nb; i++) {
            var selectElement = document.getElementById('rowid' + i);
            if (selectElement.hidden == true) {
                selectElement.hidden = false;
                return;
            }
        }
    };
    HreservationComponent.prototype.delchambre = function (i) {
        var selectElement = document.getElementById('rowid' + i);
        selectElement.hidden = true;
        var select1 = document.getElementById('homme' + i);
        select1.selectedIndex = 0;
        var select2 = document.getElementById('enfant' + i);
        select2.selectedIndex = 0;
        var select3 = document.getElementById('bebe' + i);
        select3.selectedIndex = 0;
    };
    HreservationComponent.prototype.calculPrix = function () {
        this.prix = 0;
        var prix;
        var prixtElement = document.getElementById('prix');
        var typePrix = prixtElement.selectedIndex;
        for (var i = 0; i < this.ListeHotels.length; i++) {
            if (this.ListeHotels[i].id == this.id) {
                this.hotelreserver = this.ListeHotels[i];
                if (typePrix == 1) {
                    prix = this.hotelreserver.allinsoftvente;
                }
                if (typePrix == 2) {
                    prix = this.hotelreserver.allinvente;
                }
                if (typePrix == 3) {
                    prix = this.hotelreserver.dpvente;
                }
                if (typePrix == 4) {
                    prix = this.hotelreserver.lpdvente;
                }
                if (typePrix == 5) {
                    prix = this.hotelreserver.pcvente;
                }
                if (typePrix == 6) {
                    prix = this.hotelreserver.ultraallinvente;
                }
            }
            console.log(prix);
        }
        console.log(this.hotelreserver);
        for (var j = 0; j < this.nb; j++) {
            var selectElement = document.getElementById('rowid' + j);
            if (selectElement.hidden == false) {
                var select1 = document.getElementById('homme' + j);
                var select2 = document.getElementById('enfant' + j);
                var select3 = document.getElementById('bebe' + j);
                this.prix += (select1.selectedIndex * prix) + (select2.selectedIndex * prix) + (select3.selectedIndex * prix);
                console.log(this.prix);
            }
        }
        console.log(this.prix);
    };
    HreservationComponent.prototype.clear = function () {
        this.reactiveform.reset();
        for (var i = 0; i < this.nb; i++) {
            var selectElement = document.getElementById('rowid' + i);
            selectElement.hidden = true;
            var select1 = document.getElementById('homme' + i);
            select1.selectedIndex = 0;
            var select2 = document.getElementById('enfant' + i);
            select2.selectedIndex = 0;
            var select3 = document.getElementById('bebe' + i);
            select3.selectedIndex = 0;
        }
        var firstElement = document.getElementById('rowid' + 0);
        firstElement.hidden = false;
    };
    HreservationComponent.prototype.affiche = function () {
        this.TnbrAdulte = new Array();
        this.TnbrEnfant = new Array();
        this.TnbrBebe = new Array();
        this.nbrAdulte = 0;
        this.nbrEnfant = 0;
        this.nbrBebe = 0;
        this.TChambre = [];
        var cmp = 0;
        for (var j = 0; j < this.nb; j++) {
            var selectElement = document.getElementById('rowid' + j);
            if (selectElement.hidden == false) {
                this.Adulte = document.getElementById('homme' + j);
                this.Enfant = document.getElementById('enfant' + j);
                this.Bebe = document.getElementById('bebe' + j);
                this.nbrAdulte += this.Adulte.selectedIndex;
                this.nbrEnfant += this.Enfant.selectedIndex;
                this.nbrBebe += this.Bebe.selectedIndex;
                this.TnbrAdulte[cmp] = this.Adulte.selectedIndex;
                this.TnbrEnfant[cmp] = this.Enfant.selectedIndex;
                this.TnbrBebe[cmp] = this.Bebe.selectedIndex;
                cmp += 1;
                var chambre = new Chambre();
                chambre.num = j + 1 + "";
                console.log(chambre);
                this.TChambre.push(chambre);
            }
            this.TAdulte = new Array(this.nbrAdulte);
            this.TEnfant = new Array(this.nbrEnfant);
            this.TBebe = new Array(this.nbrBebe);
        }
        var decoded = jwt_decode(localStorage.getItem('token'));
        this.reservation.IdClient = decoded.id + "";
        this.reservation.Prix = this.prix;
        this.reservation.Titre = this.hotelreserver.nom;
        this.reservation.DateReservation = new Date().toString();
        this.reservation.DateDepart = this.d[0];
        this.reservation.DatRetour = this.d[1];
        this.reservation.Nom = this.reactiveform.value.Nom;
        this.reservation.Email = this.reactiveform.value.Email;
        this.reservation.Tel = this.reactiveform.value.Tel;
        console.log(this.reservation.Nom);
    };
    HreservationComponent.prototype.test = function () {
        this.TAdulte = new Array(this.nbrAdulte - 1);
        this.TEnfant = new Array(this.nbrEnfant - 1);
        this.TBebe = new Array(this.nbrBebe - 1);
        var j = 1;
        var k = 0;
        var t = [];
        for (var i = 0; i < this.nbrAdulte; i++) {
            var selectElement = document.getElementById('Adulte' + i);
            if (j < this.TnbrAdulte[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].adulte = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrEnfant; i++) {
            console.log(i);
            var selectElement2 = document.getElementById('Enfant' + i);
            if (j < this.TnbrEnfant[k]) {
                t.push(selectElement2.value + "");
                j += 1;
            }
            else {
                t.push(selectElement2.value + "");
                this.TChambre[k].enfant = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrBebe; i++) {
            var selectElement = document.getElementById('Bebe' + i);
            if (j < this.TnbrBebe[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].bb = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        this.reservation.Chambre = this.TChambre;
    };
    HreservationComponent.prototype.AjoutCambre = function () {
        var _this = this;
        this.serviceReservation.AjoutReservation(AjoutReservationUrl, this.reservation).subscribe(function (response) {
            console.log(response);
            _this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Voyage avec succée');
        }, function (err) { _this.toastr.error('', 'Verifiez vos Donnees'); });
    };
    HreservationComponent.prototype.pdf = function () {
        var options = {
            margin: 15,
            filename: 'myfile.pdf',
            image: { type: 'jpeg' },
            jsPDF: { orientation: 'landscape' }
        };
        var content = document.getElementById('content');
        html2pdf()
            .from(content)
            .set(options)
            .save();
    };
    tslib_1.__decorate([
        ViewChild('ngcarousel', { static: true }),
        tslib_1.__metadata("design:type", NgbCarousel)
    ], HreservationComponent.prototype, "ngCarousel", void 0);
    HreservationComponent = tslib_1.__decorate([
        Component({
            selector: 'app-hreservation',
            templateUrl: './hreservation.component.html',
            styleUrls: ['./hreservation.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [Router, ToastrService, ActivatedRoute, HotelsService, HttpClient, ReservationService])
    ], HreservationComponent);
    return HreservationComponent;
}());
export { HreservationComponent };
//# sourceMappingURL=hreservation.component.js.map