import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import * as jsPDF from 'jspdf';
import { ActivatedRoute } from '@angular/router';
import { VoyagesService } from '../../Services/voyages.service';
var ConfirmReservationComponent = /** @class */ (function () {
    function ConfirmReservationComponent(Activate, voyageService) {
        this.Activate = Activate;
        this.voyageService = voyageService;
    }
    ConfirmReservationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.id = this.Activate.snapshot.paramMap.get("id");
        this.voyageService.getUneReservation(this.id).subscribe(function (response) {
            _this.uneReservation = response;
            console.log(_this.uneReservation);
        });
    };
    ConfirmReservationComponent.prototype.pdf = function () {
        var doc = new jsPDF();
        var s = {
            "#editor": function (element, renderer) {
                return true;
            }
        };
        /*let content = this.content.nativeElement;
    
        doc.fromHTML(content.innerHTML,15,15 ,{
    
          'width' : 190,
          'elementHandlers' : s ,
    
    
        })
    doc.save('test.pdf');*/
    };
    ConfirmReservationComponent = tslib_1.__decorate([
        Component({
            selector: 'app-confirm-reservation',
            templateUrl: './confirm-reservation.component.html',
            styleUrls: ['./confirm-reservation.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ActivatedRoute, VoyagesService])
    ], ConfirmReservationComponent);
    return ConfirmReservationComponent;
}());
export { ConfirmReservationComponent };
//# sourceMappingURL=confirm-reservation.component.js.map