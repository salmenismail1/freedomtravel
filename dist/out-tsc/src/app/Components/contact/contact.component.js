import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ContactService } from '../../Services/contact.service';
import { Contact } from '../../Models/contact';
var ContactComponent = /** @class */ (function () {
    function ContactComponent(contactserv) {
        this.contactserv = contactserv;
        this.contact = new Contact;
    }
    ContactComponent.prototype.ngOnInit = function () {
    };
    ContactComponent.prototype.afficher = function () {
        console.log(this.contact);
        this.contactserv.postContact(this.contact).subscribe(function (response) {
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    ContactComponent = tslib_1.__decorate([
        Component({
            selector: 'app-contact',
            templateUrl: './contact.component.html',
            styleUrls: ['./contact.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [ContactService])
    ], ContactComponent);
    return ContactComponent;
}());
export { ContactComponent };
//# sourceMappingURL=contact.component.js.map