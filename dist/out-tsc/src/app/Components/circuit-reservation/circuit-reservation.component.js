import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Reservation } from '../../Models/reservation';
import { ActivatedRoute } from '@angular/router';
import { ReservationService } from '../../Services/reservation.service';
import { formatDate } from '@angular/common';
import { Chambre } from '../../Models/chambre';
import { ToastrService } from 'ngx-toastr';
import * as html2pdf from 'html2pdf.js';
import { FormControl, FormGroup, Validators } from "@angular/forms";
var AjoutReservationUrl = "http://localhost:3000/Soiree/api/reservation";
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
var CircuitReservationComponent = /** @class */ (function () {
    function CircuitReservationComponent(toastr, Activate, httpClient, serviceReservation) {
        this.toastr = toastr;
        this.Activate = Activate;
        this.httpClient = httpClient;
        this.serviceReservation = serviceReservation;
        this.reservation = new Reservation;
        this.nbChambre = new Array(5);
    }
    CircuitReservationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.reactiveform = new FormGroup({
            'Nom': new FormControl('', [Validators.required]),
            'Email': new FormControl('', [Validators.email, Validators.required]),
            'Tel': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('[0-9]*')]),
        });
        this.nb = 4;
        this.id = this.Activate.snapshot.paramMap.get("id");
        this.unCircuit = "https://topresa.ovh/api/circuits.json?departcircuits.date[after]=" + formatDate(new Date(), 'yyyy-MM-dd', 'en') + '&soiree.id=' + this.id;
        this.getUnCircuit().subscribe(function (response) {
            _this.first = response[0];
            _this.circuit = response;
            console.log(response);
        });
    };
    Object.defineProperty(CircuitReservationComponent.prototype, "Nom", {
        get: function () { return this.reactiveform.get('Nom'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CircuitReservationComponent.prototype, "Email", {
        get: function () { return this.reactiveform.get('Email'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CircuitReservationComponent.prototype, "Tel", {
        get: function () { return this.reactiveform.get('Tel'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CircuitReservationComponent.prototype, "input", {
        get: function () { return this.RoomReactiveform.get('required'); },
        enumerable: true,
        configurable: true
    });
    CircuitReservationComponent.prototype.getUnCircuit = function () {
        return this.httpClient.get(this.unCircuit, httpOptions);
    };
    CircuitReservationComponent.prototype.chambre = function () {
        for (var i = 0; i < this.nb; i++) {
            var selectElement = document.getElementById('rowid' + i);
            if (selectElement.hidden == true) {
                selectElement.hidden = false;
                console.log(selectElement);
                console.log(this.adult);
                return;
            }
        }
    };
    CircuitReservationComponent.prototype.delchambre = function (i) {
        var selectElement = document.getElementById('rowid' + i);
        selectElement.hidden = true;
        var select1 = document.getElementById('homme' + i);
        select1.selectedIndex = 0;
        var select2 = document.getElementById('enfant' + i);
        console.log(select2.selectedIndex);
        select2.selectedIndex = 0;
        var select3 = document.getElementById('bebe' + i);
        select3.selectedIndex = 0;
        console.log(selectElement);
        console.log(this.adult);
    };
    CircuitReservationComponent.prototype.ngAfterViewInit = function () {
        var selectElement = document.getElementById('rowid' + 0);
        selectElement.hidden = false;
        console.log(selectElement);
        console.log(this.adult);
    };
    CircuitReservationComponent.prototype.pdf = function () {
        var options = {
            margin: 15,
            filename: 'myfile.pdf',
            image: { type: 'jpeg' },
            jsPDF: { orientation: 'landscape' }
        };
        var content = document.getElementById('content');
        html2pdf()
            .from(content)
            .set(options)
            .save();
    };
    CircuitReservationComponent.prototype.affiche = function () {
        this.TnbrAdulte = new Array();
        this.TnbrEnfant = new Array();
        this.TnbrBebe = new Array();
        this.nbrAdulte = 0;
        this.nbrEnfant = 0;
        this.nbrBebe = 0;
        this.TChambre = [];
        var cmp = 0;
        for (var j = 0; j < this.nb; j++) {
            var selectElement = document.getElementById('rowid' + j);
            if (selectElement.hidden == false) {
                this.Adulte = document.getElementById('homme' + j);
                this.Enfant = document.getElementById('enfant' + j);
                this.Bebe = document.getElementById('bebe' + j);
                this.nbrAdulte += this.Adulte.selectedIndex;
                this.nbrEnfant += this.Enfant.selectedIndex;
                this.nbrBebe += this.Bebe.selectedIndex;
                this.TnbrAdulte[cmp] = this.Adulte.selectedIndex;
                this.TnbrEnfant[cmp] = this.Enfant.selectedIndex;
                this.TnbrBebe[cmp] = this.Bebe.selectedIndex;
                cmp += 1;
                var chambre = new Chambre();
                chambre.num = j + 1 + "";
                console.log(chambre);
                this.TChambre.push(chambre);
            }
            this.TAdulte = new Array(this.nbrAdulte);
            this.TEnfant = new Array(this.nbrEnfant);
            this.TBebe = new Array(this.nbrBebe);
        }
        this.reservation.Prix = this.prix;
        //this.reservation.Titre=this.x.titre;
        this.reservation.DateReservation = new Date().toString();
        this.reservation.DateDepart = formatDate(this.datedepart, 'yyyy-MM-dd', 'en');
        this.reservation.DatRetour = formatDate(this.dateretour, 'yyyy-MM-dd', 'en');
        this.reservation.Nom = this.reactiveform.value.Nom;
        this.reservation.Email = this.reactiveform.value.Email;
        this.reservation.Tel = this.reactiveform.value.Tel;
        console.log(this.reservation.Nom);
    };
    CircuitReservationComponent.prototype.test = function () {
        this.TAdulte = new Array(this.nbrAdulte - 1);
        this.TEnfant = new Array(this.nbrEnfant - 1);
        this.TBebe = new Array(this.nbrBebe - 1);
        var j = 1;
        var k = 0;
        var t = [];
        for (var i = 0; i < this.nbrAdulte; i++) {
            var selectElement = document.getElementById('Adulte' + i);
            if (j < this.TnbrAdulte[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].adulte = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrEnfant; i++) {
            console.log(i);
            var selectElement2 = document.getElementById('Enfant' + i);
            if (j < this.TnbrEnfant[k]) {
                t.push(selectElement2.value + "");
                j += 1;
            }
            else {
                t.push(selectElement2.value + "");
                this.TChambre[k].enfant = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrBebe; i++) {
            var selectElement = document.getElementById('Bebe' + i);
            if (j < this.TnbrBebe[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].bb = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        this.reservation.Chambre = this.TChambre;
    };
    CircuitReservationComponent.prototype.AjoutCambre = function () {
        var _this = this;
        this.serviceReservation.AjoutReservation(AjoutReservationUrl, this.reservation).subscribe(function (response) {
            console.log(response);
            _this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Voyage avec succée');
        }, function (err) { _this.toastr.error('', 'Verifiez vos Donnees'); });
    };
    CircuitReservationComponent = tslib_1.__decorate([
        Component({
            selector: 'app-circuit-reservation',
            templateUrl: './circuit-reservation.component.html',
            styleUrls: ['./circuit-reservation.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ToastrService, ActivatedRoute, HttpClient, ReservationService])
    ], CircuitReservationComponent);
    return CircuitReservationComponent;
}());
export { CircuitReservationComponent };
//# sourceMappingURL=circuit-reservation.component.js.map