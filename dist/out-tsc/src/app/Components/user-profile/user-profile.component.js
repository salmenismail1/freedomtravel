import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { LoginService } from '../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
var UserProfileComponent = /** @class */ (function () {
    function UserProfileComponent(userServices) {
        this.userServices = userServices;
        this.loading = true;
    }
    UserProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        var decoded = jwt_decode(this.userServices.getToken());
        this.userServices.getOnUser(decoded.id).subscribe(function (response) {
            _this.user = response;
        });
        this.loading = false;
    };
    UserProfileComponent.prototype.ModifierUser = function (id) {
        this.userServices.ModifUser(id, this.user).subscribe();
        console.log(this.user);
    };
    UserProfileComponent = tslib_1.__decorate([
        Component({
            selector: 'app-user-profile',
            templateUrl: './user-profile.component.html',
            styleUrls: ['./user-profile.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [LoginService])
    ], UserProfileComponent);
    return UserProfileComponent;
}());
export { UserProfileComponent };
//# sourceMappingURL=user-profile.component.js.map