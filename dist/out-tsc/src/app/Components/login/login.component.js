import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormControl, Validators, FormBuilder } from "@angular/forms";
import { MatDialogRef } from '@angular/material/dialog';
import { LoginService } from '../../Services/login.service';
import { Login } from '../../Models/login';
import { User } from '../../Models/user';
import * as jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppVarsGlobal } from '../../app.vars.global';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(toastr, dialogRef, loginserv, formbuilder, router, vars) {
        this.toastr = toastr;
        this.dialogRef = dialogRef;
        this.loginserv = loginserv;
        this.formbuilder = formbuilder;
        this.router = router;
        this.vars = vars;
        this.log = new Login;
        this.user = new User;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.reactiveform = this.formbuilder.group({
            'username': new FormControl('', [Validators.required]),
            'email': new FormControl('', [Validators.email, Validators.required]),
            'password': new FormControl('', [Validators.required]),
            'repassword': new FormControl('', [Validators.required]),
            'cin': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('[0-9]*')]),
            'tel': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('[0-9]*')]),
        }, { validator: this.passwordMatchValidator });
        this.reactiveform.value.username = "";
    };
    LoginComponent.prototype.passwordMatchValidator = function (frm) {
        return frm.get('password').value === frm.get('repassword').value
            ? null : { 'mismatch': true };
    };
    LoginComponent.prototype.actionFunction = function () {
        alert("You have logged out.");
        this.closeModal();
    };
    LoginComponent.prototype.closeModal = function () {
        this.dialogRef.close();
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.loginserv.postLogin(this.log).subscribe(function (response) {
            localStorage.setItem('token', response.token);
            var decoded = jwt_decode(response.token);
            _this.dialogRef.close();
            //****** */
            _this.redirect(decoded.id);
        }, function (err) { _this.toastr.error('', 'Verifiez vos Donnees'); });
    };
    LoginComponent.prototype.redirect = function (id) {
        var _this = this;
        this.loginserv.getOnUser(id).subscribe(function (response) {
            console.log(response);
            if (response.type === "admin") {
                _this.router.navigate(['/admin/dashboard']);
            }
            else {
                _this.router.navigate(['/home']);
            }
            _this.toastr.success('', 'Autontification avec succée');
            _this.vars.type = response.type;
        });
    };
    LoginComponent.prototype.inscription = function () {
        var _this = this;
        this.user.type = "client";
        this.loginserv.Inscrit(this.user).subscribe(function (response) {
            _this.dialogRef.close();
        }, function (err) { console.log('erreur dinscription'); });
    };
    LoginComponent = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css'],
        }),
        tslib_1.__metadata("design:paramtypes", [ToastrService, MatDialogRef, LoginService, FormBuilder,
            Router, AppVarsGlobal])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map