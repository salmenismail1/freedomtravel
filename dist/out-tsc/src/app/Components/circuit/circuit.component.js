import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CircuitService } from '../../Services/circuit.service';
import { Circuit } from '../../Models/circuit';
var CircuitComponent = /** @class */ (function () {
    function CircuitComponent(circuitServices) {
        this.circuitServices = circuitServices;
        this.circuit = new Circuit;
    }
    CircuitComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.circuitServices.getCircuit().subscribe(function (response) {
            _this.ListeCircuits = response;
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    CircuitComponent = tslib_1.__decorate([
        Component({
            selector: 'app-circuit',
            templateUrl: './circuit.component.html',
            styleUrls: ['./circuit.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [CircuitService])
    ], CircuitComponent);
    return CircuitComponent;
}());
export { CircuitComponent };
//# sourceMappingURL=circuit.component.js.map