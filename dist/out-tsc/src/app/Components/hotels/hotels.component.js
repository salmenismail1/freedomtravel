import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HotelsService } from '../../Services/hotels.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
var HotelsComponent = /** @class */ (function () {
    function HotelsComponent(hotelServices, sanitizer, router) {
        this.hotelServices = hotelServices;
        this.sanitizer = sanitizer;
        this.router = router;
        this.loading = true;
        this.test = "";
    }
    HotelsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.hotelServices.getHotel().subscribe(function (response) {
            _this.loading = false;
            _this.ListeHotels = response;
            console.log(response);
        }, function (err) {
            "error";
        });
        this.hotelServices.getVille().subscribe(function (ville) {
            _this.listVille = ville;
        }, function (error) { return console.log(error); });
    };
    HotelsComponent.prototype.getSanitizerUrl = function (url) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    };
    HotelsComponent.prototype.getPrix = function (hotel) {
        return this.hotelServices.getPrix(hotel);
    };
    HotelsComponent.prototype.testing1 = function () {
        console.log(this.test);
        this.test = "";
        this.reset();
    };
    HotelsComponent.prototype.testing2 = function () {
        console.log(this.test);
        this.test = "ville";
        this.test = "f";
        this.text = "";
    };
    HotelsComponent.prototype.testing3 = function (s) {
        this.etoile = s;
        console.log(this.test);
        this.test = "etoile";
        this.test = "f";
        this.text = "";
    };
    HotelsComponent.prototype.testing4 = function () {
        this.test = "prix";
        console.log("min : " + this.min + "max : " + this.max);
        this.test = "f";
        this.text = "";
    };
    HotelsComponent.prototype.reset = function () {
        this.min = 0;
        this.max = 300;
        this.ville = "Liste des Villes";
    };
    HotelsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-hotels',
            templateUrl: './hotels.component.html',
            styleUrls: ['./hotels.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [HotelsService, DomSanitizer, Router])
    ], HotelsComponent);
    return HotelsComponent;
}());
export { HotelsComponent };
//# sourceMappingURL=hotels.component.js.map