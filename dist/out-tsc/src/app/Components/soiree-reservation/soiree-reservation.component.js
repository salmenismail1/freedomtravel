import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { formatDate } from '@angular/common';
import { Reservation } from '../../Models/reservation';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ReservationService } from '../../Services/reservation.service';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Chambre } from '../../Models/chambre';
import { ToastrService } from 'ngx-toastr';
import * as html2pdf from 'html2pdf.js';
var AjoutReservationUrl = "http://localhost:3000/Soiree/api/reservation";
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
var SoireeReservationComponent = /** @class */ (function () {
    function SoireeReservationComponent(router, toastr, Activate, httpClient, serviceReservation) {
        this.router = router;
        this.toastr = toastr;
        this.Activate = Activate;
        this.httpClient = httpClient;
        this.serviceReservation = serviceReservation;
        this.reservation = new Reservation;
        this.nbChambre = new Array(5);
    }
    SoireeReservationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.nb = 4;
        this.id = this.Activate.snapshot.paramMap.get("id");
        this.uneSoiree = "https://topresa.ovh/api/departsoirees.json?date[before]=" + formatDate(new Date(), 'yyyy-MM-dd', 'en') + '&soiree.id=' + this.id;
        this.reactiveform = new FormGroup({
            'Nom': new FormControl('', [Validators.required]),
            'Email': new FormControl('', [Validators.email, Validators.required]),
            'Tel': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('[0-9]*')]),
        });
        this.getUneSoiree().subscribe(function (response) {
            _this.first = response[0];
            _this.soiree = response;
            console.log(response);
        });
    };
    Object.defineProperty(SoireeReservationComponent.prototype, "Nom", {
        get: function () { return this.reactiveform.get('Nom'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SoireeReservationComponent.prototype, "Email", {
        get: function () { return this.reactiveform.get('Email'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SoireeReservationComponent.prototype, "Tel", {
        get: function () { return this.reactiveform.get('Tel'); },
        enumerable: true,
        configurable: true
    });
    SoireeReservationComponent.prototype.getUneSoiree = function () {
        return this.httpClient.get(this.uneSoiree, httpOptions);
    };
    SoireeReservationComponent.prototype.chambre = function () {
        for (var i = 0; i < this.nb; i++) {
            var selectElement = document.getElementById('rowid' + i);
            if (selectElement.hidden == true) {
                selectElement.hidden = false;
                console.log(selectElement);
                console.log(this.adult);
                return;
            }
        }
    };
    SoireeReservationComponent.prototype.delchambre = function (i) {
        var selectElement = document.getElementById('rowid' + i);
        selectElement.hidden = true;
        var select1 = document.getElementById('homme' + i);
        select1.selectedIndex = 0;
        var select2 = document.getElementById('enfant' + i);
        console.log(select2.selectedIndex);
        select2.selectedIndex = 0;
        var select3 = document.getElementById('bebe' + i);
        select3.selectedIndex = 0;
        console.log(selectElement);
        console.log(this.adult);
    };
    SoireeReservationComponent.prototype.verif = function () {
        console.log(this.e);
    };
    SoireeReservationComponent.prototype.ngAfterViewInit = function () {
        var selectElement = document.getElementById('rowid' + 0);
        selectElement.hidden = false;
        console.log(selectElement);
        console.log(this.adult);
    };
    SoireeReservationComponent.prototype.Prix = function (venteadultesingle, venteadultedble, tarifbebe, datedepart, dateretour) {
        this.venteadultesingle = venteadultesingle;
        this.venteadultedble = venteadultedble;
        this.tarifbebe = tarifbebe;
        this.datedepart = datedepart;
        this.dateretour = dateretour;
    };
    SoireeReservationComponent.prototype.calculPrix = function () {
        this.prix = 0;
        for (var j = 0; j < this.nb; j++) {
            var selectElement = document.getElementById('rowid' + j);
            if (selectElement.hidden == false) {
                var select1 = document.getElementById('homme' + j);
                var select2 = document.getElementById('enfant' + j);
                var select3 = document.getElementById('bebe' + j);
                this.prix += (select1.selectedIndex * this.venteadultesingle) + (select2.selectedIndex * this.venteadultedble) + (select3.selectedIndex * this.tarifbebe);
                console.log(this.prix);
            }
        }
        console.log(this.prix);
    };
    SoireeReservationComponent.prototype.clear = function () {
        this.reactiveform.reset();
        for (var i = 0; i < this.nb; i++) {
            var selectElement = document.getElementById('rowid' + i);
            selectElement.hidden = true;
            var firstElement = document.getElementById('rowid' + 0);
            firstElement.hidden = false;
            var select1 = document.getElementById('homme' + i);
            select1.selectedIndex = 0;
            var select2 = document.getElementById('enfant' + i);
            select2.selectedIndex = 0;
            var select3 = document.getElementById('bebe' + i);
            select3.selectedIndex = 0;
        }
        this.prix = 0;
    };
    SoireeReservationComponent.prototype.pdf = function () {
        var options = {
            margin: 15,
            filename: 'myfile.pdf',
            image: { type: 'jpeg' },
            jsPDF: { orientation: 'landscape' }
        };
        var content = document.getElementById('content');
        html2pdf()
            .from(content)
            .set(options)
            .save();
    };
    SoireeReservationComponent.prototype.affiche = function () {
        this.TnbrAdulte = new Array();
        this.TnbrEnfant = new Array();
        this.TnbrBebe = new Array();
        this.nbrAdulte = 0;
        this.nbrEnfant = 0;
        this.nbrBebe = 0;
        this.TChambre = [];
        var cmp = 0;
        for (var j = 0; j < this.nb; j++) {
            var selectElement = document.getElementById('rowid' + j);
            if (selectElement.hidden == false) {
                this.Adulte = document.getElementById('homme' + j);
                this.Enfant = document.getElementById('enfant' + j);
                this.Bebe = document.getElementById('bebe' + j);
                this.nbrAdulte += this.Adulte.selectedIndex;
                this.nbrEnfant += this.Enfant.selectedIndex;
                this.nbrBebe += this.Bebe.selectedIndex;
                this.TnbrAdulte[cmp] = this.Adulte.selectedIndex;
                this.TnbrEnfant[cmp] = this.Enfant.selectedIndex;
                this.TnbrBebe[cmp] = this.Bebe.selectedIndex;
                cmp += 1;
                var chambre = new Chambre();
                chambre.num = j + 1 + "";
                console.log(chambre);
                this.TChambre.push(chambre);
            }
            this.TAdulte = new Array(this.nbrAdulte);
            this.TEnfant = new Array(this.nbrEnfant);
            this.TBebe = new Array(this.nbrBebe);
        }
        this.reservation.Prix = this.prix;
        this.reservation.Titre = this.first.titre;
        this.reservation.DateReservation = new Date().toString();
        this.reservation.DateDepart = formatDate(this.datedepart, 'yyyy-MM-dd', 'en');
        this.reservation.DatRetour = formatDate(this.dateretour, 'yyyy-MM-dd', 'en');
        this.reservation.Nom = this.reactiveform.value.Nom;
        this.reservation.Email = this.reactiveform.value.Email;
        this.reservation.Tel = this.reactiveform.value.Tel;
        console.log(this.reservation.Nom);
    };
    SoireeReservationComponent.prototype.test = function () {
        this.TAdulte = new Array(this.nbrAdulte - 1);
        this.TEnfant = new Array(this.nbrEnfant - 1);
        this.TBebe = new Array(this.nbrBebe - 1);
        var j = 1;
        var k = 0;
        var t = [];
        for (var i = 0; i < this.nbrAdulte; i++) {
            var selectElement = document.getElementById('Adulte' + i);
            if (j < this.TnbrAdulte[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].adulte = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrEnfant; i++) {
            console.log(i);
            var selectElement2 = document.getElementById('Enfant' + i);
            if (j < this.TnbrEnfant[k]) {
                t.push(selectElement2.value + "");
                j += 1;
            }
            else {
                t.push(selectElement2.value + "");
                this.TChambre[k].enfant = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        j = 1;
        k = 0;
        t = [];
        for (var i = 0; i < this.nbrBebe; i++) {
            var selectElement = document.getElementById('Bebe' + i);
            if (j < this.TnbrBebe[k]) {
                t.push(selectElement.value + "");
                j += 1;
            }
            else {
                t.push(selectElement.value + "");
                this.TChambre[k].bb = t;
                t = [];
                j = 1;
                k += 1;
            }
        }
        this.reservation.Chambre = this.TChambre;
    };
    SoireeReservationComponent.prototype.AjoutCambre = function () {
        var _this = this;
        this.serviceReservation.AjoutReservation(AjoutReservationUrl, this.reservation).subscribe(function (response) {
            console.log(response);
            _this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Voyage avec succée');
        }, function (err) { _this.toastr.error('', 'Verifiez vos Donnees'); });
    };
    SoireeReservationComponent = tslib_1.__decorate([
        Component({
            selector: 'app-soiree-reservation',
            templateUrl: './soiree-reservation.component.html',
            styleUrls: ['./soiree-reservation.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [Router, ToastrService, ActivatedRoute, HttpClient, ReservationService])
    ], SoireeReservationComponent);
    return SoireeReservationComponent;
}());
export { SoireeReservationComponent };
//# sourceMappingURL=soiree-reservation.component.js.map