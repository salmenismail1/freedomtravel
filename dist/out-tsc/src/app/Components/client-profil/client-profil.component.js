import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { LoginService } from '../../Services/login.service';
var ClientProfilComponent = /** @class */ (function () {
    function ClientProfilComponent(userServices) {
        this.userServices = userServices;
    }
    ClientProfilComponent.prototype.ngOnInit = function () {
        /**  var decoded = jwt_decode(this.userServices.getToken());
         this.userServices.getOnUser(decoded.id).subscribe(
           response => {
             
             this.user= response ;
             console.log(this.user)
         })*/
    };
    ClientProfilComponent.prototype.ModifierUser = function (id) {
        this.userServices.ModifUser(id, this.user).subscribe();
        console.log(this.user);
    };
    ClientProfilComponent = tslib_1.__decorate([
        Component({
            selector: 'app-client-profil',
            templateUrl: './client-profil.component.html',
            styleUrls: ['./client-profil.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LoginService])
    ], ClientProfilComponent);
    return ClientProfilComponent;
}());
export { ClientProfilComponent };
//# sourceMappingURL=client-profil.component.js.map