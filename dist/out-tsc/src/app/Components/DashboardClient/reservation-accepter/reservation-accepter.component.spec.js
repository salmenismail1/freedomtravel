import { async, TestBed } from '@angular/core/testing';
import { ReservationAccepterComponent } from './reservation-accepter.component';
describe('ReservationAccepterComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ReservationAccepterComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ReservationAccepterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=reservation-accepter.component.spec.js.map