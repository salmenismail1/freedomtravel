import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { LoginService } from '../../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
import { MatDialog, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import * as html2pdf from 'html2pdf.js';
import { Reservation } from '../../../Models/reservation';
var ReservationAccepterComponent = /** @class */ (function () {
    function ReservationAccepterComponent(userServices, matDialog, router) {
        this.userServices = userServices;
        this.matDialog = matDialog;
        this.router = router;
        this.ReservationAccepterColumns = ['Titre', 'Etat', 'Nom', 'actions'];
        this.reservation = new Reservation;
    }
    ReservationAccepterComponent.prototype.ngOnInit = function () {
        this.GetListeReservationAccepter();
    };
    ReservationAccepterComponent.prototype.onSearchClear = function () {
        this.searchKey = "";
        this.applyFilter();
    };
    ReservationAccepterComponent.prototype.applyFilter = function () {
        this.listeReservationAccepter.filter = this.searchKey.trim().toLowerCase();
    };
    ReservationAccepterComponent.prototype.GetListeReservationAccepter = function () {
        var _this = this;
        var decoded = jwt_decode(this.userServices.getToken());
        this.userServices.getReservationHotelClientEncour(decoded.id).subscribe(function (response) {
            _this.LReservationAccepter = response;
            _this.listeReservationAccepter = new MatTableDataSource(_this.LReservationAccepter);
            _this.listeReservationAccepter.sort = _this.sort;
            _this.listeReservationAccepter.paginator = _this.paginator;
            _this.listeReservationAccepter.filterPredicate = function (data, filter) {
                return _this.ReservationAccepterColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
                });
            };
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    ReservationAccepterComponent.prototype.getReservation = function (id) {
        var _this = this;
        this.userServices.getReservation(id).subscribe(function (response) {
            _this.reservation = response[0];
            console.log(_this.reservation);
            console.log(response);
        });
    };
    ReservationAccepterComponent.prototype.pdf = function () {
        var options = {
            margin: 15,
            filename: 'myfile.pdf',
            image: { type: 'jpeg' },
            jsPDF: { orientation: 'landscape' }
        };
        var content = document.getElementById('content');
        html2pdf()
            .from(content)
            .set(options)
            .save();
    };
    ReservationAccepterComponent.prototype.deleteReservation = function (id) {
        this.userServices.RemoveReservation(id).subscribe(function (response) {
            console.log(response);
        });
        this.GetListeReservationAccepter();
    };
    tslib_1.__decorate([
        ViewChild(MatSort, null),
        tslib_1.__metadata("design:type", MatSort)
    ], ReservationAccepterComponent.prototype, "sort", void 0);
    tslib_1.__decorate([
        ViewChild(MatPaginator, null),
        tslib_1.__metadata("design:type", MatPaginator)
    ], ReservationAccepterComponent.prototype, "paginator", void 0);
    ReservationAccepterComponent = tslib_1.__decorate([
        Component({
            selector: 'app-reservation-accepter',
            templateUrl: './reservation-accepter.component.html',
            styleUrls: ['./reservation-accepter.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LoginService, MatDialog, Router])
    ], ReservationAccepterComponent);
    return ReservationAccepterComponent;
}());
export { ReservationAccepterComponent };
//# sourceMappingURL=reservation-accepter.component.js.map