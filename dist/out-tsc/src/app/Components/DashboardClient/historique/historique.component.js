import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { LoginService } from '../../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
import { MatDialog, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
var HistoriqueComponent = /** @class */ (function () {
    function HistoriqueComponent(userServices, matDialog, router) {
        this.userServices = userServices;
        this.matDialog = matDialog;
        this.router = router;
        this.HistoriqueColumns = ['Titre', 'Etat', 'Nom'];
    }
    HistoriqueComponent.prototype.ngOnInit = function () {
        this.GetListeReservationAccepter();
    };
    HistoriqueComponent.prototype.onSearchClear = function () {
        this.searchKey = "";
        this.applyFilter();
    };
    HistoriqueComponent.prototype.applyFilter = function () {
        this.listeReservationAccepter.filter = this.searchKey.trim().toLowerCase();
    };
    HistoriqueComponent.prototype.GetListeReservationAccepter = function () {
        var _this = this;
        var decoded = jwt_decode(this.userServices.getToken());
        this.userServices.getHistorique(decoded.id).subscribe(function (response) {
            _this.LReservationAccepter = response;
            _this.listeReservationAccepter = new MatTableDataSource(_this.LReservationAccepter);
            _this.listeReservationAccepter.sort = _this.sort;
            _this.listeReservationAccepter.paginator = _this.paginator;
            _this.listeReservationAccepter.filterPredicate = function (data, filter) {
                return _this.HistoriqueColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
            };
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    tslib_1.__decorate([
        ViewChild(MatSort, null),
        tslib_1.__metadata("design:type", MatSort)
    ], HistoriqueComponent.prototype, "sort", void 0);
    tslib_1.__decorate([
        ViewChild(MatPaginator, null),
        tslib_1.__metadata("design:type", MatPaginator)
    ], HistoriqueComponent.prototype, "paginator", void 0);
    HistoriqueComponent = tslib_1.__decorate([
        Component({
            selector: 'app-historique',
            templateUrl: './historique.component.html',
            styleUrls: ['./historique.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LoginService, MatDialog, Router])
    ], HistoriqueComponent);
    return HistoriqueComponent;
}());
export { HistoriqueComponent };
//# sourceMappingURL=historique.component.js.map