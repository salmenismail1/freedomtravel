import { async, TestBed } from '@angular/core/testing';
import { ReservationEnCourComponent } from './reservation-en-cour.component';
describe('ReservationEnCourComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ReservationEnCourComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ReservationEnCourComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=reservation-en-cour.component.spec.js.map