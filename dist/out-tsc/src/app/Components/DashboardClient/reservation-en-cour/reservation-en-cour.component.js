import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { LoginService } from '../../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
import { MatDialog, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
var ReservationEnCourComponent = /** @class */ (function () {
    function ReservationEnCourComponent(userServices, matDialog, router) {
        this.userServices = userServices;
        this.matDialog = matDialog;
        this.router = router;
        this.ReservationEnCourColumns = ['Titre', 'Etat', 'Nom', 'actions'];
    }
    ReservationEnCourComponent.prototype.ngOnInit = function () {
        this.GetListeReservationEnCour();
    };
    ReservationEnCourComponent.prototype.onSearchClear = function () {
        this.searchKey = "";
        this.applyFilter();
    };
    ReservationEnCourComponent.prototype.applyFilter = function () {
        this.listeReservationEnCour.filter = this.searchKey.trim().toLowerCase();
    };
    ReservationEnCourComponent.prototype.GetListeReservationEnCour = function () {
        var _this = this;
        var decoded = jwt_decode(this.userServices.getToken());
        this.userServices.getReservationVoyageClientEncour(decoded.id).subscribe(function (response) {
            _this.LReservationEnCour = response;
            _this.listeReservationEnCour = new MatTableDataSource(_this.LReservationEnCour);
            _this.listeReservationEnCour.sort = _this.sort;
            _this.listeReservationEnCour.paginator = _this.paginator;
            _this.listeReservationEnCour.filterPredicate = function (data, filter) {
                return _this.ReservationEnCourColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
            };
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    tslib_1.__decorate([
        ViewChild(MatSort, null),
        tslib_1.__metadata("design:type", MatSort)
    ], ReservationEnCourComponent.prototype, "sort", void 0);
    tslib_1.__decorate([
        ViewChild(MatPaginator, null),
        tslib_1.__metadata("design:type", MatPaginator)
    ], ReservationEnCourComponent.prototype, "paginator", void 0);
    ReservationEnCourComponent = tslib_1.__decorate([
        Component({
            selector: 'app-reservation-en-cour',
            templateUrl: './reservation-en-cour.component.html',
            styleUrls: ['./reservation-en-cour.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LoginService, MatDialog, Router])
    ], ReservationEnCourComponent);
    return ReservationEnCourComponent;
}());
export { ReservationEnCourComponent };
//# sourceMappingURL=reservation-en-cour.component.js.map