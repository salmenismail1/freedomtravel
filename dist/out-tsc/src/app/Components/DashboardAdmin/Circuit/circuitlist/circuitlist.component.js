import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { AddCircuitComponent as ModalComponent } from '../add-circuit/add-circuit.component';
import { CircuitmodifComponent as ModalComponent2 } from '../circuitmodif/circuitmodif.component';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { CircuitService } from '../../../../Services/circuit.service';
var CircuitlistComponent = /** @class */ (function () {
    function CircuitlistComponent(circuitServices, matDialog, router) {
        this.circuitServices = circuitServices;
        this.matDialog = matDialog;
        this.router = router;
        this.displayedColumns = ['id', 'nom', 'actions'];
    }
    CircuitlistComponent.prototype.ngOnInit = function () {
        this.getData();
    };
    CircuitlistComponent.prototype.onSearchClear = function () {
        this.searchKey = "";
        this.applyFilter();
    };
    CircuitlistComponent.prototype.applyFilter = function () {
        this.listData.filter = this.searchKey.trim().toLowerCase();
    };
    CircuitlistComponent.prototype.openModal = function () {
        var _this = this;
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modal-component-voyage";
        var g = this.matDialog.open(ModalComponent, {
            height: '500px',
            width: '500px',
        });
        g.afterClosed().subscribe(function () {
            // Do stuff after the dialog has closed
            _this.getData();
        });
    };
    CircuitlistComponent.prototype.openModifModal = function (item) {
        var _this = this;
        console.log(item);
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modifmodal-component-voyage";
        var g = this.matDialog.open(ModalComponent2, {
            height: '500px',
            width: '500px',
            data: { comp: item },
        });
        g.afterClosed().subscribe(function () {
            // Do stuff after the dialog has closed
            _this.getData();
        });
        console.log(item);
    };
    CircuitlistComponent.prototype.delete = function (id) {
        var _this = this;
        this.circuitServices.deletePost(id);
        this.postsSub = this.circuitServices.getPostUpdateListener()
            .subscribe(function (posts) {
            _this.l = posts;
        });
        setTimeout(function () {
            console.log(_this.l);
            _this.listData = new MatTableDataSource(_this.l);
            _this.listData.sort = _this.sort;
            _this.listData.paginator = _this.paginator;
            _this.listData.filterPredicate = function (data, filter) {
                return _this.displayedColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 5000);
    };
    CircuitlistComponent.prototype.getData = function () {
        var _this = this;
        this.circuitServices.getCircuitBase();
        this.postsSub = this.circuitServices.getPostUpdateListener()
            .subscribe(function (posts) {
            _this.l = posts;
        });
        setTimeout(function () {
            console.log(_this.l);
            _this.listData = new MatTableDataSource(_this.l);
            _this.listData.sort = _this.sort;
            _this.listData.paginator = _this.paginator;
            _this.listData.filterPredicate = function (data, filter) {
                return _this.displayedColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 4000);
    };
    tslib_1.__decorate([
        ViewChild(MatSort, null),
        tslib_1.__metadata("design:type", MatSort)
    ], CircuitlistComponent.prototype, "sort", void 0);
    tslib_1.__decorate([
        ViewChild(MatPaginator, null),
        tslib_1.__metadata("design:type", MatPaginator)
    ], CircuitlistComponent.prototype, "paginator", void 0);
    CircuitlistComponent = tslib_1.__decorate([
        Component({
            selector: 'app-circuitlist',
            templateUrl: './circuitlist.component.html',
            styleUrls: ['./circuitlist.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [CircuitService, MatDialog, Router])
    ], CircuitlistComponent);
    return CircuitlistComponent;
}());
export { CircuitlistComponent };
//# sourceMappingURL=circuitlist.component.js.map