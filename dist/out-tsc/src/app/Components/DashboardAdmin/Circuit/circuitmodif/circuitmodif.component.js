import * as tslib_1 from "tslib";
import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CircuitService } from '../../../../Services/circuit.service';
var CircuitmodifComponent = /** @class */ (function () {
    function CircuitmodifComponent(dialogRef, circuitServices, http, data) {
        this.dialogRef = dialogRef;
        this.circuitServices = circuitServices;
        this.http = http;
        this.data = data;
        this.title = 'fileUpload';
        this.multipleImages = [];
        this.imagePreview = '';
        this.imageverif = false;
    }
    CircuitmodifComponent.prototype.ngOnInit = function () {
        this.form = new FormGroup({
            id: new FormControl(null, {
                validators: [Validators.required]
            }),
            nom: new FormControl(null, { validators: [Validators.required] }),
            description: new FormControl(null, { validators: [Validators.required] }),
            libelle: new FormControl(null, { validators: [Validators.required] }),
            programme: new FormControl(null, { validators: [Validators.required] }),
            venteadultesingle: new FormControl(null, { validators: [Validators.required] }),
            venteadultedble: new FormControl(null, { validators: [Validators.required] }),
            vente3rdad: new FormControl(null, { validators: [Validators.required] }),
            venteenfant2ad: new FormControl(null, { validators: [Validators.required] }),
            tarifbebe: new FormControl(null, { validators: [Validators.required] }),
            venteenfant1ad: new FormControl(null, { validators: [Validators.required] }),
            datedepart: new FormControl(null, { validators: [Validators.required] }),
            dateretour: new FormControl(null, { validators: [Validators.required] }),
            image: new FormControl(null, {
                validators: [Validators.required]
            })
        });
    };
    CircuitmodifComponent.prototype.selectImage = function (event) {
        var _this = this;
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.form.patchValue({ image: file });
            this.form.get("image").updateValueAndValidity();
            this.images = file;
            var reader_1 = new FileReader();
            reader_1.onload = function () {
                _this.imagePreview = reader_1.result;
            };
            reader_1.readAsDataURL(file);
            if (file.type == "image/png" || file.type == "image/jpeg" || file.type == "image/jpg" || file.type == "image/svg") {
                this.imageverif = true;
                console.log(this.imageverif);
            }
            else {
                this.imageverif = false;
                console.log(this.imageverif);
            }
        }
    };
    CircuitmodifComponent.prototype.selectMultipleImage = function (event) {
        if (event.target.files.length > 0) {
            this.multipleImages = event.target.files;
        }
    };
    CircuitmodifComponent.prototype.onSubmit = function (idg) {
        console.log(idg);
        if (this.form.value.id != "" || this.form.value.nom != "" || this.form.value.name != "" || this.imageverif == true) {
            this.circuitServices.update(this.form.value, idg);
            var formData = new FormData();
            formData.append('file', this.images);
            console.log(formData);
            this.http.post("http://localhost:3000/hotel/file", formData).subscribe(function (res) { return console.log(res); }, function (err) { return console.log(err); });
            /*ng serve --live-reload false
            location.reload();
                  window.stop();
      
            */
        }
        else {
            if (this.imageverif == false) {
                alert('image non valid');
            }
            else {
                alert('remplir tous les correctement');
            }
        }
    };
    CircuitmodifComponent.prototype.onMultipleSubmit = function () {
        var formData = new FormData();
        for (var _i = 0, _a = this.multipleImages; _i < _a.length; _i++) {
            var img = _a[_i];
            formData.append('files', img);
        }
        this.http.post('http://localhost:3000/multipleFiles', formData).subscribe(function (res) { return console.log(res); }, function (err) { return console.log(err); });
    };
    CircuitmodifComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    CircuitmodifComponent = tslib_1.__decorate([
        Component({
            selector: 'app-circuitmodif',
            templateUrl: './circuitmodif.component.html',
            styleUrls: ['./circuitmodif.component.scss']
        }),
        tslib_1.__param(3, Inject(MAT_DIALOG_DATA)),
        tslib_1.__metadata("design:paramtypes", [MatDialogRef, CircuitService, HttpClient, Object])
    ], CircuitmodifComponent);
    return CircuitmodifComponent;
}());
export { CircuitmodifComponent };
//# sourceMappingURL=circuitmodif.component.js.map