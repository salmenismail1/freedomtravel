import { async, TestBed } from '@angular/core/testing';
import { CircuitmodifComponent } from './circuitmodif.component';
describe('CircuitmodifComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [CircuitmodifComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(CircuitmodifComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=circuitmodif.component.spec.js.map