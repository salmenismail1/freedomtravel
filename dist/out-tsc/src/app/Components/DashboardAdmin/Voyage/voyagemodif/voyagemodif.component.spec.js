import { async, TestBed } from '@angular/core/testing';
import { VoyagemodifComponent } from './voyagemodif.component';
describe('VoyagemodifComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [VoyagemodifComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(VoyagemodifComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=voyagemodif.component.spec.js.map