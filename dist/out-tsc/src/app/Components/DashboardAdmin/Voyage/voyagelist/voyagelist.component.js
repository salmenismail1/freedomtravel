import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { AddVoyageComponent as ModalComponent } from '../add-voyage/add-voyage.component';
import { VoyagemodifComponent as ModalComponent2 } from '../voyagemodif/voyagemodif.component';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { VoyagesService } from '../../../../Services/voyages.service';
var VoyagelistComponent = /** @class */ (function () {
    function VoyagelistComponent(voyageServices, matDialog, router) {
        this.voyageServices = voyageServices;
        this.matDialog = matDialog;
        this.router = router;
        this.displayedColumns = ['id', 'nom', 'actions'];
        this.ReservationHotelColumns = ['Titre', 'Email', 'Prix', 'actions'];
        this.ReservationHotelColumns2 = ['Titre', 'Email', 'Prix', 'actions'];
    }
    VoyagelistComponent.prototype.ngOnInit = function () {
        //this.getData();
        this.GetListeReservationAccepter();
        this.GetListeReservationEnAttente();
    };
    VoyagelistComponent.prototype.onSearchClear = function () {
        this.searchKey = "";
        this.applyFilter();
    };
    VoyagelistComponent.prototype.onSearchClear2 = function () {
        this.searchKey2 = "";
        this.applyFilter2();
    };
    VoyagelistComponent.prototype.onSearchClear3 = function () {
        this.searchKey3 = "";
        this.applyFilter3();
    };
    VoyagelistComponent.prototype.applyFilter = function () {
        this.listData.filter = this.searchKey.trim().toLowerCase();
    };
    VoyagelistComponent.prototype.applyFilter2 = function () {
        this.listeReservationAccepter.filter = this.searchKey2.trim().toLowerCase();
    };
    VoyagelistComponent.prototype.applyFilter3 = function () {
        this.listeReservationEnAttente.filter = this.searchKey3.trim().toLowerCase();
    };
    VoyagelistComponent.prototype.openModal = function () {
        var _this = this;
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modal-component-voyage";
        var g = this.matDialog.open(ModalComponent, {
            height: '500px',
            width: '500px',
        });
        g.afterClosed().subscribe(function () {
            // Do stuff after the dialog has closed
            _this.getData();
        });
    };
    VoyagelistComponent.prototype.openModifModal = function (item) {
        var _this = this;
        console.log(item);
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modifmodal-component-voyage";
        var g = this.matDialog.open(ModalComponent2, {
            height: '500px',
            width: '500px',
            data: { comp: item },
        });
        g.afterClosed().subscribe(function () {
            // Do stuff after the dialog has closed
            _this.getData();
        });
        console.log(item);
    };
    VoyagelistComponent.prototype.delete = function (id) {
        var _this = this;
        this.voyageServices.deletePost(id);
        this.postsSub = this.voyageServices.getPostUpdateListener()
            .subscribe(function (posts) {
            _this.l = posts;
        });
        setTimeout(function () {
            console.log(_this.l);
            _this.listData = new MatTableDataSource(_this.l);
            _this.listData.sort = _this.sort;
            _this.listData.paginator = _this.paginator;
            _this.listData.filterPredicate = function (data, filter) {
                return _this.displayedColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 5000);
    };
    VoyagelistComponent.prototype.deleteReservationHotel = function (Titre) {
        console.log(Titre);
        this.voyageServices.DeleteReservationHotel(Titre).subscribe();
    };
    VoyagelistComponent.prototype.getData = function () {
        var _this = this;
        this.voyageServices.getVoyageBase();
        this.postsSub = this.voyageServices.getPostUpdateListener()
            .subscribe(function (posts) {
            _this.l = posts;
        });
        setTimeout(function () {
            console.log(_this.l);
            _this.listData = new MatTableDataSource(_this.l);
            _this.listData.sort = _this.sort;
            _this.listData.paginator = _this.paginator;
            _this.listData.filterPredicate = function (data, filter) {
                return _this.displayedColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 4000);
    };
    VoyagelistComponent.prototype.GetListeReservationAccepter = function () {
        var _this = this;
        this.voyageServices.getReservationAccepter().subscribe(function (response) {
            _this.LReservationAccepter = response;
            console.log(response);
        }, function (err) {
            "error";
        });
        setTimeout(function () {
            console.log(_this.LReservationAccepter);
            _this.listeReservationAccepter = new MatTableDataSource(_this.LReservationAccepter);
            _this.listeReservationAccepter.sort = _this.sort;
            _this.listeReservationAccepter.paginator = _this.paginator;
            _this.listeReservationAccepter.filterPredicate = function (data, filter) {
                console.log("-------" + filter);
                return _this.ReservationHotelColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 4000);
    };
    VoyagelistComponent.prototype.GetListeReservationEnAttente = function () {
        var _this = this;
        this.voyageServices.getReservationEnAttent().subscribe(function (response) {
            _this.LReservationEnAttente = response;
            console.log(response);
        }, function (err) {
            "error";
        });
        setTimeout(function () {
            console.log(_this.LReservationEnAttente);
            _this.listeReservationEnAttente = new MatTableDataSource(_this.LReservationEnAttente);
            _this.listeReservationEnAttente.sort = _this.sort;
            _this.listeReservationEnAttente.paginator = _this.paginator;
            _this.listeReservationEnAttente.filterPredicate = function (data, filter) {
                return _this.ReservationHotelColumns2.some(function (ele) {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 40000);
    };
    VoyagelistComponent.prototype.ReservationAccepter = function (Titre) {
        this.voyageServices.ReservationAccepter(Titre).subscribe();
    };
    VoyagelistComponent.prototype.UpgradePayement = function (user) {
        this.voyageServices.UpgradePayement(user).subscribe();
        this.GetListeReservationAccepter();
    };
    tslib_1.__decorate([
        ViewChild(MatSort, null),
        tslib_1.__metadata("design:type", MatSort)
    ], VoyagelistComponent.prototype, "sort", void 0);
    tslib_1.__decorate([
        ViewChild(MatPaginator, null),
        tslib_1.__metadata("design:type", MatPaginator)
    ], VoyagelistComponent.prototype, "paginator", void 0);
    VoyagelistComponent = tslib_1.__decorate([
        Component({
            selector: 'app-voyagelist',
            templateUrl: './voyagelist.component.html',
            styleUrls: ['./voyagelist.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [VoyagesService, MatDialog, Router])
    ], VoyagelistComponent);
    return VoyagelistComponent;
}());
export { VoyagelistComponent };
//# sourceMappingURL=voyagelist.component.js.map