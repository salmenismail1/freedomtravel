import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var HomecontrolComponent = /** @class */ (function () {
    function HomecontrolComponent() {
    }
    /* constructor(private hotelServices: HotelsService,private soireeServices: SoireesService,private voyageServices: VoyagesService,public matDialog: MatDialog,private router:Router) { }
   checkedlist:Array<any>=[];
     listData: MatTableDataSource<any>;
     displayedColumns: string[] = ['id', 'nom','actions'];
   
     voyagecheckedlist:Array<any>=[];
     voyagelistData: MatTableDataSource<any>;
     voyagedisplayedColumns: string[] = ['id', 'nom','actions'];
   
     soireecheckedlist:Array<any>=[];
     soireelistData: MatTableDataSource<any>;
     soireedisplayedColumns: string[] = ['id', 'titre','actions'];
   
   
     @ViewChild(MatSort,null) sort: MatSort;
     @ViewChild(MatPaginator,null) paginator: MatPaginator;
     searchKey: string;
     voyagesearchKey: string;
     soireesearchKey: string;
   
   
     
     l: Hotels[];
     vl: Voyages[];
     sl: Soiree[];
   
   
     private postsSub: Subscription;
     private voyagepostsSub: Subscription;
     private soireepostsSub: Subscription;*/
    HomecontrolComponent.prototype.ngOnInit = function () {
        /*
        this.getData();
        this.getVoyageData();
        this.getSoireeData();
         
      }
    
     
      onSearchClear() {
        this.searchKey = "";
        this.applyFilter();
      }
    
      onVoyageSearchClear() {
        this. voyagesearchKey = "";
        this.applyVoyageFilter();
      }
    
      applyVoyageFilter() {
        this.voyagelistData.filter = this.voyagesearchKey.trim().toLowerCase();
      }
    
      onSoireeSearchClear() {
        this. soireesearchKey = "";
        this.applySoireeFilter();
      }
    
    
    
    
      applySoireeFilter() {
        this.soireelistData.filter = this.soireesearchKey.trim().toLowerCase();
      }
      applyFilter() {
        this.listData.filter = this.searchKey.trim().toLowerCase();
      }
    
      getVoyageData(){
        this.voyageServices.getVoyageBase();
        this.voyagepostsSub = this.voyageServices.getPostUpdateListener()
          .subscribe((posts: Voyages[]) => {
            this.vl = posts;
          });
          setTimeout(() =>
          {
            console.log(this.vl);
            this.voyagelistData = new MatTableDataSource(this.vl);
            this.voyagelistData.sort = this.sort;
            this.voyagelistData.paginator = this.paginator;
            this.voyagelistData.filterPredicate = (data, filter) => {
              return this.voyagedisplayedColumns.some(ele => {
                return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
              });
            };
      
          },
          4000);
      }
    
      getSoireeData(){
        this.soireeServices.getSoireesBase();
        this.soireepostsSub = this.soireeServices.getPostUpdateListener()
          .subscribe((posts: Soiree[]) => {
            this.sl = posts;
          });
          setTimeout(() =>
          {
            console.log(this.sl);
            this.soireelistData = new MatTableDataSource(this.sl);
            this.soireelistData.sort = this.sort;
            this.soireelistData.paginator = this.paginator;
            this.soireelistData.filterPredicate = (data, filter) => {
              return this.soireedisplayedColumns.some(ele => {
                return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
              });
            };
      
          },
          4000);
      }
      
      getData(){
        this.hotelServices.getHotelBase();
        this.postsSub = this.hotelServices.getPostUpdateListener()
          .subscribe((posts: Hotels[]) => {
            this.l = posts;
          });
          setTimeout(() =>
          {
            console.log(this.l);
            this.listData = new MatTableDataSource(this.l);
            this.listData.sort = this.sort;
            this.listData.paginator = this.paginator;
            this.listData.filterPredicate = (data, filter) => {
              return this.displayedColumns.some(ele => {
                return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
              });
            };
      
          },
          4000);
      }
    
      check(item:any){
        console.log(item);
    if(!this.checkedlist.includes(item)){
      this.checkedlist.push(item);
    
    }
    console.log(this.checkedlist);
    }
    voyagecheck(item:any){
      console.log(item);
    if(!this.voyagecheckedlist.includes(item)){
    this.voyagecheckedlist.push(item);
    
    }
    console.log(this.voyagecheckedlist);
    }
    
    voyageconfirmer(){
      this.voyageServices.deleteall();
    for(let item of this.voyagecheckedlist ){
      this.voyageServices.voyagehomelistpost(item);
    }
    this.voyagecheckedlist=[];
    this.getVoyageData();
    }
    
    
    soireecheck(item:any){
      console.log(item);
    if(!this.soireecheckedlist.includes(item)){
    this.soireecheckedlist.push(item);
    
    }
    console.log(this.soireecheckedlist);
    }
    
    soireeconfirmer(){
      this.soireeServices.deleteall();
    for(let item of this.soireecheckedlist ){
      this.soireeServices.soireehomelistpost(item);
    }
    this.soireecheckedlist=[];
    this.getVoyageData();
    }
    
    confirmer(){
      this.hotelServices.deleteall();
    for(let item of this.checkedlist ){
      this.hotelServices.hotelhomelistpost(item);
    }
    this.checkedlist=[];
    this.getData();
    }
     pub(){
      for(let item of this.soireecheckedlist ){
      this.hotelServices.pubpost(item);
      }*/
    };
    HomecontrolComponent = tslib_1.__decorate([
        Component({
            selector: 'app-homecontrol',
            templateUrl: './homecontrol.component.html',
            styleUrls: ['./homecontrol.component.scss']
        })
    ], HomecontrolComponent);
    return HomecontrolComponent;
}());
export { HomecontrolComponent };
//# sourceMappingURL=homecontrol.component.js.map