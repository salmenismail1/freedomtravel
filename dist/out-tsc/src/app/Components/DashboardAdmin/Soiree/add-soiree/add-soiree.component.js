import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SoireesService } from '../../../../Services/soirees.service';
var AddSoireeComponent = /** @class */ (function () {
    function AddSoireeComponent(dialogRef, soireeServices, http) {
        this.dialogRef = dialogRef;
        this.soireeServices = soireeServices;
        this.http = http;
        this.title = 'fileUpload';
        this.multipleImages = [];
        this.imagePreview = '';
        this.imageverif = false;
    }
    AddSoireeComponent.prototype.ngOnInit = function () {
        this.form = new FormGroup({
            id: new FormControl(null, {
                validators: [Validators.required]
            }),
            titre: new FormControl(null, { validators: [Validators.required] }),
            date: new FormControl(null, { validators: [Validators.required] }),
            venteadultesingle: new FormControl(null, { validators: [Validators.required] }),
            venteadultedble: new FormControl(null, { validators: [Validators.required] }),
            vente3rdad: new FormControl(null, { validators: [Validators.required] }),
            venteenfant2ad: new FormControl(null, { validators: [Validators.required] }),
            tarifbebe: new FormControl(null, { validators: [Validators.required] }),
            venteenfant1ad: new FormControl(null, { validators: [Validators.required] }),
            image: new FormControl(null, {
                validators: [Validators.required]
            })
        });
    };
    AddSoireeComponent.prototype.selectImage = function (event) {
        var _this = this;
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.form.patchValue({ image: file });
            this.form.get("image").updateValueAndValidity();
            this.images = file;
            var reader_1 = new FileReader();
            reader_1.onload = function () {
                _this.imagePreview = reader_1.result;
            };
            reader_1.readAsDataURL(file);
            if (file.type == "image/png" || file.type == "image/jpeg" || file.type == "image/jpg" || file.type == "image/svg") {
                this.imageverif = true;
                console.log(this.imageverif);
            }
            else {
                this.imageverif = false;
                console.log(this.imageverif);
            }
        }
    };
    AddSoireeComponent.prototype.selectMultipleImage = function (event) {
        if (event.target.files.length > 0) {
            this.multipleImages = event.target.files;
        }
    };
    AddSoireeComponent.prototype.onSubmit = function () {
        if (this.form.value.id != "" && this.form.value.titre != "" && this.form.value.date != "" && this.form.value.venteadultesingle != "" &&
            this.form.value.venteadultedble != "" && this.form.value.vente3rdad != "" && this.form.value.venteenfant2ad != "" && this.form.value.name != "" && this.imageverif == true
            && this.form.value.tarifbebe != "" && this.form.value.venteenfant1ad != "") {
            this.soireeServices.addPost(this.form.value.id, this.form.value.titre, this.form.value.venteadultesingle, this.form.value.venteadultedble, this.form.value.date, this.images.name, this.form.value.vente3rdad, this.form.value.venteenfant2ad, this.form.value.tarifbebe, this.form.value.venteenfant1ad);
            var formData = new FormData();
            formData.append('file', this.images);
            console.log(formData);
            this.http.post("http://localhost:3000/soiree/file", formData).subscribe(function (res) { return console.log(res); }, function (err) { return console.log(err); });
        }
        else {
            if (this.imageverif == false) {
                alert('image non valid');
            }
            else {
                alert('remplir tous les correctement');
            }
        }
    };
    AddSoireeComponent.prototype.onMultipleSubmit = function () {
        var formData = new FormData();
        for (var _i = 0, _a = this.multipleImages; _i < _a.length; _i++) {
            var img = _a[_i];
            formData.append('files', img);
        }
        this.http.post('http://localhost:3000/multipleFiles', formData).subscribe(function (res) { return console.log(res); }, function (err) { return console.log(err); });
    };
    AddSoireeComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    AddSoireeComponent = tslib_1.__decorate([
        Component({
            selector: 'app-add-soiree',
            templateUrl: './add-soiree.component.html',
            styleUrls: ['./add-soiree.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [MatDialogRef, SoireesService, HttpClient])
    ], AddSoireeComponent);
    return AddSoireeComponent;
}());
export { AddSoireeComponent };
//# sourceMappingURL=add-soiree.component.js.map