import * as tslib_1 from "tslib";
import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SoireesService } from '../../../../Services/soirees.service';
var SoireemodifComponent = /** @class */ (function () {
    function SoireemodifComponent(dialogRef, soireeServices, http, data) {
        this.dialogRef = dialogRef;
        this.soireeServices = soireeServices;
        this.http = http;
        this.data = data;
        this.title = 'fileUpload';
        this.multipleImages = [];
        this.imagePreview = '';
        this.imageverif = false;
    }
    SoireemodifComponent.prototype.ngOnInit = function () {
        this.form = new FormGroup({
            id: new FormControl(null, {
                validators: [Validators.required]
            }),
            titre: new FormControl(null, { validators: [Validators.required] }),
            date: new FormControl(null, { validators: [Validators.required] }),
            venteadultesingle: new FormControl(null, { validators: [Validators.required] }),
            venteadultedble: new FormControl(null, { validators: [Validators.required] }),
            vente3rdad: new FormControl(null, { validators: [Validators.required] }),
            venteenfant2ad: new FormControl(null, { validators: [Validators.required] }),
            tarifbebe: new FormControl(null, { validators: [Validators.required] }),
            venteenfant1ad: new FormControl(null, { validators: [Validators.required] }),
            image: new FormControl(null, {
                validators: [Validators.required]
            })
        });
    };
    SoireemodifComponent.prototype.selectImage = function (event) {
        var _this = this;
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.form.patchValue({ image: file });
            this.form.get("image").updateValueAndValidity();
            this.images = file;
            var reader_1 = new FileReader();
            reader_1.onload = function () {
                _this.imagePreview = reader_1.result;
            };
            reader_1.readAsDataURL(file);
            if (file.type == "image/png" || file.type == "image/jpeg" || file.type == "image/jpg" || file.type == "image/svg") {
                this.imageverif = true;
                console.log(this.imageverif);
            }
            else {
                this.imageverif = false;
                console.log(this.imageverif);
            }
        }
    };
    /*
      selectMultipleImage(event){
        if (event.target.files.length > 0) {
          this.multipleImages = event.target.files;
          this.form.patchValue({ image: multipleImages });
            this.form.get("image").updateValueAndValidity();
            this.images = multipleImages;
         
          for(let item of this.multipleImages){
            const reader = new FileReader();
            reader.onload = () => {
              this.imagePreview = reader.result as string;
            };
            reader.readAsDataURL(item);
          if(item.type=="image/png" || item.type=="image/jpeg" || item.type=="image/jpg" || item.type=="image/svg"){
    
          this.imageverif=true;
          console.log(this.imageverif);
          
          }else{
            
            this.imageverif=false;
            console.log(this.imageverif);
            return;
    
          }
        }
        }
    
      }
      */
    SoireemodifComponent.prototype.onSubmit = function (idg) {
        console.log(idg);
        if (this.form.value.id != "" || this.form.value.titre != "" || this.form.value.date != "" || this.form.value.venteadultesingle != "" ||
            this.form.value.venteadultedble != "" || this.form.value.vente3rdad != "" || this.form.value.venteenfant2ad != "" || this.form.value.name != "" || this.imageverif == true
            || this.form.value.tarifbebe != "" || this.form.value.venteenfant1ad != "") {
            this.soireeServices.update(this.form.value.id, this.form.value.titre, this.form.value.venteadultesingle, this.form.value.venteadultedble, this.form.value.date, this.images.name, this.form.value.vente3rdad, this.form.value.venteenfant2ad, this.form.value.tarifbebe, this.form.value.venteenfant1ad, idg);
            var formData = new FormData();
            formData.append('file', this.images);
            console.log(formData);
            this.http.post("http://localhost:3000/soiree/file", formData).subscribe(function (res) { return console.log(res); }, function (err) { return console.log(err); });
            /*ng serve --live-reload false
            location.reload();
                  window.stop();
      
            */
        }
        else {
            if (this.imageverif == false) {
                alert('image non valid');
            }
            else {
                alert('remplir tous les correctement');
            }
        }
    };
    SoireemodifComponent.prototype.onMultipleSubmit = function () {
        var formData = new FormData();
        for (var _i = 0, _a = this.multipleImages; _i < _a.length; _i++) {
            var img = _a[_i];
            formData.append('files', img);
        }
        this.http.post('http://localhost:3000/multipleFiles', formData).subscribe(function (res) { return console.log(res); }, function (err) { return console.log(err); });
    };
    SoireemodifComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    SoireemodifComponent = tslib_1.__decorate([
        Component({
            selector: 'app-soireemodif',
            templateUrl: './soireemodif.component.html',
            styleUrls: ['./soireemodif.component.scss']
        }),
        tslib_1.__param(3, Inject(MAT_DIALOG_DATA)),
        tslib_1.__metadata("design:paramtypes", [MatDialogRef, SoireesService, HttpClient, Object])
    ], SoireemodifComponent);
    return SoireemodifComponent;
}());
export { SoireemodifComponent };
//# sourceMappingURL=soireemodif.component.js.map