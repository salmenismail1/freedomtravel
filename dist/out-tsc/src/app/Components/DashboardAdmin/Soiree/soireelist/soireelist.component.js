import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { AddSoireeComponent as ModalComponent } from '../add-soiree/add-soiree.component';
import { SoireemodifComponent as ModalComponent2 } from '../soireemodif/soireemodif.component';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { SoireesService } from '../../../../Services/soirees.service';
var SoireelistComponent = /** @class */ (function () {
    function SoireelistComponent(soireeServices, matDialog, router) {
        this.soireeServices = soireeServices;
        this.matDialog = matDialog;
        this.router = router;
        this.displayedColumns = ['id', 'nom', 'actions'];
    }
    SoireelistComponent.prototype.ngOnInit = function () {
        this.getData();
    };
    SoireelistComponent.prototype.onSearchClear = function () {
        this.searchKey = "";
        this.applyFilter();
    };
    SoireelistComponent.prototype.applyFilter = function () {
        this.listData.filter = this.searchKey.trim().toLowerCase();
    };
    SoireelistComponent.prototype.openModal = function () {
        var _this = this;
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modal-component-voyage";
        var g = this.matDialog.open(ModalComponent, {
            height: '500px',
            width: '500px',
        });
        g.afterClosed().subscribe(function () {
            // Do stuff after the dialog has closed
            _this.getData();
        });
    };
    SoireelistComponent.prototype.openModifModal = function (item) {
        var _this = this;
        console.log(item);
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modifmodal-component-voyage";
        var g = this.matDialog.open(ModalComponent2, {
            height: '500px',
            width: '500px',
            data: { comp: item },
        });
        g.afterClosed().subscribe(function () {
            // Do stuff after the dialog has closed
            _this.getData();
        });
        console.log(item);
    };
    SoireelistComponent.prototype.delete = function (id) {
        var _this = this;
        this.soireeServices.deletePost(id);
        this.postsSub = this.soireeServices.getPostUpdateListener()
            .subscribe(function (posts) {
            _this.l = posts;
        });
        setTimeout(function () {
            console.log(_this.l);
            _this.listData = new MatTableDataSource(_this.l);
            _this.listData.sort = _this.sort;
            _this.listData.paginator = _this.paginator;
            _this.listData.filterPredicate = function (data, filter) {
                return _this.displayedColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 5000);
    };
    SoireelistComponent.prototype.getData = function () {
        var _this = this;
        this.soireeServices.getSoireesBase();
        this.postsSub = this.soireeServices.getPostUpdateListener()
            .subscribe(function (posts) {
            _this.l = posts;
        });
        setTimeout(function () {
            console.log(_this.l);
            _this.listData = new MatTableDataSource(_this.l);
            _this.listData.sort = _this.sort;
            _this.listData.paginator = _this.paginator;
            _this.listData.filterPredicate = function (data, filter) {
                return _this.displayedColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 4000);
    };
    tslib_1.__decorate([
        ViewChild(MatSort, null),
        tslib_1.__metadata("design:type", MatSort)
    ], SoireelistComponent.prototype, "sort", void 0);
    tslib_1.__decorate([
        ViewChild(MatPaginator, null),
        tslib_1.__metadata("design:type", MatPaginator)
    ], SoireelistComponent.prototype, "paginator", void 0);
    SoireelistComponent = tslib_1.__decorate([
        Component({
            selector: 'app-soireelist',
            templateUrl: './soireelist.component.html',
            styleUrls: ['./soireelist.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [SoireesService, MatDialog, Router])
    ], SoireelistComponent);
    return SoireelistComponent;
}());
export { SoireelistComponent };
//# sourceMappingURL=soireelist.component.js.map