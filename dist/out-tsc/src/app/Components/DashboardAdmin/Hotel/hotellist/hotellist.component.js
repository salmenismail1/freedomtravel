import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { AddHotelComponent as ModalComponent } from '../add-hotel/add-hotel.component';
import { HotelmodifComponent as ModalComponent2 } from '../hotelmodif/hotelmodif.component';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { HotelsService } from '../../../../Services/hotels.service';
import { Router } from '@angular/router';
var HotellistComponent = /** @class */ (function () {
    function HotellistComponent(hotelServices, matDialog, router) {
        this.hotelServices = hotelServices;
        this.matDialog = matDialog;
        this.router = router;
        this.displayedColumns = ['id', 'nom', 'actions'];
        this.ReservationHotelColumns = ['Titre', 'Email', 'Prix', 'actions'];
        this.ReservationHotelColumns2 = ['Titre', 'Email', 'Prix', 'actions'];
    }
    HotellistComponent.prototype.ngOnInit = function () {
        //this.getData();
        this.GetListeReservationAccepter();
        this.GetListeReservationEnAttente();
    };
    HotellistComponent.prototype.onSearchClear = function () {
        this.searchKey = "";
        this.applyFilter();
    };
    HotellistComponent.prototype.onSearchClear2 = function () {
        this.searchKey2 = "";
        this.applyFilter2();
    };
    HotellistComponent.prototype.onSearchClear3 = function () {
        this.searchKey3 = "";
        this.applyFilter3();
    };
    HotellistComponent.prototype.applyFilter = function () {
        this.listData.filter = this.searchKey.trim().toLowerCase();
    };
    HotellistComponent.prototype.applyFilter2 = function () {
        this.listeReservationAccepter.filter = this.searchKey2.trim().toLowerCase();
    };
    HotellistComponent.prototype.applyFilter3 = function () {
        this.listeReservationEnAttente.filter = this.searchKey3.trim().toLowerCase();
    };
    HotellistComponent.prototype.openModal = function () {
        var _this = this;
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modal-component";
        var g = this.matDialog.open(ModalComponent, {
            height: '500px',
            width: '500px',
        });
        g.afterClosed().subscribe(function () {
            // Do stuff after the dialog has closed
            _this.getData();
        });
    };
    HotellistComponent.prototype.openModifModal = function (item, event) {
        var _this = this;
        console.log(item);
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modifmodal-component";
        var g = this.matDialog.open(ModalComponent2, {
            height: '500px',
            width: '500px',
            data: { comp: item },
        });
        event.pre;
        g.afterClosed().subscribe(function () {
            // Do stuff after the dialog has closed
            _this.getData();
        });
        console.log(item);
    };
    HotellistComponent.prototype.delete = function (id) {
        var _this = this;
        this.hotelServices.deletePost(id);
        this.postsSub = this.hotelServices.getPostUpdateListener()
            .subscribe(function (posts) {
            _this.l = posts;
        });
        setTimeout(function () {
            console.log(_this.l);
            _this.listData = new MatTableDataSource(_this.l);
            _this.listData.sort = _this.sort;
            _this.listData.paginator = _this.paginator;
        }, 5000);
    };
    HotellistComponent.prototype.deleteReservationHotel = function (Titre) {
        console.log(Titre);
        this.hotelServices.DeleteReservationHotel(Titre).subscribe();
    };
    HotellistComponent.prototype.getData = function () {
        var _this = this;
        this.hotelServices.getHotelBase();
        this.postsSub = this.hotelServices.getPostUpdateListener()
            .subscribe(function (posts) {
            _this.l = posts;
        });
        setTimeout(function () {
            console.log(_this.l);
            _this.listData = new MatTableDataSource(_this.l);
            _this.listData.sort = _this.sort;
            _this.listData.paginator = _this.paginator;
            _this.listData.filterPredicate = function (data, filter) {
                return _this.displayedColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 4000);
    };
    HotellistComponent.prototype.GetListeReservationAccepter = function () {
        var _this = this;
        this.hotelServices.getReservationAccepter().subscribe(function (response) {
            _this.LReservationAccepter = response;
            console.log(response);
        }, function (err) {
            "error";
        });
        setTimeout(function () {
            console.log(_this.LReservationAccepter);
            _this.listeReservationAccepter = new MatTableDataSource(_this.LReservationAccepter);
            _this.listeReservationAccepter.sort = _this.sort;
            _this.listeReservationAccepter.paginator = _this.paginator;
            _this.listeReservationAccepter.filterPredicate = function (data, filter) {
                console.log("-------" + filter);
                return _this.ReservationHotelColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 4000);
    };
    HotellistComponent.prototype.GetListeReservationEnAttente = function () {
        var _this = this;
        this.hotelServices.getReservationEnAttent().subscribe(function (response) {
            _this.LReservationEnAttente = response;
            console.log(response);
        }, function (err) {
            "error";
        });
        setTimeout(function () {
            console.log(_this.LReservationEnAttente);
            _this.listeReservationEnAttente = new MatTableDataSource(_this.LReservationEnAttente);
            _this.listeReservationEnAttente.sort = _this.sort;
            _this.listeReservationEnAttente.paginator = _this.paginator;
            _this.listeReservationEnAttente.filterPredicate = function (data, filter) {
                return _this.ReservationHotelColumns2.some(function (ele) {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
            };
        }, 4000);
    };
    HotellistComponent.prototype.ReservationAccepter = function (Titre) {
        this.hotelServices.ReservationAccepter(Titre).subscribe();
    };
    tslib_1.__decorate([
        ViewChild(MatSort, null),
        tslib_1.__metadata("design:type", MatSort)
    ], HotellistComponent.prototype, "sort", void 0);
    tslib_1.__decorate([
        ViewChild(MatPaginator, null),
        tslib_1.__metadata("design:type", MatPaginator)
    ], HotellistComponent.prototype, "paginator", void 0);
    HotellistComponent = tslib_1.__decorate([
        Component({
            selector: 'app-hotellist',
            templateUrl: './hotellist.component.html',
            styleUrls: ['./hotellist.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [HotelsService, MatDialog, Router])
    ], HotellistComponent);
    return HotellistComponent;
}());
export { HotellistComponent };
//# sourceMappingURL=hotellist.component.js.map