import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HotelsService } from '../../../../Services/hotels.service';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
var AddHotelComponent = /** @class */ (function () {
    function AddHotelComponent(_snackBar, dialogRef, hotelServices, http) {
        this._snackBar = _snackBar;
        this.dialogRef = dialogRef;
        this.hotelServices = hotelServices;
        this.http = http;
        this.title = 'fileUpload';
        this.multipleImages = [];
        this.imagePreview = '';
        this.imageverif = false;
    }
    AddHotelComponent.prototype.ngOnInit = function () {
        this.form = new FormGroup({
            id: new FormControl(null, {
                validators: [Validators.required]
            }),
            nom: new FormControl(null, { validators: [Validators.required] }),
            content: new FormControl(null, { validators: [Validators.required] }),
            adultOnly: new FormControl(null, { validators: [Validators.required] }),
            ville: new FormControl(null, { validators: [Validators.required] }),
            categorie: new FormControl(null, { validators: [Validators.required] }),
            type: new FormControl(null, { validators: [Validators.required] }),
            lpdvente: new FormControl(null, { validators: [Validators.required] }),
            dpvente: new FormControl(null, { validators: [Validators.required] }),
            pcvente: new FormControl(null, { validators: [Validators.required] }),
            allinsoftvente: new FormControl(null, { validators: [Validators.required] }),
            allinvente: new FormControl(null, { validators: [Validators.required] }),
            ultraallinvente: new FormControl(null, { validators: [Validators.required] }),
            age_enf_gratuit: new FormControl(null, { validators: [Validators.required] }),
            image: new FormControl(null, {
                validators: [Validators.required]
            })
        });
    };
    AddHotelComponent.prototype.selectImage = function (event) {
        var _this = this;
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.form.patchValue({ image: file });
            this.form.get("image").updateValueAndValidity();
            this.images = file;
            var reader_1 = new FileReader();
            reader_1.onload = function () {
                _this.imagePreview = reader_1.result;
            };
            reader_1.readAsDataURL(file);
            if (file.type == "image/png" || file.type == "image/jpeg" || file.type == "image/jpg" || file.type == "image/svg") {
                this.imageverif = true;
                console.log(this.imageverif);
            }
            else {
                this.imageverif = false;
                console.log(this.imageverif);
            }
        }
        this._snackBar.open('Cannonball!!', '', {
            duration: 5000,
            horizontalPosition: 'end',
            verticalPosition: 'top',
        });
    };
    AddHotelComponent.prototype.selectMultipleImage = function (event) {
        if (event.target.files.length > 0) {
            this.multipleImages = event.target.files;
        }
    };
    AddHotelComponent.prototype.onSubmit = function () {
        if (this.form.value.id != "" && this.form.value.nom != "" && this.form.value.adultOnly != "" && this.form.value.ville != "" &&
            this.form.value.categorie != "" && this.form.value.type != "" && this.form.value.lpdvente != "" && this.form.value.dpvente != "" &&
            this.form.value.pcvente != "" && this.form.value.allinsoftvente != "" && this.form.value.allinvente != "" && this.form.value.ultraallinvente != "" &&
            this.form.value.age_enf_gratuit != "" && this.form.value.name != "" && this.imageverif == true) {
            this.hotelServices.addPost(this.form.value.id, this.form.value.nom, this.form.value.adultOnly, this.form.value.ville, this.form.value.categorie, this.form.value.type, this.form.value.lpdvente, this.form.value.dpvente, this.form.value.pcvente, this.form.value.allinsoftvente, this.form.value.allinvente, this.form.value.ultraallinvente, this.form.value.age_enf_gratuit, this.images.name);
            var formData = new FormData();
            formData.append('file', this.images);
            console.log(formData);
            this.http.post("http://localhost:3000/hotel/file", formData).subscribe(function (res) { return console.log(res); }, function (err) { return console.log(err); });
        }
        else {
            if (this.imageverif == false) {
                alert('image non valid');
            }
            else {
                alert('remplir tous les correctement');
            }
        }
    };
    AddHotelComponent.prototype.onMultipleSubmit = function () {
        var formData = new FormData();
        for (var _i = 0, _a = this.multipleImages; _i < _a.length; _i++) {
            var img = _a[_i];
            formData.append('files', img);
        }
        this.http.post('http://localhost:3000/multipleFiles', formData).subscribe(function (res) { return console.log(res); }, function (err) { return console.log(err); });
    };
    AddHotelComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    AddHotelComponent = tslib_1.__decorate([
        Component({
            selector: 'app-add-hotel',
            templateUrl: './add-hotel.component.html',
            styleUrls: ['./add-hotel.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [MatSnackBar, MatDialogRef, HotelsService, HttpClient])
    ], AddHotelComponent);
    return AddHotelComponent;
}());
export { AddHotelComponent };
//# sourceMappingURL=add-hotel.component.js.map