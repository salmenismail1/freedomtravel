import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var HotelmodifComponent = /** @class */ (function () {
    function HotelmodifComponent() {
    }
    /*title = 'fileUpload';
    images;
    multipleImages = [];
    imagePreview: string='';
    form: FormGroup;
    imageverif=false;
    constructor(public dialogRef: MatDialogRef<HotelmodifComponent>,private hotelServices:HotelsService,private http: HttpClient,@Inject(MAT_DIALOG_DATA) public data:any){}*/
    HotelmodifComponent.prototype.ngOnInit = function () {
        /*this.form = new FormGroup({
          id: new FormControl(null, {
            validators: [Validators.required]
          }),
          nom: new FormControl(null, { validators: [Validators.required] }),
          content: new FormControl(null, { validators: [Validators.required] }),
          adultOnly: new FormControl(null, { validators: [Validators.required] }),
          ville: new FormControl(null, { validators: [Validators.required] }),
          categorie: new FormControl(null, { validators: [Validators.required] }),
          type: new FormControl(null, { validators: [Validators.required] }),
          lpdvente: new FormControl(null, { validators: [Validators.required] }),
          dpvente: new FormControl(null, { validators: [Validators.required] }),
          pcvente: new FormControl(null, { validators: [Validators.required] }),
          allinsoftvente: new FormControl(null, { validators: [Validators.required] }),
          allinvente: new FormControl(null, { validators: [Validators.required] }),
          ultraallinvente: new FormControl(null, { validators: [Validators.required] }),
          age_enf_gratuit: new FormControl(null, { validators: [Validators.required] }),
    
          image: new FormControl(null, {
            validators: [Validators.required]
          })
        });
      }
     
      selectImage(event) {
    
        if (event.target.files.length > 0) {
          const file = event.target.files[0];
            this.form.patchValue({ image: file });
            this.form.get("image").updateValueAndValidity();
            this.images = file;
            const reader = new FileReader();
          reader.onload = () => {
            this.imagePreview = reader.result as string;
          };
          reader.readAsDataURL(file);
          if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){
    
          this.imageverif=true;
          console.log(this.imageverif);
          
          }else{
            
            this.imageverif=false;
            console.log(this.imageverif);
    
          }
    
    
        }
    
      }
      
    
    
      selectMultipleImage(event){
        if (event.target.files.length > 0) {
          this.multipleImages = event.target.files;
        }
    
      }
    
      onSubmit(idg:string){
        console.log(idg);
        if(this.form.value.id!="" || this.form.value.nom!="" || this.form.value.adultOnly!="" || this.form.value.ville!="" ||
        this.form.value.categorie!="" || this.form.value.type!="" || this.form.value.lpdvente!="" || this.form.value.dpvente!="" ||
        this.form.value.pcvente!="" || this.form.value.allinsoftvente!="" || this.form.value.allinvente!="" || this.form.value.ultraallinvente!="" ||
        this.form.value.age_enf_gratuit!="" || this.form.value.name!="" || this.imageverif==true
        ){
    
          this.hotelServices.update(this.form.value.id,this.form.value.nom,this.form.value.adultOnly,
            this.form.value.ville,this.form.value.categorie,this.form.value.type , this.form.value.lpdvente,
             this.form.value.dpvente, this.form.value.pcvente,this.form.value.allinsoftvente,this.form.value.allinvente,
             this.form.value.ultraallinvente,this.form.value.age_enf_gratuit,this.images.name,idg);
          const formData = new FormData();
          formData.append('file', this.images);
          console.log(formData);
          this.http.post<any>("http://localhost:3000/hotel/file", formData).subscribe(
            (res) => console.log(res),
            (err) => console.log(err)
          );
          /*ng serve --live-reload false
          location.reload();
                window.stop();
    
        
         
    
    
        }else{
    if(this.imageverif==false){
      alert('image non valid');
    
    }else{
      alert('remplir tous les correctement');
    
    }
        }
       
      }
    
      onMultipleSubmit(){
        const formData = new FormData();
        for(let img of this.multipleImages){
          formData.append('files', img);
        }
    
        this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
          (res) => console.log(res),
          (err) => console.log(err)
        );
      }
      close(){
            this.dialogRef.close();
    
    */
    };
    HotelmodifComponent = tslib_1.__decorate([
        Component({
            selector: 'app-hotelmodif',
            templateUrl: './hotelmodif.component.html',
            styleUrls: ['./hotelmodif.component.scss']
        })
    ], HotelmodifComponent);
    return HotelmodifComponent;
}());
export { HotelmodifComponent };
//# sourceMappingURL=hotelmodif.component.js.map