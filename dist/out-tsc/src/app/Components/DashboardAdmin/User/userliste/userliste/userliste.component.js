import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { LoginService } from '../../../../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
import { AppVarsGlobal } from '../../../../../app.vars.global';
var UserlisteComponent = /** @class */ (function () {
    function UserlisteComponent(userServices, matDialog, router, vars) {
        this.userServices = userServices;
        this.matDialog = matDialog;
        this.router = router;
        this.vars = vars;
        this.AdminColumns = ['nom', 'cin', 'tel', 'type', 'actions'];
        this.ClientColumns = ['nom', 'cin', 'tel', 'type', 'actions'];
        this.HistoriqueColumns = ['Titre', 'Etat', 'Nom', 'actions'];
    }
    UserlisteComponent.prototype.ngOnInit = function () {
        this.userType = this.vars.type;
        console.log(this.userType);
        this.GetListeAdmin();
        this.GetListeClient();
        this.GetListePayement();
    };
    UserlisteComponent.prototype.onSearchClear = function () {
        this.searchKey = "";
        this.applyFilter();
    };
    UserlisteComponent.prototype.onSearchClear2 = function () {
        this.searchKey2 = "";
        this.applyFilter2();
    };
    UserlisteComponent.prototype.onSearchClear3 = function () {
        this.searchKey3 = "";
        this.applyFilter3();
    };
    UserlisteComponent.prototype.applyFilter = function () {
        this.listeAdmin.filter = this.searchKey.trim().toLowerCase();
    };
    UserlisteComponent.prototype.applyFilter2 = function () {
        this.listeclient.filter = this.searchKey2.trim().toLowerCase();
    };
    UserlisteComponent.prototype.applyFilter3 = function () {
        this.listePayement.filter = this.searchKey3.trim().toLowerCase();
    };
    UserlisteComponent.prototype.DeleteUser = function (id) {
        console.log(id);
        this.userServices.DeleteUser(id).subscribe();
    };
    UserlisteComponent.prototype.GetListeClient = function () {
        var _this = this;
        this.userServices.getClient().subscribe(function (response) {
            _this.LClient = response;
            _this.listeclient = new MatTableDataSource(_this.LClient);
            _this.listeclient.sort = _this.sort;
            _this.listeclient.paginator = _this.paginator;
            _this.listeclient.filterPredicate = function (data, filter) {
                return _this.ClientColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
            };
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    UserlisteComponent.prototype.GetListePayement = function () {
        var _this = this;
        var decoded = jwt_decode(this.userServices.getToken());
        this.userServices.getHistoriquee().subscribe(function (response) {
            _this.LPayement = response;
            _this.listePayement = new MatTableDataSource(_this.LPayement);
            _this.listePayement.sort = _this.sort;
            _this.listePayement.paginator = _this.paginator;
            _this.listePayement.filterPredicate = function (data, filter) {
                return _this.HistoriqueColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
            };
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    UserlisteComponent.prototype.GetListeAdmin = function () {
        var _this = this;
        this.userServices.getAdmin().subscribe(function (response) {
            _this.LAdmin = response;
            _this.listeAdmin = new MatTableDataSource(_this.LAdmin);
            _this.listeAdmin.sort = _this.sort;
            _this.listeAdmin.paginator = _this.paginator;
            _this.listeAdmin.filterPredicate = function (data, filter) {
                return _this.AdminColumns.some(function (ele) {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
            };
            console.log(response);
        }, function (err) {
            "error";
        });
    };
    UserlisteComponent.prototype.UpgradeToAdmin = function (user) {
        this.userServices.UpgradeToAdmin(user).subscribe();
        this.GetListeAdmin();
        this.GetListeClient();
    };
    UserlisteComponent.prototype.DowngradeToClient = function (user) {
        this.userServices.DowngradeToClient(user).subscribe();
        this.GetListeAdmin();
        this.GetListeClient();
    };
    tslib_1.__decorate([
        ViewChild(MatSort, null),
        tslib_1.__metadata("design:type", MatSort)
    ], UserlisteComponent.prototype, "sort", void 0);
    tslib_1.__decorate([
        ViewChild(MatPaginator, null),
        tslib_1.__metadata("design:type", MatPaginator)
    ], UserlisteComponent.prototype, "paginator", void 0);
    UserlisteComponent = tslib_1.__decorate([
        Component({
            selector: 'app-userliste',
            templateUrl: './userliste.component.html',
            styleUrls: ['./userliste.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LoginService, MatDialog, Router, AppVarsGlobal])
    ], UserlisteComponent);
    return UserlisteComponent;
}());
export { UserlisteComponent };
//# sourceMappingURL=userliste.component.js.map