import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { LoginComponent as ModalComponent } from '../login/login.component';
import { LoginService } from '../../Services/login.service';
import { Login } from '../../Models/login';
import { AppVarsGlobal } from '../../app.vars.global';
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(matDialog, loginserv, vars) {
        this.matDialog = matDialog;
        this.loginserv = loginserv;
        this.vars = vars;
        this.user = new Login;
        this.taille = 100;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        this.vars.type = this.vars.login();
    };
    NavbarComponent.prototype.openModal = function () {
        var dialogConfig = new MatDialogConfig();
        // The user can't close the dialog by clicking outside its body
        dialogConfig.disableClose = false;
        dialogConfig.id = "modal-component";
        this.matDialog.open(ModalComponent, {
            height: '600px',
            width: '500px',
        });
    };
    NavbarComponent.prototype.GetStatut = function () {
        if (this.loggued()) {
            return this.vars.type;
        }
        return "";
    };
    NavbarComponent.prototype.loggued = function () {
        return !this.loginserv.loggedIn();
    };
    NavbarComponent.prototype.logOut = function () {
        this.loginserv.logOut();
        this.vars.type = '';
    };
    NavbarComponent = tslib_1.__decorate([
        Component({
            selector: 'app-navbar',
            templateUrl: './navbar.component.html',
            styleUrls: ['./navbar.component.css'],
        }),
        tslib_1.__metadata("design:paramtypes", [MatDialog, LoginService, AppVarsGlobal])
    ], NavbarComponent);
    return NavbarComponent;
}());
export { NavbarComponent };
//# sourceMappingURL=navbar.component.js.map