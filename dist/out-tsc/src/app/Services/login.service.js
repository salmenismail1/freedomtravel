import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
var logintUrl = "http://localhost:3000/user/connection";
var inscritUrl = "http://localhost:3000/user/in";
var UpgradeToAdminUrl = "http://localhost:3000/user/api/UpgradeToAdmin/";
var ClientUrl = "http://localhost:3000/user/api/client";
var AdminUrl = "http://localhost:3000/user/api/admin";
var DowngradeToClientUrl = "http://localhost:3000/user/api/DowngradeToClient/";
var deleteUserUrl = "http://localhost:3000/user/api/DeleteUser/";
var OneUserUrl = "http://localhost:3000/user/api/statut/";
var ModifUserUrl = "http://localhost:3000/user/api/ModifUser/";
var ReservationVoyageClientEncourUrl = "http://localhost:3000/user/api/ReservationVoyageClientEncour/";
var ReservationHotelClientEncourUrl = "http://localhost:3000/user/api/ReservationHotelClientEncour/";
var HistoriqueClientUrl = "http://localhost:3000/user/api/HistoriqueClient/";
var HistoriqueUrl = "http://localhost:3000/user/api/Historique";
var reservationUrl = "http://localhost:3000/user/api/reservation/";
var removeReservationUrl = "http://localhost:3000/user/api/DeleteReservation/";
var LoginService = /** @class */ (function () {
    function LoginService(httpClient, router) {
        this.httpClient = httpClient;
        this.router = router;
    }
    LoginService.prototype.postLogin = function (login) {
        return this.httpClient.post(logintUrl, login);
    };
    LoginService.prototype.Inscrit = function (user) {
        return this.httpClient.post(inscritUrl, user);
    };
    LoginService.prototype.logOut = function () {
        localStorage.removeItem('token');
    };
    LoginService.prototype.getToken = function () {
        return localStorage.getItem('token');
    };
    LoginService.prototype.loggedIn = function () {
        return !!!this.getToken();
    };
    LoginService.prototype.UpgradeToAdmin = function (user) {
        return this.httpClient.put(UpgradeToAdminUrl + user, user);
    };
    LoginService.prototype.DowngradeToClient = function (user) {
        return this.httpClient.put(DowngradeToClientUrl + user, user);
    };
    LoginService.prototype.DeleteUser = function (id) {
        return this.httpClient.delete(deleteUserUrl + id);
    };
    LoginService.prototype.ModifUser = function (id, user) {
        return this.httpClient.put(ModifUserUrl + id, user);
    };
    LoginService.prototype.getAdmin = function () {
        return this.httpClient.get(AdminUrl);
    };
    LoginService.prototype.getClient = function () {
        return this.httpClient.get(ClientUrl);
    };
    LoginService.prototype.getOnUser = function (id) {
        return this.httpClient.get(OneUserUrl + id);
    };
    LoginService.prototype.getReservationVoyageClientEncour = function (id) {
        return this.httpClient.get(ReservationVoyageClientEncourUrl + id);
    };
    LoginService.prototype.getReservationHotelClientEncour = function (id) {
        return this.httpClient.get(ReservationHotelClientEncourUrl + id);
    };
    LoginService.prototype.getHistorique = function (id) {
        return this.httpClient.get(HistoriqueClientUrl + id);
    };
    LoginService.prototype.getHistoriquee = function () {
        return this.httpClient.get(HistoriqueUrl);
    };
    LoginService.prototype.getReservation = function (id) {
        return this.httpClient.get(reservationUrl + id);
    };
    LoginService.prototype.RemoveReservation = function (id) {
        return this.httpClient.delete(removeReservationUrl + id);
    };
    LoginService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient, Router])
    ], LoginService);
    return LoginService;
}());
export { LoginService };
//# sourceMappingURL=login.service.js.map