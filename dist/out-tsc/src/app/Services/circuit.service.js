import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
var url = "https://topresa.ovh/api/departcircuits.json?date[before]=2020-06-18";
var CircuitService = /** @class */ (function () {
    function CircuitService(httpClient) {
        this.httpClient = httpClient;
        this.posts = [];
        this.postsUpdated = new Subject();
    }
    CircuitService.prototype.getPostUpdateListener = function () {
        return this.postsUpdated.asObservable();
    };
    CircuitService.prototype.getCircuit = function () {
        return this.httpClient.get(url, httpOptions);
    };
    //**************njeimi */
    CircuitService.prototype.getCircuitBase = function () {
        var _this = this;
        this.httpClient
            .get("http://localhost:3000/circuit/api/circuit")
            .pipe(map(function (postData) {
            return postData.posts.map(function (post) {
                return {
                    _id: post._id, id: post.id, nom: post.nom, image: post.image, datedepart: post.datedepart,
                    dateretour: post.dateretour, description: post.programmme.description, libelle: post.programmme.libelle, programme: post.programmme.programme, venteadultesingle: post.venteadultesingle, venteadultedble: post.venteadultedble,
                    vente3rdad: post.vente3rdad, venteenfant2ad: post.venteenfant2ad, tarifbebe: post.tarifbebe, venteenfant1ad: post.venteenfant1ad
                };
            });
        }))
            .subscribe(function (transformedPosts) {
            _this.posts = transformedPosts;
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    CircuitService.prototype.addPost = function (item, images) {
        var _this = this;
        var post = { id: item.id, nom: item.nom, image: images, datedepart: item.datedepart,
            dateretour: item.dateretour, programmme: { description: item.description, libelle: item.libelle, programme: item.programme }, venteadultesingle: item.venteadultesingle, venteadultedble: item.venteadultedble,
            vente3rdad: item.vente3rdad, venteenfant2ad: item.venteenfant2ad, tarifbebe: item.tarifbebe, venteenfant1ad: item.venteenfant1ad
        };
        this.httpClient
            .post("http://localhost:3000/circuit/api/circuit", post)
            .subscribe(function (responseData) {
            var id = responseData.postId;
            post.id = id;
            _this.posts.push(post);
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    CircuitService.prototype.update = function (item, idg) {
        var _this = this;
        var post = { id: item.id, nom: item.nom, image: item.images, datedepart: item.datedepart,
            dateretour: item.dateretour, programmme: item.programmme, venteadultesingle: item.venteadultesingle, venteadultedble: item.venteadultedble,
            vente3rdad: item.vente3rdad, venteenfant2ad: item.venteenfant2ad, tarifbebe: item.tarifbebe, venteenfant1ad: item.venteenfant1ad
        };
        this.httpClient
            .put("http://localhost:3000/circuit/api/circuitModif/" + idg, post)
            .subscribe(function (responseData) {
            _this.posts.push(post);
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    CircuitService.prototype.deletePost = function (postId) {
        var _this = this;
        this.httpClient.delete("http://localhost:3000/circuit/api/posts/" + postId)
            .subscribe(function () {
            var updatedPosts = _this.posts.filter(function (post) { return post.id !== postId; });
            _this.posts = updatedPosts;
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    CircuitService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], CircuitService);
    return CircuitService;
}());
export { CircuitService };
//# sourceMappingURL=circuit.service.js.map