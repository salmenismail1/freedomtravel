import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
var HotelUrl = "https://freedomtravel.tn/json/hotels2.php";
var AjoutReservationUrl = "http://localhost:3000/hotel/api/reservation";
var villeUrl = "https://www.freedomtravel.tn/ng/villes.php";
var Hotel2Url = "http://localhost:3000/hotel/api/ListerHotel";
var ReservationHotelUrl = "http://localhost:3000/hotel/api/ReservationHotel";
var deleteReservationHotelUrl = "http://localhost:3000/hotel/api/DeleteReservation/";
var ReservationAccepter = "http://localhost:3000/hotel/api/ReservationAccepter/";
var ReservationAccepterUrl = "http://localhost:3000/hotel/api/ReservationHotelAccepter";
var ReservationEnAttenteUrl = "http://localhost:3000/hotel/api/ReservationHotelEnAttente";
var DeleteHotelUrl = "http://localhost:3000/hotel/api/Hotel/";
var HotelsService = /** @class */ (function () {
    //********njeimi */
    function HotelsService(httpClient) {
        this.httpClient = httpClient;
        //*****njeimi***********
        this.posts = [];
        this.postsUpdated = new Subject();
        this.hotelhomelist = [];
        this.hotelhomelistUpdated = new Subject();
    }
    HotelsService.prototype.getHotel = function () {
        return this.httpClient.get(HotelUrl, httpOptions);
    };
    HotelsService.prototype.getHotel2 = function () {
        return this.httpClient.get(Hotel2Url, httpOptions);
    };
    HotelsService.prototype.getVille = function () {
        return this.httpClient.get(villeUrl);
    };
    HotelsService.prototype.getPrix = function (hotel) {
        if (hotel.lpdvente != 0)
            return hotel.lpdvente;
        else if (hotel.dpvente)
            return hotel.dpvente;
        else if (hotel.allinvente)
            return hotel.allinvente;
        else
            (hotel.allinsoftvente);
        return hotel.allinsoftvente;
    };
    HotelsService.prototype.getReservationHotel = function () {
        return this.httpClient.get(ReservationHotelUrl, httpOptions);
    };
    HotelsService.prototype.DeleteReservationHotel = function (Titre) {
        return this.httpClient.delete(deleteReservationHotelUrl + Titre, httpOptions);
    };
    HotelsService.prototype.ReservationAccepter = function (Titre) {
        return this.httpClient.put(ReservationAccepter + Titre, httpOptions);
    };
    HotelsService.prototype.getReservationAccepter = function () {
        return this.httpClient.get(ReservationAccepterUrl, httpOptions);
    };
    HotelsService.prototype.getReservationEnAttent = function () {
        return this.httpClient.get(ReservationEnAttenteUrl, httpOptions);
    };
    //*******************Njeimi**************************
    HotelsService.prototype.getPostUpdateListener = function () {
        return this.postsUpdated.asObservable();
    };
    HotelsService.prototype.gethomehotelUpdateListener = function () {
        return this.hotelhomelistUpdated.asObservable();
    };
    HotelsService.prototype.postContact = function (ReservationHotel) {
        return this.httpClient.post(AjoutReservationUrl, ReservationHotel);
    };
    HotelsService.prototype.getHotelBase = function () {
        var _this = this;
        this.httpClient
            .get("http://localhost:3000/hotel/api/hotel")
            .pipe(map(function (postData) {
            return postData.posts.map(function (post) {
                return {
                    _id: post._id,
                    id: post.id, nom: post.nom, adultOnly: post.adultOnly, ville: post.ville, categorie: post.categorie, type: post.type,
                    lpdvente: post.lpdvente, dpvente: post.dpvente, pcvente: post.pcvente, allinsoftvente: post.allinsoftvente, allinvente: post.allinvente,
                    ultraallinvente: post.ultraallinvente, age_enf_gratuit: post.age_enf_gratuit, image: post.image
                };
            });
        }))
            .subscribe(function (transformedPosts) {
            _this.posts = transformedPosts;
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    HotelsService.prototype.addPost = function (id, nom, adultOnly, ville, categorie, type, lpdvente, dpvente, pcvente, allinsoftvente, allinvente, ultraallinvente, age_enf_gratuit, image) {
        var _this = this;
        var post = { id: id, nom: nom, adultOnly: adultOnly, ville: ville, categorie: categorie, type: type,
            lpdvente: lpdvente, dpvente: dpvente, pcvente: pcvente, allinsoftvente: allinsoftvente, allinvente: allinvente,
            ultraallinvente: ultraallinvente, age_enf_gratuit: age_enf_gratuit, image: image
        };
        this.httpClient
            .post("http://localhost:3000/hotel/api/hotel", post)
            .subscribe(function (responseData) {
            var id = responseData.postId;
            post.id = id;
            _this.posts.push(post);
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    HotelsService.prototype.update = function (id, nom, adultOnly, ville, categorie, type, lpdvente, dpvente, pcvente, allinsoftvente, allinvente, ultraallinvente, age_enf_gratuit, image, idg) {
        var _this = this;
        var post = { id: id, nom: nom, adultOnly: adultOnly, ville: ville, categorie: categorie, type: type,
            lpdvente: lpdvente, dpvente: dpvente, pcvente: pcvente, allinsoftvente: allinsoftvente, allinvente: allinvente,
            ultraallinvente: ultraallinvente, age_enf_gratuit: age_enf_gratuit, image: image
        };
        this.httpClient
            .put("http://localhost:3000/hotel/api/hotelModif/" + idg, post)
            .subscribe(function (responseData) {
            _this.posts.push(post);
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    HotelsService.prototype.deletePost = function (postId) {
        var _this = this;
        this.httpClient.delete("http://localhost:3000/hotel/api/posts/" + postId)
            .subscribe(function () {
            var updatedPosts = _this.posts.filter(function (post) { return post.id !== postId; });
            _this.posts = updatedPosts;
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    HotelsService.prototype.pubpost = function (item) {
        this.httpClient
            .post("http://localhost:3000/hotel/api/pub", item)
            .subscribe(function (responseData) {
            console.log(responseData.message);
        });
    };
    HotelsService.prototype.deleteall = function () {
        this.httpClient.delete("http://localhost:3000/hotel/api/deleteall/" + null)
            .subscribe(function () {
            console.log("dd");
        });
    };
    HotelsService.prototype.getHotelhomelist = function () {
        var _this = this;
        this.httpClient
            .get("http://localhost:3000/hotel/api/hotelhomelist")
            .pipe(map(function (postData) {
            return postData.posts.map(function (post) {
                return {
                    _id: post._id,
                    id: post.id, nom: post.nom, adultOnly: post.adultOnly, ville: post.ville, categorie: post.categorie, type: post.type,
                    lpdvente: post.lpdvente, dpvente: post.dpvente, pcvente: post.pcvente, allinsoftvente: post.allinsoftvente, allinvente: post.allinvente,
                    ultraallinvente: post.ultraallinvente, age_enf_gratuit: post.age_enf_gratuit, image: post.image
                };
            });
        }))
            .subscribe(function (transformedPosts) {
            _this.posts = transformedPosts;
            _this.postsUpdated.next(_this.posts.slice());
            _this.hotelhomelist = transformedPosts;
            _this.hotelhomelistUpdated.next(_this.posts.slice());
        });
    };
    HotelsService.prototype.hotelhomelistpost = function (item) {
        var _this = this;
        var post = { id: item.id, nom: item.nom, adultOnly: item.adultOnly, ville: item.ville, categorie: item.categorie, type: item.type,
            lpdvente: item.lpdvente, dpvente: item.dpvente, pcvente: item.pcvente, allinsoftvente: item.allinsoftvente, allinvente: item.allinvente,
            ultraallinvente: item.ultraallinvente, age_enf_gratuit: item.age_enf_gratuit, image: item.image
        };
        this.httpClient
            .post("http://localhost:3000/hotel/api/hotelhomelist", post).subscribe(function (responseData) {
            var id = responseData.postId;
            post.id = id;
            _this.hotelhomelist.push(post);
            _this.hotelhomelistUpdated.next(_this.posts.slice());
        });
    };
    HotelsService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], HotelsService);
    return HotelsService;
}());
export { HotelsService };
//# sourceMappingURL=hotels.service.js.map