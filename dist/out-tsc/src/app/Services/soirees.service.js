import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
//const url = "https://topresa.ovh/api/soirees.json?departsoirees.date[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en');
var url = "https://topresa.ovh/api/soirees.json?departsoirees.date[after]=17/06/2020";
var SoireesService = /** @class */ (function () {
    //*********Njeimi */
    function SoireesService(httpClient) {
        this.httpClient = httpClient;
        //*********Njeimi */
        this.posts = [];
        this.postsUpdated = new Subject();
        this.soireehomelist = [];
        this.soireehomelistUpdated = new Subject();
    }
    SoireesService.prototype.getSoiree = function () {
        return this.httpClient.get(url, httpOptions);
    };
    //*********Njeimi */
    SoireesService.prototype.getSoireesBase = function () {
        var _this = this;
        this.httpClient
            .get("http://localhost:3000/soiree/api/soiree")
            .pipe(map(function (postData) {
            return postData.posts.map(function (post) {
                return {
                    _id: post._id, id: post.id, titre: post.titre, image: post.image, date: post.date,
                    venteadultesingle: post.venteadultesingle, venteadultedble: post.venteadultedble,
                    vente3rdad: post.vente3rdad, venteenfant2ad: post.venteenfant2ad, tarifbebe: post.tarifbebe,
                    venteenfant1ad: post.venteenfant1ad
                };
            });
        }))
            .subscribe(function (transformedPosts) {
            _this.posts = transformedPosts;
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    SoireesService.prototype.addPost = function (id, titre, venteadultesingle, venteadultedble, date, images, vente3rdad, venteenfant2ad, tarifbebe, venteenfant1ad) {
        var _this = this;
        var post = { id: id, titre: titre, image: images, date: date, venteadultesingle: venteadultesingle,
            venteadultedble: venteadultedble, vente3rdad: vente3rdad, venteenfant2ad: venteenfant2ad, tarifbebe: tarifbebe,
            venteenfant1ad: venteenfant1ad
        };
        this.httpClient
            .post("http://localhost:3000/soiree/api/soiree", post)
            .subscribe(function (responseData) {
            var id = responseData.postId;
            post.id = id;
            _this.posts.push(post);
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    SoireesService.prototype.update = function (id, titre, venteadultesingle, venteadultedble, date, images, vente3rdad, venteenfant2ad, tarifbebe, venteenfant1ad, idg) {
        var _this = this;
        var post = { id: id, titre: titre, image: images, date: date, venteadultesingle: venteadultesingle,
            venteadultedble: venteadultedble, vente3rdad: vente3rdad, venteenfant2ad: venteenfant2ad, tarifbebe: tarifbebe,
            venteenfant1ad: venteenfant1ad
        };
        this.httpClient
            .put("http://localhost:3000/soiree/api/soireeModif/" + idg, post)
            .subscribe(function (responseData) {
            _this.posts.push(post);
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    SoireesService.prototype.deletePost = function (postId) {
        var _this = this;
        this.httpClient.delete("http://localhost:3000/soiree/api/posts/" + postId)
            .subscribe(function () {
            var updatedPosts = _this.posts.filter(function (post) { return post.id !== postId; });
            _this.posts = updatedPosts;
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    SoireesService.prototype.deleteall = function () {
        this.httpClient.delete("http://localhost:3000/soiree/api/deleteall/" + null)
            .subscribe(function () {
            console.log("dd");
        });
    };
    SoireesService.prototype.getSoireehomelist = function () {
        var _this = this;
        this.httpClient
            .get("http://localhost:3000/soiree/api/soireehomelist")
            .pipe(map(function (postData) {
            return postData.posts.map(function (post) {
                return {
                    _id: post._id,
                    id: post.id, titre: post.titre, image: post.image, date: post.date, venteadultesingle: post.venteadultesingle,
                    venteadultedble: post.venteadultedble, vente3rdad: post.vente3rdad, venteenfant2ad: post.venteenfant2ad, tarifbebe: post.tarifbebe,
                    venteenfant1ad: post.venteenfant1ad
                };
            });
        }))
            .subscribe(function (transformedPosts) {
            _this.posts = transformedPosts;
            _this.postsUpdated.next(_this.posts.slice());
            _this.soireehomelist = transformedPosts;
            _this.soireehomelistUpdated.next(_this.posts.slice());
        });
    };
    SoireesService.prototype.soireehomelistpost = function (item) {
        var _this = this;
        var post = { id: item.id, titre: item.titre, image: item.image, date: item.date, venteadultesingle: item.venteadultesingle,
            venteadultedble: item.venteadultedble, vente3rdad: item.vente3rdad, venteenfant2ad: item.venteenfant2ad, tarifbebe: item.tarifbebe,
            venteenfant1ad: item.venteenfant1ad
        };
        this.httpClient
            .post("http://localhost:3000/soiree/api/soireehomelist", post).subscribe(function (responseData) {
            var id = responseData.postId;
            post.id = id;
            _this.soireehomelist.push(post);
            _this.soireehomelistUpdated.next(_this.posts.slice());
        });
        console.log(post);
    };
    SoireesService.prototype.getPostUpdateListener = function () {
        return this.postsUpdated.asObservable();
    };
    SoireesService.prototype.gethomesoireeUpdateListener = function () {
        return this.soireehomelistUpdated.asObservable();
    };
    SoireesService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], SoireesService);
    return SoireesService;
}());
export { SoireesService };
//# sourceMappingURL=soirees.service.js.map