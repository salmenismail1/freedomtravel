import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
var ReservationService = /** @class */ (function () {
    function ReservationService(httpClient) {
        this.httpClient = httpClient;
    }
    ReservationService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], ReservationService);
    return ReservationService;
}());
export { ReservationService };
//# sourceMappingURL=reservation.service.js.map