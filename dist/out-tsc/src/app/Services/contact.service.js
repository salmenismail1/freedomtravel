import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
var contactUrl = "https://topresa.ovh/api/contactes";
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
var ContactService = /** @class */ (function () {
    function ContactService(httpClient) {
        this.httpClient = httpClient;
    }
    ContactService.prototype.postContact = function (contact) {
        return this.httpClient.post(contactUrl, contact);
    };
    ContactService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], ContactService);
    return ContactService;
}());
export { ContactService };
//# sourceMappingURL=contact.service.js.map