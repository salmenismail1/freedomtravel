import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
//const url = "https://topresa.ovh/api/omras.json?date[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en');
var url = "https://topresa.ovh/api/omras.json?date[after]=11/11/1000";
var OmraService = /** @class */ (function () {
    function OmraService(httpClient) {
        this.httpClient = httpClient;
    }
    OmraService.prototype.getOmra = function () {
        return this.httpClient.get(url, httpOptions);
    };
    OmraService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], OmraService);
    return OmraService;
}());
export { OmraService };
//# sourceMappingURL=omra.service.js.map