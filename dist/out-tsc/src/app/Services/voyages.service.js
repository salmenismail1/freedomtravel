import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
var url = "https://topresa.ovh/api/voyages.json?departvoyages.datedepart[after]=2020-02-14 ";
var deleteReservationVoyageUrl = "http://localhost:3000/voyage/api/DeleteReservation/";
var ReservationAccepter = "http://localhost:3000/voyage/api/ReservationAccepter/";
var ReservationAccepterUrl = "http://localhost:3000/voyage/api/ReservationVoyageAccepter";
var ReservationEnAttenteUrl = "http://localhost:3000/voyage/api/ReservationVoyageEnAttente";
var UneReservationUrl = "http://localhost:3000/voyage/api/UneReservation/";
var UpgradePayementUrl = "http://localhost:3000/user/api/UpgradePayement/";
var httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
var VoyagesService = /** @class */ (function () {
    //*******njeimi */
    function VoyagesService(httpClient) {
        this.httpClient = httpClient;
        //*******njeimi */
        this.posts = [];
        this.postsUpdated = new Subject();
        this.voyagehomelist = [];
        this.voyagehomelistUpdated = new Subject();
    }
    VoyagesService.prototype.getVoyage = function (voyage) {
        return this.httpClient.get(url, httpOptions);
    };
    VoyagesService.prototype.DeleteReservationHotel = function (Titre) {
        return this.httpClient.delete(deleteReservationVoyageUrl + Titre, httpOptions);
    };
    VoyagesService.prototype.ReservationAccepter = function (Titre) {
        return this.httpClient.put(ReservationAccepter + Titre, httpOptions);
    };
    VoyagesService.prototype.getReservationAccepter = function () {
        return this.httpClient.get(ReservationAccepterUrl, httpOptions);
    };
    VoyagesService.prototype.getReservationEnAttent = function () {
        return this.httpClient.get(ReservationEnAttenteUrl, httpOptions);
    };
    VoyagesService.prototype.getUneReservation = function (id) {
        return this.httpClient.get(UneReservationUrl + id, httpOptions);
    };
    VoyagesService.prototype.UpgradePayement = function (user) {
        return this.httpClient.put(UpgradePayementUrl + user, user);
    };
    //*******njeimi */
    VoyagesService.prototype.getPostUpdateListener = function () {
        return this.postsUpdated.asObservable();
    };
    VoyagesService.prototype.gethomevoyageUpdateListener = function () {
        return this.voyagehomelistUpdated.asObservable();
    };
    VoyagesService.prototype.getVoyageBase = function () {
        var _this = this;
        this.httpClient
            .get("http://localhost:3000/voyage/api/voyage")
            .pipe(map(function (postData) {
            return postData.posts.map(function (post) {
                return {
                    _id: post._id,
                    id: post.id, nom: post.nom, pays: post.pays, titre: post.titre, prix: post.prix, departvoyages: post.departvoyages,
                    imageVoyages: post.imageVoyages, image: post.image
                };
            });
        }))
            .subscribe(function (transformedPosts) {
            _this.posts = transformedPosts;
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    VoyagesService.prototype.addPost = function (id, nom, pays, titre, prix, datedepart, dateretour, images) {
        var _this = this;
        var post = { id: id, nom: nom, pays: pays, titre: titre, prix: prix, departvoyages: { datedepart: datedepart, dateretour: dateretour }, imageVoyages: [{ images: images }]
        };
        this.httpClient
            .post("http://localhost:3000/voyage/api/voyage", post)
            .subscribe(function (responseData) {
            var id = responseData.postId;
            post.id = id;
            _this.posts.push(post);
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    VoyagesService.prototype.update = function (id, nom, pays, titre, prix, datedepart, dateretour, images, idg) {
        var _this = this;
        var post = { id: id, nom: nom, pays: pays, titre: titre, prix: prix, departvoyages: { datedepart: datedepart, dateretour: dateretour }, imageVoyages: [{ images: images }]
        };
        this.httpClient
            .put("http://localhost:3000/voyage/api/voyageModif/" + idg, post)
            .subscribe(function (responseData) {
            _this.posts.push(post);
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    VoyagesService.prototype.deletePost = function (postId) {
        var _this = this;
        this.httpClient.delete("http://localhost:3000/voyage/api/posts/" + postId)
            .subscribe(function () {
            var updatedPosts = _this.posts.filter(function (post) { return post.id !== postId; });
            _this.posts = updatedPosts;
            _this.postsUpdated.next(_this.posts.slice());
        });
    };
    VoyagesService.prototype.deleteall = function () {
        this.httpClient.delete("http://localhost:3000/voyage/api/deleteall/" + null)
            .subscribe(function () {
        });
    };
    VoyagesService.prototype.getVoyagehomelist = function () {
        var _this = this;
        this.httpClient
            .get("http://localhost:3000/voyage/api/voyagehomelist")
            .pipe(map(function (postData) {
            return postData.posts.map(function (post) {
                return {
                    _id: post._id,
                    id: post.id, nom: post.nom, pays: post.pays, titre: post.titre, prix: post.prix,
                    departvoyages: { datedepart: post.datedepart, dateretour: post.dateretour }, imageVoyages: post.imageVoyages
                };
            });
        }))
            .subscribe(function (transformedPosts) {
            _this.posts = transformedPosts;
            _this.postsUpdated.next(_this.posts.slice());
            _this.voyagehomelist = transformedPosts;
            _this.voyagehomelistUpdated.next(_this.posts.slice());
        });
    };
    VoyagesService.prototype.voyagehomelistpost = function (item) {
        var _this = this;
        var datedepart = item.datedepart;
        var dateretour = item.dateretour;
        var post = { id: item.id, nom: item.nom, pays: item.pays, titre: item.titre, prix: item.prix,
            departvoyages: { datedepart: datedepart, dateretour: dateretour }, imageVoyages: item.imageVoyages
        };
        this.httpClient
            .post("http://localhost:3000/voyage/api/voyagehomelist", post).subscribe(function (responseData) {
            var id = responseData.postId;
            post.id = id;
            _this.voyagehomelist.push(post);
            _this.voyagehomelistUpdated.next(_this.posts.slice());
        });
    };
    VoyagesService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], VoyagesService);
    return VoyagesService;
}());
export { VoyagesService };
//# sourceMappingURL=voyages.service.js.map