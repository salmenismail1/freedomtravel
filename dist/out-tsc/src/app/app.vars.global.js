import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { LoginService } from './Services/login.service';
import { Login } from './Models/login';
import { User } from './Models/user';
import * as jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
var AppVarsGlobal = /** @class */ (function () {
    function AppVarsGlobal(toastr, loginserv, router) {
        this.toastr = toastr;
        this.loginserv = loginserv;
        this.router = router;
        this.type = "";
        this.log = new Login;
        this.user = new User;
    }
    AppVarsGlobal.prototype.login = function () {
        var _this = this;
        var decoded = jwt_decode(localStorage.getItem('token'));
        this.loginserv.getOnUser(decoded.id).subscribe(function (response) {
            _this.type = response.type;
        });
        return this.type;
    };
    AppVarsGlobal = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [ToastrService, LoginService, Router])
    ], AppVarsGlobal);
    return AppVarsGlobal;
}());
export { AppVarsGlobal };
//# sourceMappingURL=app.vars.global.js.map