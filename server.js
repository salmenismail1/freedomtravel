const app = require("./BackEnd/app");
const debug = require("debug")("node-angular");
const http = require("http");


const normalizePort = val => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};

const onError = error => {
  if (error.syscall !== "listen") {
    throw error;
  }
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  debug("Listening on " + bind);
};

const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);

const server = http.createServer(app);
server.on("error", onError);
server.on("listening", onListening);
server.listen(port);



const RVoyage= require('./BackEnd/Controllers/VoyageControllers');
const RHotel= require('./BackEnd/Controllers/HotelControllers');
const circuit= require('./BackEnd/Controllers/CircuitControllers');
const soiree= require('./BackEnd/Controllers/SoireeControllers');
const omra= require('./BackEnd/Controllers/OmraControllers');
const user= require('./BackEnd/Controllers/UserController')

app.use('/voyage',RVoyage);
app.use('/hotel',RHotel);
app.use('/Circuit',circuit);
app.use('/Omra',omra);
app.use('/Soiree',soiree);
app.use('/user',user)


app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST,PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});